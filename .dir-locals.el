((emacs-lisp-mode .
                  ((eval .
                         (setq-local
                          imenu-generic-expression
                          `(,@lisp-imenu-generic-expression
                            ("D&D"
                             ,(concat "^\\s-*("
			              (eval-when-compile
			                (regexp-opt '("defdnd"
                                                      "defdnd-priority"
                                                      "defdnd-custom"
                                                      "defdnd-custom-priority") nil))
                                      "\\s-+\\("
                                      lisp-mode-symbol-regexp "\\)") 1))))
                   (eval .
                         (font-lock-add-keywords
                          'emacs-lisp-mode
                          `((,(concat (regexp-opt '("defdnd" "defdnd-priority"))
                                      " \\(.*?\\)\\(?:(\\)") 1 'font-lock-function-name-face)) 'prepend)))))

                         

