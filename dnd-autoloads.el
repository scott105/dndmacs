;;; dnd-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

;;;### (autoloads nil "dnd-mode" "dnd-mode.el" (23922 56619 0 0))
;;; Generated autoloads from dnd-mode.el

(autoload 'dnd-mode "dnd-mode" "\
Minor mode for playing D&D 5e in an org-mode document.

Configuration of a document is largely done through org-mode data lines in the form of:
#+<type>: <data>

#+config: <path>
	 Loads a relative or absolute path pointing to another org file.  Only the '#+' options are loaded from the config (i.e. not tables nor character sheets).

#+static-data: <path>
	 Loads a relative or absolute path pointing to another org file. The data loaded will be all org tables and all entries marked as character sheets (with the :character: tag)

#+custom-commands: <path>
	 Loads a relative or absolute path pointing to an elisp file. The file is evaluated as lisp code.

#+chat: <url>
	 Specifies the Discord webhook (if https://) or irc (if irc://server/#channel)
	 If no chat URL is specified, chat messages are sent to the log.

When first run in an org document, any missing required sections or tables will be created.

Character sheets are of the form:

* Some Entry :character:
:PROPERTIES:
:name: <character name>
:stats: <str> <dex> <con> <int> <wis> <char>
:class: <class>
:level: <level>
:xp: <xp>
:hp: <hp>
:ac: <ac>
:speed: <speed>
:resistant: <resistances>
:immune: <immunities>
:vulnerable: <vulnerabilities>
:attack: (<to hit bonus> (<dmg type>: <dmg>) <range> [:magic t :silvered t])
:attack+: ...
:END:

Further, characters can have 'proficiencies', 'saves', and 'casting'.
Monsters can have 'skills' and 'cr'.

Skills can have the special forms:
  <skill>:exp    which results in the skill being treated as having 'Expertise'
and
  <skill>:<#>    which specifies the skill score (which may not match the stats for monsters)

Free-form attributes can also be stored in the property block.  These will not be processed in any special way aside from being converted to strings.

Mode Keys:

\\{dnd-mode-map}

In the command queue, several other commands are bound:

\\{dnd-queue-map}

\(fn &optional ARG)" t nil)

(autoload 'dnd-challenge-probability "dnd-mode" "\


\(fn SUCCESSES BONUS &rest FORMS)" nil t)

(autoload 'dnd-challenge-probability-aggregate "dnd-mode" "\


\(fn SUCCESSES &rest FORMS)" nil t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dnd-mode" '(#("dnd-" 0 4 (fontified nil)))))

;;;***

(provide 'dnd-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; dnd-autoloads.el ends here

;;;### (autoloads nil "dnd-def" "dnd-def.el" (0 0 0 0))
;;; Generated autoloads from dnd-def.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dnd-def" '("dnd-" "defdnd" "helm-dnd-")))

;;;***

;;;### (autoloads nil "dnd-helm" "dnd-helm.el" (0 0 0 0))
;;; Generated autoloads from dnd-helm.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dnd-helm" '("helm-dnd-" "dnd-")))

;;;***

;;;### (autoloads nil "dnd-mode" "dnd-mode.el" (0 0 0 0))
;;; Generated autoloads from dnd-mode.el

(autoload 'dnd-mode "dnd-mode" "\
Minor mode for playing D&D 5e in an org-mode document.

Configuration of a document is largely done through org-mode data lines in the form of:
#+<type>: <data>

#+config: <path>
	 Loads a relative or absolute path pointing to another org file.  Only the '#+' options are loaded from the config (i.e. not tables nor character sheets).

#+static-data: <path>
	 Loads a relative or absolute path pointing to another org file. The data loaded will be all org tables and all entries marked as character sheets (with the :character: tag)

#+custom-commands: <path>
	 Loads a relative or absolute path pointing to an elisp file. The file is evaluated as lisp code.

#+chat: <url>
	 Specifies the Discord webhook (if https://) or irc (if irc://server/#channel)
	 If no chat URL is specified, chat messages are sent to the log.

When first run in an org document, any missing required sections or tables will be created.

Character sheets are of the form:

* Some Entry :character:
:PROPERTIES:
:name: <character name>
:stats: <str> <dex> <con> <int> <wis> <char>
:class: <class>
:level: <level>
:xp: <xp>
:hp: <hp>
:ac: <ac>
:speed: <speed>
:resistant: <resistances>
:immune: <immunities>
:vulnerable: <vulnerabilities>
:attack: (<to hit bonus> (<dmg type>: <dmg>) <range> [:magic t :silvered t])
:attack+: ...
:END:

Further, characters can have 'proficiencies', 'saves', and 'casting'.
Monsters can have 'skills' and 'cr'.

Skills can have the special forms:
  <skill>:exp    which results in the skill being treated as having 'Expertise'
and
  <skill>:<#>    which specifies the skill score (which may not match the stats for monsters)

Free-form attributes can also be stored in the property block.  These will not be processed in any special way aside from being converted to strings.

Mode Keys:

\\{dnd-mode-map}

In the command queue, several other commands are bound:

\\{dnd-queue-map}

\(fn &optional ARG)" t nil)

(autoload 'dnd-challenge-probability "dnd-mode" "\


\(fn SUCCESSES BONUS &rest FORMS)" nil t)

(autoload 'dnd-challenge-probability-aggregate "dnd-mode" "\


\(fn SUCCESSES &rest FORMS)" nil t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dnd-mode" '(#("dnd-" 0 4 (fontified nil)))))

;;;***

;;;### (autoloads nil "dnd-overland" "dnd-overland.el" (0 0 0 0))
;;; Generated autoloads from dnd-overland.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dnd-overland" '("dnd-")))

;;;***

;;;### (autoloads nil "dnd-queue" "dnd-queue.el" (0 0 0 0))
;;; Generated autoloads from dnd-queue.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dnd-queue" '("dnd-")))

;;;***

;;;### (autoloads nil "dnd-roll" "dnd-roll.el" (0 0 0 0))
;;; Generated autoloads from dnd-roll.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dnd-roll" '("dnd-roll-")))

;;;***

;;;### (autoloads nil "dnd-tables" "dnd-tables.el" (0 0 0 0))
;;; Generated autoloads from dnd-tables.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dnd-tables" '("dnd-")))

;;;***

;;;### (autoloads nil "overland" "overland.el" (0 0 0 0))
;;; Generated autoloads from overland.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "overland" '("overland-")))

;;;***

;;;### (autoloads nil "dnd-commands" "dnd-commands.el" (0 0 0 0))
;;; Generated autoloads from dnd-commands.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dnd-commands" '("dnd-")))

;;;***
