(require 'cl-lib)
(require 'dnd-def)
(require 'dnd-helm)

(defdnd-command-alias
  :clear
  :break
  :wait-completed)

(defun dnd-5e-chat-helper (&rest data)
  ""
  (let ((chat-state (dnd-get-state :chat-inversions))
        key
        val
        quiet
        chats
        bare
        chat-open
        results)

    (cl-loop while data do
             (setq key (pop data)
                   val (pop data))

             (setq quiet (s-matches? "-quiet$" (symbol-name key)))
             ;; Check the state for the inversion of this key
             (when (member key chat-state)
               (setq quiet (not quiet)))
             
             (cond
              ((or quiet (equal key nil))
               (when chat-open
                 (push `(chat ,(s-join "\n" (reverse chat-open))) results)
                 (setq chat-open nil))
               (push val results))

              (t
               (push val chat-open))))

    ;;
    (when chat-open
      (push `(chat ,(s-join "\n" (reverse chat-open))) results))

    (reverse results)))

(defun dnd-invert-chat (&rest keys)
  (let ((current (or (dnd-get-state :chat-inversions) '())))
    (dnd-set-state :chat-inversions
                   (delete-dups
                    (append current keys)))))

(defun dnd-clear-chat (&rest keys)
  (prog1 nil
    (let ((current (dnd-get-state :chat-inversions))
          new)
      (cl-loop for k in current do
               (setq new (delete k current)))
      (dnd-set-state :chat-inversions new))))

(defdnd dnd-5e-invert-chat (&rest keys)
  (prog1 nil
    (apply 'dnd-invert-chat keys)))

(defdnd dnd-5e-clear-chat (&rest keys)
  (prog1 nil
    (apply 'dnd-clear-chat keys)))

(defun dnd-5e-conditional-helper (check forms)
  "Helper for dealing with conditional blocks with the special identifier `else' as the car of the final element of `forms'.  The branching is done by comparing `check' with the `:success' value in the state.
A `:clear' command is added to the returned form."
  (let* ((last-form (-last-item forms))
         has-else)

    ;; Detect misplaced 'else' form (must be at the end)
    (cl-loop for el in forms if (listp el) do
             (when (equal (car el) 'else)
               (setq has-else t)))

    (when (and has-else
               (not
                (and (listp last-form)
                     (equal (car last-form) 'else))))
      (error "Malformed conditional.  'else' form not at end of block"))
    
    (--when-let
        (if (equal (dnd-get-state :success) check)
            (if has-else
                (butlast forms)
              forms)

          ;; else
          (when has-else
            (cdr last-form)))

      (append it '(:clear)))))

(defdnd dnd-5e-on-success (&rest forms)
  (:call (:on-success))
  (dnd-5e-conditional-helper t forms))

(defdnd dnd-5e-on-failure (&rest forms)
  (:call (:on-failure))
  (dnd-5e-conditional-helper nil forms))

(defdnd dnd-5e-else (&rest _)
  (:call (:else))
  ;; Note:  Execution will only get in here if an else block is at the top level in the queue.
  (error "'else' block found outside of 'on-success' or 'on-failure'"))

(defdnd dnd-5e-chat (str)
  (dnd-send-to-chat str))

(defdnd dnd-5e-speak (speaker str)
  (dnd-send-to-chat str (format "%s" speaker)))

(defdnd dnd-5e-actor (actor)
  (:call (t . (actor-source)))
  (dnd-set-state :actor (format "%s" actor)))

(defdnd dnd-5e-advantage ()
  (:call (t))
  (dnd-set-state :advantage t))

(defdnd dnd-5e-disadvantage ()
  (:call (t))
  (dnd-set-state :disadvantage t))

(defdnd dnd-5e-clear-advantage ()
  (:call (t))
  (dnd-set-state :advantage nil)
  (dnd-set-state :disadvantage nil))

(defdnd dnd-5e-d20 ()
  (:call (t))
  (let ((advantage (dnd-get-state :advantage))
        (disadvantage (dnd-get-state :disadvantage))
        dice op result)

    (prog1 nil
      (setq dice
            (if (or (and advantage disadvantage)
                    (not (or advantage disadvantage)))
                "1d20"
              "2d20"))
      ;;
      (dnd-append-state :roll (dnd-roll-parse dice)))))

(defdnd dnd-5e-capture (&rest parts)
  (:call (t))
  (let ((roll (dnd-get-state :roll)))
    (if (not (equal (dnd-roll-remaining-rolls roll) (length parts)))
        (progn
          (setq dnd-queue-pass t)
          `(((:overlay before-string
                       ,(concat
                         (propertize
                          (concat "    " (dnd-roll-to-string roll) "    ")
                          'face
                          '(:foreground "white" :background "grey24" :box "white"))
                         "\n")
                       line-prefix
                       ,(propertize "# " 'face '(:foreground "grey24")))
             (capture ,@parts
                      ((:propertize input-capture t) "")))))
                       
      ;;
      (setq roll (apply 'dnd-roll-replace-rolls roll parts))
      (dnd-set-state :roll roll)
      '())))

(defdnd dnd-5e-foreach (actor-list &rest forms)
  (:call (:foreach . (multi-actor-source)))
  (let (results)
    (cl-loop for actor in actor-list do
             (cl-loop for entry in forms do
                      (push (dnd-replace-element-in-form :actor actor entry) results)))
    (reverse results)))

(defun dnd-5e-hit-roll-str (status)
  (concat
   "***"
   (cond
    ((equal status :fail) "[fail]")
    ((equal status :crit) "[crit]")
    ((equal status :hit) "[hit]")
    ((equal status :miss) "[miss]"))
   "***"))

(defdnd dnd-5e-attack-check (ac)
  (let* ((dice-roll (dnd-roll-perform (dnd-get-state :roll)))
         (d20 (dnd-roll-get-d20 dice-roll))
         (total (dnd-roll-result dice-roll (dnd-roll-has-disadvantage)))
         (crit-threshold (dnd-get-state :crit-threshold 20))
         (ac (dnd-5e--dc-arg ac))
         (suspense-tag (if (dnd-get-state :suspense) "||" ""))
         result)

    (setq dnd-last-roll-result dice-roll)
    ;; Note:  In order to detect crit/fail, the d20 roll needs to be isolated
    ;; .This is done by finding the first d20 result in the structure and using that
    ;; .The only time that might fall down would be if there were damage calcs using d20, but I don't think those exist

    (setq result
          (cond ((>= d20 crit-threshold)
                 :crit)
                
                ((= 1 d20)
                 :fail)

                (t
                 (if (>= total ac)
                     :hit
                   :miss))))

    (dnd-set-state :success (or (equal result :crit) (equal result :hit)))
    (dnd-set-state :crit (equal result :crit))
    (cl-remove
     nil
     `(,@(dnd-5e-chat-helper :roll (format "***attack roll**  %s%s = %s%s*"
                                           suspense-tag
                                           (dnd-roll-to-string dice-roll)
                                           total
                                           suspense-tag)
                             :hit-result-quiet (dnd-5e-hit-roll-str result))
       ,(when (equal result :crit)
          '(set-crit))))))

(defun dnd-5e--requested-data-pending ()
  `(((:overlay
      before-string
      ,(concat
        (propertize
         (concat "    missing     ")
         'face
         '(:foreground "white" :background "grey24" :box "white"))
        "\n")

      after-string "\n"
      
      line-prefix
      ,(propertize "# " 'face '(:foreground "grey24")))
     
     ,@(cl-loop
        for key in (sort (hash-table-keys dnd-requested-data) 'string<)
        as val = (gethash key dnd-requested-data)
        unless val collect
        
        `(set-request
          ,key
          ((:propertize input-capture t) ""))))))

(defdnd-priority dnd-5e-set-request (key &optional val)
  (when val
    (dnd-request-data-set key val)))

(defdnd-priority dnd-5e-attack (actor weapon target &key offhand)
  (:call (t . (actor-source weapon-source target-source))
         (attack-offhand . (actor-source weapon-source target-source :offhand t))
         (attack-statblock . (actor-source weapon-source statblock-source)))
  (let* ((actor (format "%s" actor))
         (target (format "%s" target))
         (actor-state (dnd-get-actor-state actor))
         (actor-data (dnd-get-actor-data actor-state))
         (actor-crit-threshold (dnd-query-data actor-data "crit-threshold"))
         (target-state (dnd-get-actor-state target))
         (target-data (dnd-get-actor-data target))
         (target-ac (or (dnd-get-actor-ac target-data) (dnd-request-data target 'ac)))
         (target-type (dnd-query-data target-data "type"))
         (target-undead (or (dnd-get-state :undead) (string= target-type "undead")))
         (weapon (format "%s" weapon))
         (weapon-data (dnd-query-data actor-data "attacks" weapon))
         (live-rolling (dnd-get-state :live-roll))
         parameter-data
         to-hit
         dice
         dmg-dice)

    (setq parameter-data
          `(:dex ,(dnd-ability-modifier
                   (string-to-number (dnd-query-data actor-data "dex")))
                 :str ,(dnd-ability-modifier
                        (string-to-number (dnd-query-data actor-data "str")))
                 :wis ,(dnd-ability-modifier
                        (string-to-number (dnd-query-data actor-data "wis")))
                 :int ,(dnd-ability-modifier
                        (string-to-number (dnd-query-data actor-data "int")))
                 :char ,(dnd-ability-modifier
                         (string-to-number (dnd-query-data actor-data "char")))
                 :pro ,(dnd-get-actor-proficiency-bonus actor-data)))

    ;; Only add negative damage modifier when off-hand attacking
    (when offhand
      (cl-loop for k in '(:str :dex :wis :int :char) do
               (--when-let (plist-get parameter-data k)
                 (when (> it 0)
                   (plist-put parameter-data k 0)))))

    (setq to-hit (dnd-roll-replace-equation-parameters
                  (dnd-query-data weapon-data "hit_bonus") parameter-data)
          dice (dnd-roll-replace-equation-parameters
                (dnd-query-data weapon-data "dmg") parameter-data))

    (setq dmg-dice
          (if (string-match "^[0-9]+$" dice)
              (string-to-number dice)
            (dnd-to-symbol dice)))

    (cond
     ((not target-ac) `((:break ,(dnd-5e--requested-data-pending))))
     (t
      (dnd-set-state :actor actor)
      (dnd-set-state :weapon weapon)
      (dnd-set-state :target target)
      (dnd-set-state :target-state target-state)
      (puthash actor `(,weapon ,target) dnd-attack-history)

      (cl-remove
       nil
       `(,@(dnd-5e-chat-helper (unless (dnd-query-data target-state "hidden") :attack)
                               (format "***%s*** attacks ***%s*** with _%s_"
                                       (dnd-beautify-actor-name (dnd-get-actor-public-name actor-state))
                                       (dnd-beautify-actor-name (dnd-get-actor-public-name target-state))
                                       weapon))
         
         (d20)

         (roll-modifier ,to-hit "to hit bonus")
         ,(when target-undead
            (--when-let (dnd-query-data weapon-data "undead-hit")
              `(roll-modifier ,it "undead hit bonus")))
         
         ,(when live-rolling
            '(capture))
         
         ,(--when-let actor-crit-threshold
            `(crit-threshold ,(string-to-number actor-crit-threshold)))
         (set-dc ,target-ac)
         (attack-check :dc)
         ,(cl-remove
           nil
           `(on-success
             ,(when (dnd-query-data weapon-data "magic")
                '(magical))
             ,(when (dnd-query-data weapon-data "silver")
                '(silvered))

             ;;
             (dmg-modifier ,dmg-dice "weapon dmg")
             
             ,(when target-undead
                (--when-let (dnd-query-data weapon-data "undead-dmg")
                  `(dmg-modifier ,it "undead dmg bonus")))

             (dmg-to-roll)
             ,@(when live-rolling
                 `((live-roll ,dmg-dice)
                   (capture)))
             
             (dmg ,(dnd-to-symbol target)
                  :roll :chat t)
             ;; ,(if live-rolling
             ;;      :roll
             ;;    dmg-dice)
             ;; :chat t)
             ))
         :clear))))))

(defdnd-priority dnd-5e-spell-attack (actor target &key casting label hit-bonus)
  (:call (t . (actor-source target-source))
         ("spell attack (monster)" . (actor-source target-source :hit-bonus custom-integer-entry-source)))
  (let* ((actor (format "%s" actor))
         (actor-data (dnd-get-actor-data actor))
         (target (format "%s" target))
         (target-data (dnd-get-actor-data target))
         (target-state (dnd-get-actor-state target))
         (target-ac (dnd-get-actor-ac target-data))
         (stat (--when-let (or casting (dnd-query-data actor-data "casting"))
                 (format "%s" it)))
         (live-rolling (dnd-get-state :live-roll)))

    (dnd-set-state :actor actor)
    (dnd-set-state :target target)

    (if (or stat hit-bonus)
        (-non-nil
         `(,@(dnd-5e-chat-helper
              (unless (dnd-query-data target-state "hidden") :attack)
              (format "***%s*** casts *%s* at ***%s***"
                      (dnd-beautify-actor-name actor)
                      (or label "a spell")
                      (dnd-beautify-actor-name
                       (dnd-get-actor-public-name target-state))))

           ,@(when stat
               `((roll-modifier ,(dnd-get-actor-proficiency-bonus actor-data) "Proficiency")
                 (stat-bonus ,(dnd-to-symbol actor) ,(make-symbol stat))))
           ,(when hit-bonus
              `(roll-modifier ,hit-bonus "to-hit bonus"))
           
           (d20)
           
           ,(when live-rolling
              '(capture))

           (set-dc ,target-ac)
           (attack-check :dc)
           :clear))
      (list (format "Actor '%s' has no casting data" actor)))))

(defdnd dnd-5e-roll-modifier (amount &optional comment)
  (:call
   (proficiency . (`(dnd-get-actor-proficiency-bonus . ,helm-dnd-actor-source)))
   (t . (dice-and-roll-modifier-source)))
  (prog1 nil
    (dnd-append-state :roll (dnd-roll-parse amount))))

(defdnd dnd-5e-dmg-modifier (amount &optional comment)
  (:call (t . (damage-dice-source))
         ("*feat charger" . (5)))
  (prog1 nil
    (dnd-append-state :dmg (dnd-roll-parse amount))))

(defdnd dnd-5e-dmg-to-roll ()
  (:call (t))
  (prog1 nil
    (dnd-set-state
     :roll
     (--when-let (dnd-get-state :dmg)
       (if (dnd-get-state :crit)
           (dnd-roll-crit-spec it)
         it)))))

(defdnd dnd-5e-stat-bonus (actor stat &optional comment)
  (:call (t . (actor-and-statblock-source stat-source)))
  (let* ((actor (format "%s" actor))
         (data (dnd-get-actor-data actor))
         (stat (string-to-number (dnd-query-data data (prin1-to-string stat))))
         (bonus (dnd-ability-modifier stat))
         (current (dnd-get-state :roll)))

    (dnd-set-state :roll (append current (list bonus))))
  nil)

(defun dnd-5e-ability-check (actor stat-or-skill dc &optional type override-stat)
  (let* ((actor (format "%s" actor))
         (data (dnd-get-actor-data actor))
         (actor-state (dnd-get-actor-state actor))
         (advantage (dnd-get-state :advantage))
         (disadvantage (or (dnd-get-state :disadvantage)
                           (dnd-query-data data "skill-disadvantage" stat-or-skill)))
         (is-passive (equal type :passive))
         (is-save (equal type :save))
         (stat (or (--when-let override-stat (format "%s" it)) (dnd-query-static-data "skills" stat-or-skill "stat") stat-or-skill))
         (is-monster (dnd-query-data data "cr"))
         (proficient (if is-save
                         (dnd-query-static-data "monsters" actor "saves" stat)
                       (dnd-actor-has-proficiency actor stat-or-skill)))
         (passive-roll 10)
         (jack (dnd-actor-has-proficiency actor "jack"))
         (live-rolling (dnd-get-state :live-roll))
         (dc (dnd-5e--dc-arg dc))
         monster-skill)

    (if is-passive
        (setq passive-roll
              (+ passive-roll
                 (if advantage 5 0)
                 (if disadvantage -5 0)))
      (when disadvantage
        (dnd-set-state :disadvantage t)))
    
    (cl-remove nil
               `(,@(if is-passive
                       `((roll-modifier ,passive-roll))
                     `((d20)
                       ,(when live-rolling
                         '(capture))))
                 (stat-bonus ,(dnd-to-symbol actor) ,(make-symbol stat))
                 ,(when (not is-monster)
                    (cond
                     (proficient
                      `(roll-modifier ,(dnd-get-actor-proficiency-bonus
                                        data
                                        (when (string= proficient "exp") 2))
                                      "Proficiency"))
                     ((and jack (not (equal type :passive)))
                      `(roll-modifier ,(dnd-get-actor-proficiency-bonus data 0.5)
                                      "Jack of all trades"))))
                 ,(when is-monster
                    (when (setq monster-skill
                                (if is-save
                                    proficient
                                  (dnd-query-data data "skills" stat-or-skill)))
                      `(roll-modifier ,(string-to-number monster-skill) "monster skill")))
                 
                 ,(if (equal dc :store)
                      '(store-roll)
                    `(roll-check ,dc ,(format "%s's %s"
                                              (if actor-state
                                                  (dnd-beautify-actor-name
                                                   (dnd-get-actor-public-name actor-state))
                                                actor)
                                              stat-or-skill)))))))

(defdnd-priority dnd-5e-skill-check (actor skill dc &optional override-stat)
  (:call
   (t . (actor-and-statblock-source skill-source difficulty-source))
   (skill-check-var . (actor-and-statblock-source skill-source difficulty-source stat-source)))
  (dnd-5e-ability-check (dnd-to-symbol actor) (prin1-to-string skill) dc :skill override-stat))

(defdnd-priority dnd-5e-passive (actor skill dc &optional override-stat)
  (:call
   (t . (actor-and-statblock-source skill-source difficulty-source))
   (passive-var . (actor-and-statblock-source skill-source difficulty-source stat-source)))
  (dnd-5e-ability-check (dnd-to-symbol actor) (prin1-to-string skill) dc :passive override-stat))

(defdnd dnd-5e-party-passive (skill dc &optional override-stat)
  (:call
   (t . (skill-source difficulty-source))
   (party-passive-var . (skill-source difficulty-source stat-source)))
  (let* ((state-actors (dnd-table-to-assoc "Actors")))
    (cl-remove nil
               (cl-loop for entry in state-actors collect
                        (when (dnd-actor-state-playerp entry)
                          `(passive ,(dnd-to-symbol (car entry)) ,skill ,dc ,override-stat))))))

(defdnd dnd-5e-stealth (actor other)
  (:call (t . (actor-and-statblock-source actor-and-statblock-source)))
  `((passive ,other perception :store)
    (skill-check ,actor stealth :last)))

(defdnd dnd-5e-group-stealth (actor-list other)
  (:call (t . (multi-actor-and-statblock-source actor-and-statblock-source)))
  (let* ()
     `((passive ,other perception :store)
     
       ,@(cl-loop for actor in actor-list 
                  collect
                  `(skill-check ,actor stealth :last)))))

(defdnd dnd-5e-group-perceive-stealth (actor-list sneaker &optional sneaker-advantage)
  (:call (t . (multi-actor-and-statblock-source actor-and-statblock-source))
         (chase-quarry-check . (multi-actor-source chase-source chase-advantage-source)))
  (let* ((states (dnd-table-to-assoc "Actors")))
    (cl-remove
     nil
     `(,(cond
         ((equal sneaker-advantage :advantage)
          '(advantage))
         ((equal sneaker-advantage :disadvantage)
          '(disadvantage)))
       (skill-check ,sneaker stealth :store)
       ,@(cl-loop for actor in actor-list
                  if (not (string= "0" (dnd-query-data states actor "hp")))
                  collect
                  `(passive ,actor perception :last))))))

(defdnd-priority dnd-5e-saving-throw (actor stat dc)
  (:call (t . (actor-source stat-source difficulty-source)))
  (dnd-5e-ability-check (dnd-to-symbol actor) (prin1-to-string stat) dc :save))

(defdnd-priority dnd-5e-spell-save (actor stat caster &optional casting-stat)
  (:call (t . (actor-source stat-source actor-source stat-source)))
  (let* ((caster (format "%s" caster))
         (caster-data (dnd-get-actor-data caster)))

    (when casting-stat
      (dnd-5e-ability-check (dnd-to-symbol actor)
                            (prin1-to-string stat)
                            (dnd-5e--caster-dc caster-data casting-stat)
                            :save))))

(defdnd dnd-5e-death-save (actor)
  (:call (t . (actor-source)))
  `((clear-advantage)
    (d20)
    (death-check ,actor)))

(defdnd dnd-5e-stabilize (actor)
  (:call (t . (actor-source)))
  (let ((actor (format "%s" actor)))
    (dnd-remove-effect actor "life" 3)
    (dnd-remove-effect actor "death" 3)
    (dnd-add-effect actor "stable")))

(defdnd dnd-5e-dead (actor)
  (:call (t . (actor-source)))
  (let ((actor (format "%s" actor)))
    (dnd-remove-effect actor "life" 3)
    (dnd-remove-effect actor "death" 3)
    (dnd-remove-effect actor "unconscious")
    (dnd-remove-effect actor "stable")
    (when (not (dnd-has-effect actor "dead"))
      (dnd-add-effect actor "dead"))))

(defdnd dnd-5e-death-check (actor)
  (:call (t . (actor-source)))
  (let* ((actor (format "%s" actor))
         (dice-roll (dnd-roll-perform (dnd-get-state :roll)))
         (d20 (dnd-roll-get-d20 dice-roll))
         (row (dnd-table-find-row "Actors" `("*" ,actor)))
         status-string
         life death
         result
         result-string)

    (setq result
          (cond ((= 20 d20)
                 :crit)

                ((= 1 d20)
                 :fail)

                (t
                 (if (>= d20 10)
                     :good
                   :bad))))

    (setq result-string
          (cond
           ((equal result :crit) "[crit]")
           ((equal result :fail) "[fail]")
           ((equal result :good) "[good]")
           ((equal result :bad) "[bad]")))
    
    ;; Deal with the status additions right here
    (cond
     ((equal result :crit)
      (dnd-remove-effect actor "death")
      (dnd-table-set-field "Actors" row "hp" 1))

     ((equal result :fail)
      (dnd-add-effect actor "death" 2))
     
     ((equal result :good)
      (dnd-add-effect actor "life"))

     ((equal result :bad)
      (dnd-add-effect actor "death")))

    ;; Now see where we're at
    (setq life (dnd-get-effect-count actor "life")
          death (dnd-get-effect-count actor "death"))

    (setq status-string
          (cond
           ((>= life 3)
            (dnd-remove-effect actor "life")
            (dnd-remove-effect actor "death")
            (concat (dnd-beautify-actor-name actor) " has stabilized.  Death will have to wait."))

           ((>= death 3)
            (dnd-remove-effect actor "life")
            (dnd-remove-effect actor "death")
            (dnd-add-effect actor "dead")
            (concat (dnd-beautify-actor-name actor) " has worsened and **D I E D**."))

           (t
            (concat (dnd-beautify-actor-name actor)
                    (if (or (equal result :crit) (equal result :good))
                        " has improved somewhat."
                      " has worsened.")))))

    (dnd-clear-state (dnd-get-state :actor))
    
    `(,@(dnd-5e-chat-helper :death-save
                            (format "***%s** %s*\n\n\t\t_%s_"
                                    result-string
                                    (dnd-roll-to-string dice-roll)
                                    status-string)))))

(defdnd-priority dnd-5e-contest (actor actor-skill target target-skill)
  (:call (t . (actor-and-statblock-source skill-source actor-and-statblock-source skill-source)))
  (let ((actor (format "%s" actor))
        (target (format "%s" target)))
    
    `((skill-check ,target ,target-skill :store)
      (roll-to-string
       :last
       ,(format "%s DC" 
                (dnd-beautify-actor-name
                 (dnd-get-actor-public-name (dnd-get-actor-state target)))))
      (skill-check ,actor ,actor-skill :last))))

(defdnd dnd-5e-challenge-success ()
  (:call (t))
  (dnd-combat-state-set
   "challenge-success"
   (dnd-inc-state :challenge-success)))

(defdnd dnd-5e-challenge-failure ()
  (:call (t))
  (dnd-combat-state-set
   "challenge-failure"
   (dnd-inc-state :challenge-failure)))

(defdnd dnd-5e-challenge-complete ()
  (:call (t))
  (dnd-set-state :challenge-success nil)
  (dnd-set-state :challenge-failure nil)
  (dnd-combat-state-set "challenge-success" nil)
  (dnd-combat-state-set "challenge-failure" nil))

(defdnd dnd-5e-set (&rest pairs)
  (while pairs
    (let ((key (pop pairs))
          (value (pop pairs)))
      (dnd-set-state key value))))

(defdnd dnd-5e-set-crit ()
  (:call (t))
  (dnd-set-state :crit t))

(defdnd dnd-5e-clear-crit ()
  (:call (t))
  (dnd-set-state :crit nil))

(defdnd dnd-5e-crit-threshold (threshold)
  (:call (custom-integer-entry-source))
  (dnd-set-state :crit-threshold threshold))

(defdnd dnd-5e-roll-check (threshold &optional comment)
  (:call (t . (custom-integer-entry-source)))
  (let* ((dice (dnd-roll-perform (dnd-get-state :roll)))
         (threshold (dnd-5e--dc-arg threshold))
         (roll (dnd-roll-result dice (dnd-roll-has-disadvantage)))
         (suspense-tag (if (dnd-get-state :suspense) "||" ""))
         result)

    (setq dnd-last-roll-result dice)
    (setq result (>= roll threshold))

    (prog1
        `(,@(dnd-5e-chat-helper
             :roll-quiet
             (format "%s:  %s%s = %s [%s]%s"
                     comment
                     suspense-tag
                     (dnd-roll-to-string dice)
                     roll
                     (if result
                         "SUCCESS"
                       "FAIL")
                     suspense-tag)))
                     
      (dnd-clear-state (dnd-get-state :actor))
      (dnd-set-state :success result))))

(defdnd dnd-5e-roll (dice &optional label)
  (:call (t . (dice-source roll-label-source)))
  (let* ((roll (dnd-roll-perform dice))
         (result (dnd-roll-result roll))
         (suspense-tag (if (dnd-get-state :suspense) "||" ""))
         (label (if label (format "%s " label) "")))
    (dnd-set-state :roll roll)
    (setq dnd-last-roll-result roll)
    `(,@(dnd-5e-chat-helper
         :roll-quiet (format "***[%sroll]** %s%s = %s%s*"
                             label
                             suspense-tag
                             (dnd-roll-to-string roll)
                             result
                             suspense-tag)))))

(defdnd dnd-5e-store-roll ()
  (:call (t))
  (let ((roll (dnd-get-state-number :roll))
        (suspense (dnd-get-state :suspense)))
    (dnd-clear-state (dnd-get-state :actor))
    (dnd-set-state :last-roll (dnd-roll-perform roll))
    (dnd-set-state :suspense suspense)
    nil))

(defun dnd-5e--dc-arg (dc)
  (pcase dc
    (:store dc)
    (_
     (dnd-roll-result
      (dnd-roll-perform 
       (pcase dc
         (:dc
          (dnd-get-state :dc))
         (:last
          (dnd-get-state :last-roll))
         (_ dc)))))))

(defdnd-priority dnd-5e-set-dc (dc)
  (:call (t . (difficulty-source)))
  (prog1 nil
    (dnd-set-state
     :dc
     (pcase dc
       (:roll
        (dnd-get-state :roll))
       
       (:last
        (dnd-get-state :last-roll))
       
       (_
        (dnd-roll-parse dc))))))

(defdnd dnd-5e-dc-modifier (amount)
  (:call (t . (dice-and-roll-modifier-source)))
  (prog1 nil
    (dnd-append-state :dc (dnd-roll-parse amount))))

(defdnd dnd-5e-roll-to-string (which &optional label)
  (:call (t . (:roll))
         (last-roll-to-string . (:last-roll)))
  (let ((roll (dnd-get-state
               (pcase which
                 (:last :last-roll)
                 (_ :roll))))
        (suspense-tag (if (dnd-get-state :suspense) "||" "")))
    `(,(format "%s: %s%s = %s%s"
               (or label "roll")
               suspense-tag
               (dnd-roll-to-string roll)
               suspense-tag
               (dnd-roll-result roll)))))

(defdnd dnd-5e-immune ()
  (:call (t))
  (dnd-set-state :immunity t))

(defdnd dnd-5e-resistant ()
  (:call (t))
  (dnd-set-state :resistant t))

(defdnd dnd-5e-vulnerable ()
  (:call (t))
  (dnd-set-state :vulnerable t))

(defdnd dnd-5e-undead ()
  (:call (t))
  (dnd-set-state :undead t))

(defdnd dnd-5e-silvered ()
  (:call (t))
  (dnd-set-state :silvered t))

(defdnd dnd-5e-magical ()
  (:call (t))
  (dnd-set-state :magical t))

(defdnd dnd-5e-suspense ()
  (:call (t))
  (dnd-set-state :suspense t))

(cl-defun dnd-5e-damage-result (roll &key immunities resistances vulnerabilities magical-or-silver reductions)
  (cl-loop 
   with force-immune = (dnd-get-state :immunity)
   with force-resistant = (dnd-get-state :resistant)
   with force-vulnerable = (dnd-get-state :vulnerable)
   with all = (dnd-roll-collect-label-groups roll)
   for group being the hash-keys of all sum

   (let* ((raw-result (dnd-roll-result (gethash group all)))
          (damage-type (when group (substring group 0 -1)))
          (physical (cl-member damage-type '("piercing" "slashing" "bludgeoning") :test 'string=))
          (damage
           (cond
            ((or force-immune
                 (and (not magical-or-silver)
                      physical
                      (cl-member "non-magical-or-silver" immunities :test 'string=))
                 (cl-member damage-type immunities :test 'string=))
             0)
            ((or force-resistant
                 (cl-member damage-type resistances :test 'string=)
                 (and (not magical-or-silver)
                      physical
                      (cl-member "non-magical-or-silver" resistances :test 'string=)))
             (/ raw-result 2))
            ((or force-vulnerable
                 (cl-member damage-type vulnerabilities :test 'string=)
                 (and magical-or-silver
                      physical
                      (cl-member "magical-or-silver" vulnerabilities :test 'string=)))
             (* raw-result 2))
            (t
             raw-result))))
     
       ;;
       (--if-let (and reductions damage-type (dnd-query-data reductions damage-type))
           (max 0 (- damage (string-to-number it)))
         damage))))

(defun dnd-5e-apply-damage-scale (roll scale)
  (if scale
      (max 1
           (floor
            (* roll 
               (cond
                ((equal scale :half) 0.5)
                ((equal scale :double) 2)
                (t 1)))))
    roll))

(cl-defun dnd-5e-modify-hp (target dice &key scale chat type)
  (let* ((target (format "%s" target))
         (target-state (dnd-get-actor-state target))
         (target-data (dnd-get-actor-data target))
         (target-immunities (dnd-query-data target-data "immune"))
         (target-resistances (dnd-query-data target-data "resistant"))
         (target-vulnerabilities (dnd-query-data target-data "vulnerable"))
         (target-reductions (dnd-query-data target-data "damage-reductions"))
         (is-dummy (dnd-query-data target-state "dummy"))
         (is-party-member (dnd-actor-state-party-memberp target-state))
         (hp-data (dnd-parse-hp-field (dnd-query-data target-state "hp")))
         (hp (car hp-data))
         (temp-hp (or (cadr hp-data) 0))
         (max-hp (string-to-number (dnd-query-data target-state "max")))
         (magical (dnd-get-state :magical))
         (silvered (dnd-get-state :silvered))
         (suspense-tag (if (dnd-get-state :suspense) "||" ""))
         (dice (dnd-roll-perform dice))
         (raw-roll (dnd-roll-result dice))
         (roll (dnd-5e-apply-damage-scale
                (dnd-5e-damage-result dice
                                      :immunities target-immunities
                                      :resistances target-resistances
                                      :vulnerabilities target-vulnerabilities
                                      :magical-or-silver (or magical silvered)
                                      :reductions target-reductions)
                scale))
         (new-hp-data (dnd-process-hp hp temp-hp roll type))
         (new-hp (car new-hp-data))
         (new-temp-hp (cadr new-hp-data))

         (type-string
          (cond
           ((equal type :dmg) "damage")
           ((equal type :heal) "healing")
           ((equal type :temp) "temp hp")))

         (scale-string
          (cond
           ((equal scale :half) "[halved]")
           ((equal scale :double) "[doubled]")
           (scale (format "%s" scale))))

         (hp-delta-string (format "***[%s hp]**  %s%s -> %s%s\t(%s %s%s)*" target
                                  (if (> temp-hp 0) (format "(%s)" temp-hp) "")
                                  hp
                                  (if (> new-temp-hp 0) (format "(%s)" new-temp-hp) "")
                                  new-hp
                                  roll type-string
                                  (if scale-string (concat " " scale-string) "")))
         
         (report-string (format "***[%s roll]**  %s%s = %s%s*"
                                type-string
                                suspense-tag
                                (dnd-roll-to-string
                                 (dnd-roll-group-labels dice))
                                raw-roll
                                suspense-tag)))

    (when (equal chat :no-roll)
      (dnd-invert-chat :roll))

    (prog1
        (cl-remove
         nil
         `(,@(dnd-5e-chat-helper :roll report-string (when is-party-member :party) hp-delta-string)
           ,@(unless is-dummy
               `((record-hp ,(dnd-to-symbol target) :hp ,(max 0 (min max-hp new-hp)) :tmp ,new-temp-hp)
                 ,(when (and (equal type :dmg) (>= (/ roll max-hp) 0.75))
                    "[Massive damage]")
                 (clear-cleave)))))

      (dnd-clear-state (dnd-get-state :actor))
      (when (and (<= new-hp 0)
                 (>= raw-roll max-hp))
        (dnd-set-state :cleave-attack (dnd-roll-result dnd-last-roll-result))
        (dnd-set-state :cleave-dmg (dnd-roll-to-spec-string (dnd-roll-reduce dice max-hp))))
      (setq dnd-last-roll-result dice))))

(defdnd dnd-5e-dmg (target dice &key type scale chat)
  (:call (t . (target-source damage-dice-source :chat t))
         (dmg-quiet . (target-source damage-dice-source :chat :no-roll))
         (dmg-half . (target-source damage-dice-source :scale :half :chat :t))
         (dmg-half-quiet . (target-source damage-dice-source :scale :half :chat :no-roll)))
  (let* ((target (format "%s" target))
         (spec (append (dnd-roll-parse dice)
                       (dnd-get-state :dmg)))
         (crit (dnd-get-state :crit))
         (dice (cond
                ((equal dice :roll)
                 (dnd-get-state :roll))
                ((equal dice :last)
                 (dnd-get-state :last-roll))
                (crit
                 (dnd-roll-crit-spec spec))
                (t
                 spec))))
    (dnd-5e-modify-hp target dice :scale scale :chat chat :type :dmg)))

(defdnd dnd-5e-heal (target dice &key chat)
  (:call (t . (target-source dice-source :chat t))
         (heal-quiet . (target-source dice-source :chat :no-roll))
         ("potion: healing" . (target-source 2d4+2 :chat t))
         ("potion: greater healing" . (target-source 4d4+4 :chat t))
         ("potion: superior healing" . (target-source 8d4+8 :chat t))
         ("potion: supreme healing" . (target-source 10d4+20 :chat t)))
  (dnd-5e-modify-hp (dnd-to-symbol target) dice :type :heal :chat chat))

(defdnd dnd-5e-temp-hp (target amount &key chat)
  (:call (t . (target-source dice-source :chat t))
         (temp-hp-quiet . (target-source dice-source :chat :no-roll)))
  (dnd-5e-modify-hp (dnd-to-symbol target) amount :type :temp :chat chat))

(defdnd dnd-5e-record-hp (actor &key hp tmp)
  (:call (t . (target-source :hp custom-integer-entry-source :tmp custom-integer-entry-source)))
  (let* ((actor (format "%s" actor))
         (row (dnd-table-find-row "Actors" `("*" ,actor))))
    
    (if (<= row 1)
        (format "ERROR: row %s is bad" row)

      (when (= hp 0)
        (dnd-remove-actor-from-attack-histories actor))
      
      (dnd-table-set-field "Actors" row "hp"
                           (concat
                            (int-to-string hp)
                            (when (and tmp (> tmp 0))
                              (concat "," (int-to-string tmp)))))
      (dnd-clear-state (dnd-get-state :actor)))))

(defdnd dnd-5e-cleave (actor-list)
  (:call (t . (multi-actor-source)))
  `(,@(cl-loop
       with attack = (dnd-get-state :cleave-attack)
       with dmg = (dnd-roll-perform (dnd-get-state :cleave-dmg))
       for actor in actor-list
       as target-data = (dnd-get-actor-data actor)
       as target-state = (dnd-get-actor-state actor)
       as ac = (dnd-get-actor-ac target-data)
       as remaining = (dnd-roll-result dmg)
       as max-hp = (string-to-number (dnd-query-data target-state "max"))
       while (and attack dmg (>= attack ac) (>= remaining 0)) append
       (prog1
           (dnd-5e-modify-hp actor dmg :chat :no-roll :type :dmg)
         (setq dmg (dnd-roll-reduce dmg max-hp))))))

(defdnd dnd-5e-clear-cleave ()
  (:call (t))
  (dnd-set-state :cleave-attack nil)
  (dnd-set-state :cleave-dmg nil))

(defdnd dnd-5e-add-condition (target status &optional count)
  (:call (t . (actor-source condition-source)))
  (dnd-add-effect (format "%s" target) (prin1-to-string status) count))

(defdnd dnd-5e-remove-condition (target status &optional count)
  (:call (t . (actor-source condition-source)))
  (dnd-remove-effect (format "%s" target) (prin1-to-string status) count))

(defdnd dnd-5e-initiative (&optional new-only)
  (:call (t)
         ("initiative: new only" . (t)))
  (let* ((groups '())
         (state-actors (dnd-table-to-assoc "Actors"))
         (actions '())
         (existing (make-hash-table :test 'equal)))

    ;; On new-only, populate a list of existing values
    (when new-only
      (cl-loop for entry in state-actors do
               (let ((ref (dnd-query-data entry "ref"))
                     (init (dnd-query-data entry "init")))
                 (when init
                   (puthash ref (string-to-number init) existing)))))

    (cl-loop
     for entry in state-actors
     as ref = (dnd-query-data entry "ref")
     as char = (car entry)
     as skip = (or (when new-only (dnd-query-data entry "init"))
                   (dnd-actor-name-special-p char))
     as actor-or-group = (or ref (car entry))
     as data = (dnd-get-actor-data actor-or-group)

     unless (or skip (member actor-or-group groups)) do

     (progn
       (push actor-or-group groups)
       (setq actions
             (append actions
                     (-non-nil
                      (--if-let (gethash actor-or-group existing)
                          `((set-roll ,it))
                        `((stat-bonus ,(dnd-to-symbol actor-or-group) dex)
                          ,(--when-let (dnd-query-data data "initiative")
                             `(roll-modifier ,(string-to-number it) "initiative bonus"))
                          (d20))))
                     `((record-initiative ,(dnd-to-symbol actor-or-group)))))))

    (append actions '((sort-initiative)))))

(defdnd dnd-5e-record-initiative (actor-or-group)
  (let* ((actor-or-group (format "%s" actor-or-group))
         (group-row (dnd-table-find-row "Actors" `("*" "*" "*" "*" "*" ,actor-or-group)))
         (roll (dnd-roll-result (dnd-roll-perform (dnd-get-state :roll))))
         (val (number-to-string roll)))

    (if group-row
        (while group-row
          (dnd-table-set-field "Actors" group-row "init" val "!" "")
          (setq group-row (dnd-table-find-row "Actors" `("*" "*" "*" "*" "*" ,actor-or-group) group-row)))

      ;; Search for actor
      (--if-let (dnd-table-find-row "Actors" `("*" ,actor-or-group))
          (dnd-table-set-field "Actors" it "init" val "!" "")))
    
    (dnd-clear-state (dnd-get-state :actor))
    `(,(concat "initiative: " val))))

(defdnd dnd-5e-sort-initiative ()
  (:call (t))
  (dnd-set-state :round 1)
  (dnd-sort-actors-initiative))

(defdnd dnd-5e-chase (actor quarry distance)
  (:call (t . (actor-source target-source custom-integer-entry-source)))
  (let* ((actor (format "%s" actor))
         (quarry (format "%s" quarry))
         (distance (* -1 distance))
         (row (dnd-table-find-row "Chases" `("*" ,quarry)))
         pursuers)

    (if (not row)
        (dnd-table-add-row "Chases" `("" ,quarry ,(format "%s:0, %s:%s" quarry actor distance)))

      (setq pursuers
            (cl-acons actor (number-to-string distance)
                      (dnd-parse-compact-field (dnd-query-data
                                                (dnd-table-get-row "Chases" row)
                                                "*pursuers"))))
      (prog1 nil
        (save-excursion
          (dnd-table-goto "Chases" t)
          (org-table-put row 3
                         (dnd-convert-compact-field
                          (dnd-sort-compact-by-numeric-value pursuers))
                         t))))))

(defdnd dnd-5e-chase-completed (quarry)
  (:call (t . (chase-source)))
  (prog1 nil
    (dnd-table-kill-rows "Chases" `("" ,(format "%s" quarry)))))

(defdnd dnd-5e-chase-record (actor quarry travel)
  (let* ((actor (format "%s" actor))
         (quarry (format "%s" quarry))
         (row (dnd-table-find-row "Chases" `("*" ,quarry)))
         pursuers)

    (if (not row)
        '("No such quarry")

      (setq pursuers
            (dnd-parse-compact-field (dnd-query-data
                                      (dnd-table-get-row "Chases" row)
                                      "*pursuers")))

      (setq pursuers
            (cl-loop for (k . v) in pursuers collect
                     (if (string= k actor)
                         (cons actor
                               (number-to-string
                                (+ (string-to-number v) travel)))
                       (cons k v))))
      (prog1 nil
        (save-excursion
          (dnd-table-goto "Chases" t)
          (org-table-put row 3
                         (dnd-convert-compact-field
                          (dnd-sort-compact-by-numeric-value pursuers))
                         t))))))

(defdnd dnd-5e-chase-move (actor quarry &optional dash)
  (:call (t . (actor-source chase-source chase-dash-soucre chase-advantage-source)))
  (let* ((actor (format "%s" actor))
         (quarry (format "%s" quarry))
         (dash-count (dnd-get-effect-count actor "dashed"))
         (con-bonus (dnd-ability-modifier (string-to-number (dnd-query-static-data "monsters" actor "con"))))
         (check-exhaustion (> dash-count (+ 3 con-bonus)))
         (dashing (equal dash :dash))
         travel)
    
    (setq travel
          (* (if dashing 2 1)
             (string-to-number (dnd-query-static-data "monsters" actor "speed"))))

    (cl-remove-if nil
                  (append
                   `((chase-record ,(dnd-to-symbol actor) ,(dnd-to-symbol quarry) ,travel))
                   
                   (when dashing
                     `((add-condition ,(dnd-to-symbol actor) dashed)))

                   (when check-exhaustion
                     `((saving-throw ,(dnd-to-symbol actor) con 10)
                       (on-failure
                        (add-condition ,(dnd-to-symbol actor) exhausted))
                       (on-success)))))))

(defdnd-priority dnd-5e-roll-force ()
  (:call (t))
  `((set-roll ,@dnd-last-roll-result)))

(defdnd dnd-5e-set-roll (&rest roll-forms)
  (prog1 nil
    (dnd-set-state :roll roll-forms)))

(defdnd-priority dnd-5e-live-roll (&optional dice)
  (:call (t)
         (live-roll-set . (dice-source)))
  (prog1 nil
    (dnd-set-state :live-roll t)
    (when dice
      (dnd-set-state :roll (dnd-roll-parse dice)))))

(defdnd dnd-5e-lucky ()
  (:call (t))
  `((set-roll
     ,@(cl-loop for part in dnd-last-roll-result collect
               (if (and (listp (car part)) (equal (cdr part) 20))
                   (cons
                    (let (found)
                      (cl-loop for old in (car part) collect
                               (if (> old 1)
                                   old
                                 (if found 
                                     old
                                   (setq found t)
                                   (dnd-roll-die 20)))))
                    20)
                 part)))))

(defun dnd-5e--caster-dc (caster-data &optional stat)
  (let ((casting-stat
         (--when-let
             (or stat (dnd-query-data caster-data "casting"))
           (format "%s" it))))
    (when casting-stat
      (+ 8
         (dnd-get-actor-proficiency-bonus caster-data)
         (dnd-ability-modifier
          (string-to-number
           (dnd-query-data caster-data casting-stat)))))))

(defdnd-priority dnd-5e-damage-area (targets dmg save dc-or-castor &rest spell-data)
  (:call
   (t . (multi-actor-source damage-dice-source stat-source difficulty-source))
   ("*spell damage-area" . (multi-actor-source damage-dice-source stat-source actor-source)))

  (let* ((actor-name (when (not (numberp dc-or-castor))
                       (format "%s" dc-or-castor)))
         (actor-data (when actor-name (dnd-get-actor-data actor-name)))
         (spell-name (plist-get spell-data :name))
         (casting-stat (plist-get spell-data :casting))
         (dc (if (numberp dc-or-castor)
                 dc-or-castor
               (dnd-5e--caster-dc actor-data casting-stat))))

    (cl-remove
     nil
     `(,(when (and actor-name spell-name)
          `(chat ,(format
                   "***%s*** casts *%s*"
                   (dnd-beautify-actor-name actor-name)
                   (dnd-beautify-actor-name spell-name))))
       
       (invert-chat :roll-quiet)
       (roll ,dmg "damage")
       (clear-chat :roll-quiet)
       (store-roll)
       (foreach ,(if (numberp dc-or-castor)
                     targets
                   (cl-remove dc-or-castor targets :test 'string=))
                (set-dc ,dc)
                (saving-throw :actor ,save :dc)
                (on-failure
                 (dmg :actor :last :chat :no-roll)
                 (else
                  (dmg :actor :last :scale :half :chat :no-roll))))))))

;;;; Spells
(defdnd-priority dnd-5e-spell-attack-handler (actor target damage &rest spell-data)
  (let* ((bonus-dmg (plist-get spell-data :bonus-dmg))
         (stat (plist-get spell-data :casting))
         (label (dnd-beautify-actor-name (plist-get spell-data :name))))
    (cl-remove
     nil
     `((spell-attack ,actor ,target :casting ,stat :label ,label)
       ,(cl-remove
         nil
         `(on-success
           (dmg-modifier ,damage)
           ,(when bonus-dmg
              `(dmg-modifier ,bonus-dmg))
           (dmg-to-roll)
           ,(when (dnd-get-state :live-roll)
              '(capture))
           (dmg ,target :roll :chat t)))
       :clear))))

(setq dnd-spell-handlers
      `((fireball . (damage-area multi-actor-source "fire: 8d6" dex :actor))
        (thunderwave . (damage-area multi-actor-source "thunder: 2d8" con :actor))
        ("aganazzar's scorcher" . (damage-area multi-actor-source "fire: 3d8" dex :actor))
        ("burning hands" . (damage-area multi-actor-source "fire: 3d6" dex :actor))
        ("eldritch blast" . (spell-attack-handler :actor target-source "force: 1d10"))))

;;;; Feats
(defdnd-priority dnd-5e-feat-weapon-master ()
  (:call ("*feat great weapon master")
         ("*feat sharpshooter"))
  '((roll-modifier -5 "Weapon Master")
    (dmg-modifier 10 "Weapon Master")))

(defdnd-priority dnd-5e-feat-inspiring-leader (actor targets)
  (:call ("*feat inspiring leader" . (actor-source multi-actor-source)))
  (let* ((actor (format "%s" actor))
         (actor-data (dnd-get-actor-data actor))
         (actor-level (string-to-number (dnd-query-data actor-data "level")))
         (char-bonus (dnd-ability-modifier (string-to-number (dnd-query-data actor-data "char"))))
         (amount (+ actor-level char-bonus)))

    `((foreach ,targets
               (temp-hp :actor ,amount :chat t)))))

(defdnd-priority dnd-5e-feat-defensive-duelist (actor)
  (:call ("*feat defensive duelist" . (actor-source)))
  (let* ((actor (format "%s" actor))
         (bonus (dnd-get-actor-proficiency-bonus actor)))
    `((dc-modifier ,bonus))))

(defdnd-priority dnd-5e-feat-martial-adept-dc (actor)
  (:call ("*feat martial adept (dc)" . (actor-source))
         ("*battlemaster maneuver (dc)" . (actor-source)))
  (let* ((actor (format "%s" actor))
         (dex-bonus (dnd-ability-modifier (string-to-number (dnd-query-static-data "monsters" actor "dex"))))
         (str-bonus (dnd-ability-modifier (string-to-number (dnd-query-static-data "monsters" actor "str"))))
         (stat-bonus (max dex-bonus str-bonus))
         (prof (dnd-get-actor-proficiency-bonus actor)))
    `((set-dc ,(+ 8 prof stat-bonus)))))

(defdnd-priority dnd-5e-feat-linguist-dc (actor)
  (:call ("*feat linguist (dc)" . (actor-source)))
  (let* ((actor (format "%s" actor))
         (int (string-to-number (dnd-query-static-data "monsters" actor "dex")))
         (prof (dnd-get-actor-proficiency-bonus actor)))
    `((set-dc ,(+ int prof)))))

(defdnd-priority dnd-5e-feat-shield-master ()
  (:call ("*feat shield master (dc mod)"))
  `((dc-modifier -2)))

;;;;;;
(provide 'dnd-commands)
