(defvar dnd-command-priorities '())
(setq dnd-command-priorities '())

(defvar-local dnd-master-buffer nil)
(defvar-local dnd-current-state '())
(defvar-local dnd-requested-data nil)
(defvar-local dnd-actor-modifiers-active nil)
(defvar-local dnd-attack-history nil)
(defvar-local dnd-queue-marker nil)
(defvar-local dnd-queue-area nil)
(defvar-local dnd-queue-history nil)
(defvar-local dnd-static-data nil)
(defvar-local dnd-chat-type nil)
(defvar-local dnd-chat-url nil)
(defvar-local dnd-chat-channel nil)
(defvar-local dnd-chat-server-buffer nil)
(defvar-local dnd-overland-server nil)
(defvar-local dnd-overland-apikey nil)
(defvar-local dnd-overland-ws nil)
(defvar-local dnd-overland-party-maps nil)
(defvar-local dnd-last-roll-result nil)
(defvar-local helm-dnd-last-actor-selected nil)
(defvar dnd-custom-command-registry nil)
(defvar dnd-spell-handlers nil)
(defvar helm-dnd-buffer nil)
(defvar helm-dnd-current-buffer nil)
(defvar dnd-last-master-loaded nil)

(cl-defstruct dnd-actor-data data mods)
(cl-defstruct dnd-roll-label label)

(defcustom dnd-queue-status-hook nil
  "Hook run on queue status line update"
  :group 'dnd
  :type 'hook)

;;;;;;;;;;;;;;;;;;;;;;;;;
; Cosmetics
(defvar dnd-queue-status-sep "")

(defface dnd-queue-status-title
  '((t
     :background "grey24"
     :foreground "grey1"
     :weight extra-bold
     :slant italic
     :box nil))
  "Face for status area title"
  :group 'dnd-faces)

(defface dnd-queue-status-key
  '((t :foreground "#F92672" :height 1.3))
  "Face for state key names in queue status line."
  :group 'dnd-faces)

(defface dnd-queue-status-flag-key
  '((t :underline t :inherit 'dnd-queue-status-key))
  "Face for state key names that only have a value of `t' in queue status line."
  :group 'dnd-faces)

(defface dnd-queue-status-value
  '((t))
  "Face for state key names in queue status line."
  :group 'dnd-faces)

(defface dnd-queue-error
  '((t :foreground "yellow1"))
  "Face for queue resolution errors."
  :group 'dnd-faces)

(defface dnd-queue-status-modifier-name
  '((t :foreground "#54ff9f" :height 1.1))
  "Face for actor modifier names in queue status line."
  :group 'dnd-faces)

(defface dnd-history-command-name
  '((t :box t))
  "Face for commands in history log."
  :group 'dnd-faces)

(defface dnd-actor-note
  '((t :slant italic))
  "Face for actor notes in helm interface."
  :group 'dnd-faces)
;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;  Command definition utilities
(defun dnd-register-command (entries)
  (unless dnd-custom-command-registry
    (setq dnd-custom-command-registry (make-hash-table :test 'equal)))

  (cl-loop for (label . cmd) in entries do
           (puthash label (car cmd) dnd-custom-command-registry)))

(cl-defun defdnd--process-command-alias (form &rest replacements)
  (cl-loop for el in form collect
           (let ((el (or (plist-get replacements el) el))
                 (alt-symbol
                  (car
                   (read-from-string
                    (concat "helm-dnd-" (prin1-to-string el))))))
             (cond
              ((or (stringp el) (numberp el))
               el)

              ((listp el)
               (eval el))
              
              ((boundp alt-symbol)
               (eval alt-symbol))

              ((boundp el)
               (eval el))

              (t
               el)))))

(defmacro defdnd--internal (macro-type priority name signature &rest forms)
  "Internal function for creating commands"
  (let (full-name
        priority-data
        doc
        registrations)

    ;; :internal definitions have the name unmangled.
    (if (equal macro-type :internal)
        (setq full-name name
              name (car (read-from-string
                         (replace-regexp-in-string
                          "^dnd-5e-" ""
                          (prin1-to-string name)))))
      (setq full-name (car (read-from-string (concat "dnd-5e-" (prin1-to-string name))))))

    (when (stringp (car forms))
      (setq doc (pop forms)))

    (let ((top (car forms))
            arg-symbols)

        (when (and (listp top) (equal (car top) :call))
              (pop forms)

              ;; Assumes (entry-name . data)
              ;; .or `(t)' for an auto-entry
              ;; .`t' can be specified as the entry name and the short name of the function will be used.
              (cl-loop for (label . data) in (cdr top) do
                       (setq arg-symbols (defdnd--process-command-alias data))
                       (when (equal label t)
                         (setq label name))

                       ;; Add this call definition to the command registry
                       (push
                        `(,(if (stringp label) label (prin1-to-string label))
                          ,(if arg-symbols
                               (cons name arg-symbols)
                             `(,name)))
                        registrations))))

    (when forms
      (when (equal priority :priority)
        (push `(,name . 1) dnd-command-priorities))
      `(progn
         (cl-defmacro ,full-name ,signature ,doc ,@forms)
         (eval-and-compile
           (dnd-register-command ',registrations))))))

(defmacro defdnd (name signature &rest forms)
  "Defines a lisp macro that can be called from the dnd-mode queue."
  `(defdnd--internal :internal :normal ,name ,signature ,@forms))

(defmacro defdnd-priority (name signature &rest forms)
  "Defines a lisp macro that can be called from the dnd-mode queue and has priority during queue expansion."
  `(defdnd--internal :internal :priority ,name ,signature ,@forms))

(defmacro defdnd-custom (name signature &rest forms)
  ""
  `(defdnd--internal :custom :normal ,name ,signature ,@forms))

(defmacro defdnd-custom-priority (name signature &rest forms)
  ""
  `(defdnd--internal :custom :priority ,name ,signature ,@forms))

(defmacro defdnd-command-alias (&rest forms)
  ""
  (let ((registrations
         (cl-loop for el in forms collect
                  (cond
                   ((listp el)
                    (list (format "%s" (car el)) (cdr el)))
                   (t
                    (list (format "%s" el) el))))))
      `(progn
         (eval-and-compile
           (dnd-register-command ',registrations)))
    ))

;; Note:  These are needed in order to properly debug with edebug.  Without them edebug-eval-top-level-form/edebug-defun won't be able to properly instrument things defined with the `defdnd', `defdnd-priority', `defdnd-custom', `defdnd-custom-priority' macros.
(def-edebug-spec defdnd
  (&define name cl-macro-list
           cl-declarations-or-string
           [&optional (":call" &rest sexp)]
           def-body))

(def-edebug-spec defdnd-priority
  (&define name cl-macro-list
           cl-declarations-or-string
           [&optional (":call" &rest sexp)]
           def-body))

(def-edebug-spec defdnd-custom
  (&define name cl-macro-list
           cl-declarations-or-string
           [&optional (":call" &rest sexp)]
           def-body))

(def-edebug-spec defdnd-custom-priority
  (&define name cl-macro-list
           cl-declarations-or-string
           [&optional (":call" &rest sexp)]
           def-body))

;;;
(provide 'dnd-def)
