(defun helm-dnd-list-tables ()
  (interactive)
  (helm :sources `(,(helm-build-sync-source "Tables"
                        :candidates (dnd-get-org-metadata "name" t)
                        :action (lambda (pos) (goto-char pos))))))

;;;;;;;;;;;;;;;;;;;;
(setq helm-dnd-custom-entry-source
      (helm-build-dummy-source "Custom" :action '(("Custom" .
                                                   (lambda (candidate)
                                                     (if (s-match "^[ ]*[0-9]+[ ]*$" candidate)
                                                         (string-to-number candidate)
                                                       candidate))))))

(setq helm-dnd-roll-label-source
      (helm-build-dummy-source "Label"
        :action '(("Use" .
                   (lambda (candidate)
                     ;; Hack:  Just want an empty value when nothing is selected
                     ;; .Helm seems to return the source name in those cases
                     (if (string= "Label" candidate) nil candidate))))))
                     
      
(setq helm-dnd-custom-integer-entry-source
      (helm-build-dummy-source "Custom"
        :action '(("Custom" . 
                   (lambda (candidate)
                     (string-to-number candidate))))))

(setq helm-dnd-select-actor
      `(("Use" .
         ,(lambda (el)
            (prog1 el
              (let ((source-name (cdr (assoc 'name (helm-get-current-source)))))
                (when (or (equal source-name "Actors")
                          (equal source-name "Current"))
                  (with-current-buffer helm-dnd-buffer
                    (setq helm-dnd-last-actor-selected el)))))))
        ("Insert" . insert)))

(cl-defun helm-dnd--get-valid-actor-entries ()
  (--remove
   (dnd-actor-name-special-p it)
   (dnd-get-table-raw "Actors" "@2$char..@>$char")))

(setq helm-dnd-actor-source
      `(,(helm-build-sync-source "Current"
           :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                    (--when-let (dnd-get-state :actor)
                                      (unless (dnd-actor-name-special-p it)
                                        `(,it)))))
           :action helm-dnd-select-actor)
        
        ,(helm-build-sync-source "Actors"
           :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                    (helm-dnd--get-valid-actor-entries)))
           :action helm-dnd-select-actor)
        
        ,(helm-build-sync-source "Special"
           :candidates '(:actor))))

(setq helm-dnd-multi-actor-source
      `(,(helm-build-sync-source "Actors"
           :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                    (helm-dnd--get-valid-actor-entries)))
           :action '(("Use" . (lambda (candidate)
                                (cl-loop for actor in (helm-marked-candidates :all-sources t) collect
                                         (format "%s" actor))))))))

(setq helm-dnd-statblock-source
      `(,(helm-build-sync-source "Stat Blocks"
           :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                    (cl-loop for n being hash-keys of (dnd-query-static-data "monsters") collect n))))))

(setq helm-dnd-multi-statblock-source
      `(,(helm-build-sync-source "Stat Blocks"
           :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                    (cl-loop for n being hash-keys of (dnd-query-static-data "monsters") collect n)))
           :action '(("Use" . (lambda (candidate)
                                (cl-loop for block in (helm-marked-candidates :all-sources t) collect
                                         (format "%s" block))))))))

(setq helm-dnd-target-source
      `(,(helm-build-sync-source "Last Target"
           :candidates (lambda ()
                         (with-current-buffer helm-dnd-buffer
                           (--when-let helm-dnd-last-actor-selected
                             (cl-remove nil (list
                                             (cadr (gethash it dnd-attack-history))))))))
                         
        ,(helm-build-sync-source "Likely Targets"
           :candidates (lambda ()
                         (with-current-buffer helm-dnd-buffer
                           (--when-let helm-dnd-last-actor-selected
                             (cl-loop with results
                                      with data = (dnd-table-to-assoc "Actors")
                                      with actor = it
                                      with actor-is-player = (dnd-actor-state-party-memberp (dnd-query-data data actor))
                                      
                                      for el in data
                                      as name = (dnd-query-data el "char")
                                      as alive = (not (string= "0" (dnd-query-data el "hp")))
                                      as is-player = (dnd-actor-state-party-memberp el)

                                      do

                                      (cond 
                                       ((and actor-is-player (not is-player) alive)
                                        (push name results))
                                       ((and (not actor-is-player) is-player alive)
                                        (push name results)))
                                      finally return (cl-remove nil (reverse results)))))))

        ,(helm-build-sync-source "All Actors"
           :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                    (helm-dnd--get-valid-actor-entries))))

        ,(helm-build-sync-source "Special"
           :candidates '(:actor))))

(setq helm-dnd-weapon-source
      `(,(helm-build-sync-source "Last Weapon"
           :candidates (lambda ()
                         (with-current-buffer helm-dnd-buffer
                           (when (and helm-dnd-last-actor-selected
                                      dnd-attack-history)
                             (-non-nil
                              `(,(car (gethash
                                       helm-dnd-last-actor-selected
                                       dnd-attack-history))))))))
        
        ,(helm-build-sync-source "Actor"
           :candidates (lambda ()
                         (with-current-buffer helm-dnd-buffer
                           (--when-let helm-dnd-last-actor-selected                         
                             (let* ((actor helm-dnd-last-actor-selected)
                                    (actor-state (dnd-get-actor-state actor))
                                    (ref (dnd-query-data actor-state "ref"))
                                    (actor-data (dnd-get-actor-data (or actor ref))))
                               (when actor-data
                                 (hash-table-keys
                                  (dnd-query-data actor-data "attacks"))))))))))

(setq helm-dnd-stat-source
      `(,(helm-build-sync-source "Stat"
           :candidates '("str" "dex" "con" "int" "wis" "char"))))
                     
(setq helm-dnd-skill-source
      `(,(helm-build-sync-source "Frequently Used"
           :candidates '("perception" "athletics"))

        ,(helm-build-sync-source "Skill"
           :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                    (hash-table-keys (dnd-query-static-data "skills")))))

        ,(helm-build-sync-source "Stat"
           :candidates '("str" "dex" "con" "int" "wis" "char"))))
                     
(setq helm-dnd-difficulty-source
      `(,(helm-build-sync-source "Difficulty"
           :candidates '(("very easy (5)" . 5)
                         ("easy (10)" . 10)
                         ("medium (15)" . 15)
                         ("hard (20)" . 20)
                         ("very hard (25)" . 25)
                         ("nearly impossible (30)" . 30)))
        ,helm-dnd-custom-integer-entry-source))

(setq helm-dnd-tracking-difficulty-source
      `(,(helm-build-sync-source "Tracking Circumstance: base 10 (select multiple)"
           :candidates '(("Soft Surface (+0)" . 0)
                         ("Dirt/Grass (+5)" . 5)
                         ("Bare Stone (+10)" . 10)
                         ("Left a Trail (-5)" . -5)
                         ("1 Day Passed (+5)" . 5)
                         ("2 Days Passed (+10)" . 10)
                         ("3 Days Passed (+15)" . 15)
                         ("4 Days Passed (+20)" . 20))
           :action `(("Sum" . (lambda (el) (apply '+ (cons 10 (helm-marked-candidates :all-sources t)))))))
        ,helm-dnd-custom-integer-entry-source))

(setq helm-dnd-condition-source
      `(,(helm-build-sync-source "Condition"
           :candidates '("blinded" "charmed" "deafened" "frightened" "grappled"
                         "incapacitated" "exhausted" "petrified" "paralyzed"
                         "invisible" "poisoned" "prone" "restrained" "stunned"
                         "unconscious" "inspired" "burning"))
        ,helm-dnd-custom-entry-source))

(setq helm-dnd-dice-source
      `(,(helm-build-sync-source "Dice"
           :candidates '("1d4" "1d6" "1d8" "1d10" "1d12" "1d20" "1d100" ":last"))
        ,helm-dnd-custom-entry-source))

(defun helm-dnd-damage-dice-source-action (candidate)
  (format "%s: %s" (helm :sources helm-dnd-damage-type-source) candidate))

(setq helm-dnd-damage-dice-source
      `(,(helm-build-sync-source "Dice"
           :candidates (cdr (assoc 'candidates (car helm-dnd-dice-source)))
           :action 'helm-dnd-damage-dice-source-action)
        ,(helm-build-sync-source "Improvised Damage"
           :candidates '(("Coals, bookcase, poison needle" . "1d10")
                         ("Lightning, fire pit" . "2d10")
                         ("Tunnel collapse, acid vat" . "4d10")
                         ("Compacting walls, whirling blades, lava stream" . "10d10")
                         ("Lava submersion, crashing fortress" . "18d10")
                         ("Vortex of fire (elemental plane), godlike creature, moon-sized monster" . "24d10"))
           :action 'helm-dnd-damage-dice-source-action)
        ,(helm-build-dummy-source "Custom" :action 'helm-dnd-damage-dice-source-action)))

(setq helm-dnd-damage-type-source
      `(,(helm-build-sync-source "Damage Type"
           :candidates
            '("acid" "bludgeoning" "cold" "fire" "force" "lightning"
              "necrotic" "piercing" "poison" "psychic" "radiant"
              "slashing" "thunder"))))

(setq helm-dnd-roll-modifier-source
      `(,(helm-build-sync-source "Difficulty"
           :candidates '(("target half cover (-2)" . -2)
                         ("target 3/4 cover (-5)" . -5)
                         ("dex check: half cover (2)" . 2)
                         ("dex check: 3/4 cover (5)" . 5)))
        ,helm-dnd-custom-entry-source))

(setq helm-dnd-dice-and-roll-modifier-source
      `(,(car helm-dnd-dice-source)
        ,(car helm-dnd-roll-modifier-source)
        ,helm-dnd-custom-entry-source))

(setq helm-dnd-chase-source
      `(,(helm-build-sync-source "Chases"
         :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                  (cdr (dnd-get-table-raw "Chases" "@<$quarry..@>$quarry")))))))

(setq helm-dnd-chase-dash-source
      `(,(helm-build-sync-source "Chases"
         :candidates '(("Move" . :move) ("Dash" . :dash))
         :action (lambda (chase)
                   (list (format "%s" chase))))))

(setq helm-dnd-chase-advantage-source
      `(,(helm-build-sync-source "Escape Factors"
           :candidates '(("Quarry has many places to hide" . :advantage)
                         ("Quarry in crowded/noisy area" . :advantage)
                         ("Quarry has few places to hide" . :disadvantage)
                         ("Quarry in uncrowded/quiet area" . :disadvantage)
                         ("Lead pursuer is Ranger or has `Survival'" . :disadvantage)))))

(setq helm-dnd-actor-and-statblock-source
      (append helm-dnd-actor-source helm-dnd-statblock-source))

(setq helm-dnd-multi-actor-and-statblock-source
      (append helm-dnd-multi-actor-source helm-dnd-multi-statblock-source))

(setq helm-dnd-spells-source
      `(,(helm-build-sync-source "Spells"
           :candidates
           (lambda ()
             (with-current-buffer helm-dnd-buffer
               (cl-loop
                with actor-data = (dnd-get-actor-data helm-dnd-last-actor-selected)
                with spells = (dnd-query-data actor-data "spells")
                for level being hash-keys of spells append
                (cl-loop
                 with level-spells = (gethash level spells)
                 with names = (sort (hash-table-keys level-spells) 'string<)
                 for spell-name in names
                 as spell-data = (gethash spell-name level-spells)
                 as spell-handler = (cdr (cl-assoc spell-name dnd-spell-handlers :test 'string-equal))
                 collect
                 (cons (propertize
                        (format "%s  %s" level spell-name)
                        'font-lock-face
                        (when spell-handler 'font-lock-function-name-face))
                       `(,spell-name ,spell-handler (,@spell-data :name ,spell-name)))))))
           :action
           (lambda (candidate)
             (let ((spell-name (car candidate))
                   (spell-handler (cadr candidate))
                   (spell-data (caddr candidate)))
               (when spell-handler
                 (with-current-buffer helm-dnd-buffer
                   (helm-dnd-build-command
                    `(,@(defdnd--process-command-alias spell-handler :actor helm-dnd-last-actor-selected)
                      ,@spell-data)))))))))

(defun dnd-command-name< (a b)
  (let ((a-feat (s-matches? "*" a))
        (b-feat (s-matches? "*" b)))
      
    (cond
     ((equal a-feat b-feat)
      (string< a b))
     (t
      b-feat))))
  
(setq helm-dnd-command-source
      `(,(helm-build-sync-source "Player Commands"
           :init (lambda ()
                   (setq helm-dnd-buffer (current-buffer)))

           :candidates (lambda ()
                         (with-current-buffer helm-dnd-buffer
                           (when (and (boundp 'dnd-custom-command-registry) dnd-custom-command-registry)
                             (let* ((player (format ":%s" (dnd-get-state :actor)))
                                    (keys (sort (hash-table-keys dnd-custom-command-registry) 'string<)))
                               (cl-loop for k in keys if (s-matches? player k) collect
                                        (cons k (gethash k dnd-custom-command-registry)))))))
           
           :action (lambda (candidate)
                     (with-current-buffer helm-dnd-buffer
                       (helm-dnd-build-command candidate))))

        ,(helm-build-sync-source "Commands"
           :init (lambda ()
                   (setq helm-dnd-buffer (current-buffer)))

           :candidates (lambda ()
                         (with-current-buffer helm-dnd-buffer
                           (when (and (boundp 'dnd-custom-command-registry) dnd-custom-command-registry)
                             (let ((keys (sort (hash-table-keys dnd-custom-command-registry) 'dnd-command-name<)))
                               (cl-loop for k in keys collect
                                        (cons k (gethash k dnd-custom-command-registry)))))))
           
           :action (lambda (candidate)
                     (with-current-buffer helm-dnd-buffer
                       (helm-dnd-build-command candidate))))
        
        ,(helm-build-sync-source "Other"
           :init (lambda ()
                   (setq helm-dnd-buffer (current-buffer)))
           
           :candidates '("cast")
           
           :action (lambda (candidate)
                     (with-current-buffer helm-dnd-buffer
                       (pcase candidate
                         ("cast"
                          (--when-let (helm :allow-nest t :sources helm-dnd-actor-and-statblock-source)
                            (helm :allow-nest t :sources helm-dnd-spells-source)))))))))

(setq helm-dnd-management-command-source
      (helm-build-sync-source "Management"
        :candidates `(("Cull Monsters" . ,'dnd-cull-monsters)
                      ("Cull Dead Monsters" . ,'dnd-cull-dead-monsters)
                      ("Add Monsters" . ,'helm-dnd-monster)
                      ("Add New Actor" . ,'helm-dnd-add-actor)
                      ("Add Encounter Actor" . ,'helm-dnd-encounter-add-actor)
                      ("Run Encounter" . ,'helm-dnd-run-encounter)
                      ("Status to chat" . ,'dnd-send-actor-report-to-chat)
                      ("Goto Log" . ,'dnd-log-goto)
                      ("New Day" . ,'dnd-log-new-day)
                      ("Add to log" . ,'dnd-log-add)
                      ("Mark session in log" . ,'dnd-log-mark-session)
                      ("Log to chat" . ,'dnd-send-last-log-to-chat)
                      ("Log to clipboard" . ,'dnd-send-last-log-to-clipboard-md)
                      ("Clear history" . ,'dnd-clear-history)
                      ("Clear state" . ,'dnd-clear-state)
                      ("Hold music" . ,'dnd-send-hold-music-to-chat)
                      ("Update XP" . ,'dnd-update-party-xp-from-session-and-chat)
                      ("Award XP" . ,'helm-dnd-award-xp)
                      ("Sideline Actors" . ,'helm-dnd-sideline)
                      ("Random Table" . ,'helm-dnd-table-roll)
                      ("Chase Complication" . ,'dnd-chase-complication)
                      ("Actor Characteristics" . ,'helm-dnd-actor-characteristics)
                      ("Actor Notes" . ,'helm-dnd-actor-notes)
                      ("Add Actor Modifier" . ,'helm-dnd-add-actor-modifier)
                      ("Remove Actor Modifier" . ,'helm-dnd-remove-actor-modifier)
                      ("Remove All Modifiers" . ,'helm-dnd-remove-all-actor-modifiers)
                      ("Don Armor" . ,'helm-dnd-don-armor)
                      ("Doff Armor" . ,'helm-dnd-doff-armor)
                      ("Clear Attack/Target History" . ,'dnd-clear-attack-history)
                      ("Add Timer" . ,'helm-dnd-add-timer)
                      ("End Combat" . ,'dnd-combat-end)
                      ("Add Lair Entry" . ,(lambda () (dnd-add-special-actor-entry "lair")))
                      ("Remove Lair Entry" . ,(lambda () (dnd-remove-special-actor-entry "lair")))
                      ("Overland: Connect" . ,'dnd-overland-connect)
                      ("Overland: Disconnect" . ,'dnd-overland-disconnect)
                      ("Overland: Send Sheets" . ,'helm-dnd-overland-send-sheets)
                      ("Overland: Load Map" . ,'helm-dnd-overland-load-map))

        :action '(("Run" .
                   (lambda (candidate)
                     (funcall candidate)))
                  ("Run w/ prefix" .
                   (lambda (candidate)
                     (funcall candidate t))))))

(defun helm-dnd-build-command (template)
  (let ((starting-point (point))
        (outside (not (dnd-point-in-queue)))
        cancel
        results)

    (setq results
          (cond
           ((symbolp template)
            template)

           (t
            (cl-loop for el in template until cancel collect
                     (cond
                      ((listp el)
                       (let* ((processor (when (functionp (car el)) (car el)))
                              (this-source (if processor (cdr el) el))
                              (selection (helm :allow-nest t :sources this-source)))

                         (if (= helm-exit-status 1)
                             (setq cancel t)
                           ;;
                           (when processor
                             (setq selection (funcall processor selection)))
                           
                           (if (and (stringp selection) (not (string-match "[() ]+" selection)))
                               (make-symbol selection)
                             selection))))

                      (t
                       el))))))

    (unless cancel
      (when outside
        (dnd-goto-queue t))

      (dnd-insert-formatted-queue-sexp results)
      (dnd-queue-indent)
      
      ;; Some commands expect that further additions will be done inside of them
      (when (and (not (symbolp template)) (member (car template) '(foreach on-failure on-success else)))
        (backward-char 2)
        (insert "\n")
        (backward-char))
      
      (when outside (goto-char starting-point)))))

(defun helm-dnd-command ()
  (interactive)
  (with-current-buffer helm-dnd-buffer
    (helm :allow-nest t :sources helm-dnd-command-source)))

(defun helm-dnd-management ()
  (interactive)
  (setq helm-dnd-current-buffer (current-buffer))
  (with-current-buffer helm-dnd-buffer
    (helm :sources helm-dnd-management-command-source)))

(defun helm-dnd-weapon ()
  (interactive)
  (helm :sources helm-dnd-weapon-source))

(defun helm-dnd-set-actor ()
  (interactive)
  (helm :sources
        `(,(helm-build-sync-source "Actor"
             :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                      (dnd-get-table-raw "Actors" "@2$char..@>$char")))
             :action (lambda (candidate)
                       (dnd-clear-state candidate)
                       (dnd-update-queue-status))))))

(defun helm-dnd-monster ()
  (let ((count (helm :sources helm-dnd-dice-source)))
    (helm :sources
          `(,(helm-build-sync-source "Monsters"
               :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                        (hash-table-keys (dnd-query-static-data "monsters"))))
               :action `(("Add" . (lambda (name)
                                    (dnd-add-actor-monster name ,count)))
                         ("Link" . (lambda (name)
                                     (insert "[[elisp:(dnd-add-actor-monster" " \"" name "\" "
                                             (prin1-to-string count) ")][Add " name "]]")))
                         ("Insert" . insert)))))))

(defun helm-dnd-add-actor ()
  (interactive)
  (helm :sources
        `(,(helm-build-sync-source "Add Actor"
             :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                      (hash-table-keys (dnd-query-static-data "monsters"))))
             :action `(("Add" . (lambda (name)
                                  (dnd-add-actor name)
                                  (dnd-sideline-actor `((,name)) t)))
                       ("Link" . (lambda (name)
                                   (insert
                                    (format "[[elisp:(dnd-add-actor \"%s\")][Add %s]]" name name))))
                       ("Insert" . insert))))))

(defun helm-dnd-encounter-add-actor ()
  (interactive)
  (let ((count (helm :sources helm-dnd-dice-source)))
    (helm :sources
          `(,(helm-build-sync-source "Sheets"
               :candidates (lambda () (with-current-buffer helm-dnd-buffer
                                        (hash-table-keys (dnd-query-static-data "monsters"))))
               :action (lambda (name)
                         (with-current-buffer helm-dnd-current-buffer
                           (dnd-encounter-add-actor-entry name count)
                           (dnd-encounter-update-lethality))))))))

(defun helm-dnd-run-encounter ()
  (with-current-buffer helm-dnd-current-buffer
    (dnd-add-encounter-actors)))

(defun helm-dnd-actor-characteristics (&optional arg)
  (interactive "P")
  (let (fields)
    (when arg
      (setq fields
            (helm :sources
                  `(,(helm-build-sync-source "Actor Data"
                       :candidates '("stats" "ac" "perception" "speed" "jump")
                       :action (lambda (el) (helm-marked-candidates)))))))
    
    (helm :sources
          `(,(helm-build-sync-source "Actor Data"
               :candidates (lambda ()
                             (with-current-buffer helm-dnd-buffer
                               (let ((data (dnd-table-to-assoc "Actors")))
                                 (cl-loop for (name . entry) in data collect
                                          (format "%-15s%s" name
                                                  (dnd-actor-characteristics
                                                   (or (dnd-query-data entry "ref") name)
                                                   fields)))))))))))

(defun helm-dnd-add-timer ()
  (dnd-add-timer
   (helm :sources helm-dnd-actor-source)
   (read-from-minibuffer "Timer: ")))

(defun helm-dnd-actor-notes (&optional arg)
  (interactive "P")
  (let ((actors (dnd-table-to-assoc "Actors"))
        data)

    (helm :sources
          `(,(helm-build-sync-source "Actor Data"
               :candidates
               (lambda ()
                 (with-current-buffer helm-dnd-buffer
                   (cl-loop for (name . entry) in actors append
                            (progn
                              (setq data (dnd-get-actor-data name))
                              (--when-let (dnd-query-data data "note")
                                (cl-loop for note in it collect
                                         (dnd-propertize-note name note))))))))))))

(defun dnd-propertize-note (actor note)
  (format "%s\t\t%s"
          (propertize 
           (concat ":" name)
           'font-lock-face 'font-lock-function-name-face)
          (propertize note 'font-lock-face 'dnd-actor-note)))

(defun dnd-propertize-characteristics-key-value (k v &optional pad)
  (format "%s%s(%s)"
          (make-string (or pad 0) ?\s)
          (propertize k 'font-lock-face 'font-lock-function-name-face)
          v))

(defun dnd-actor-characteristics (actor &optional fields)
  (let* ((fields (or fields '("stats" "ac" "perception" "speed" "jump" "pro")))
         (sheet (dnd-get-actor-data actor))
         (is-monster (dnd-query-data sheet "cr"))
         (proficient (dnd-actor-has-proficiency actor "perception")))
    (cl-loop for el in fields concat
             (cond
              ((equal el "stats")
               (cl-loop for s in '("str" "dex" "con" "int" "wis" "char") concat
                        (dnd-propertize-characteristics-key-value
                         (substring s 0 1)
                         (format "%+d" (string-to-number (dnd-query-data sheet s)))
                         1)))
              
              ((equal el "ac")
               (dnd-propertize-characteristics-key-value
                "ac" (dnd-get-actor-ac sheet)
                4))
              
              ((equal el "perception")
               (dnd-propertize-characteristics-key-value
                "perc"
                (number-to-string
                 (+ 10
                    (if is-monster
                        (string-to-number
                         (or (dnd-query-data sheet "skills/perception") "0"))
                      (+ (dnd-ability-modifier (string-to-number (dnd-query-data sheet "wis")))
                         (dnd-get-actor-proficiency-bonus sheet (when (string= proficient "exp") 2))))))
                1))

              ((equal el "speed")
               (dnd-propertize-characteristics-key-value
                "spd" (dnd-query-data sheet "speed") 1))

              ((equal el "jump")
               (let ((str (string-to-number (dnd-query-data sheet "str"))))
                 (dnd-propertize-characteristics-key-value
                  "jmp"
                  (format "%s/%s"
                          (or
                           (dnd-query-data sheet "jump")
                           str)
                          (or
                           (dnd-query-data sheet "jump-vert")
                           (+ 3 (dnd-ability-modifier str)))
                          ) 1)))
              
              ((equal el "pro")
               (dnd-propertize-characteristics-key-value
                "pro" (dnd-get-actor-proficiency-bonus sheet) 1))
              
              (t "")))))

(setq helm-dnd-management-actor-source
      (butlast helm-dnd-actor-source))
      
(setq helm-dnd-management-actor-modifier-source
      `(,(helm-build-sync-source "Conditions"
           :candidates
           '("blinded" "charmed" "deafened" "exhausted"
             "frightened" "grappled" "incapacitated" "invisible"
             "paralyzed" "petrified" "poisoned" "prone"
             "restrained" "stunned" "unconscious"))
        ,(helm-build-sync-source "Modifiers"
           :candidates
           (lambda ()
             (with-current-buffer helm-dnd-buffer
               (hash-table-keys (dnd-query-static-data "modifiers")))))))

(setq helm-dnd-management-actor-active-modifier-source
      `(,(helm-build-sync-source "Active"
           :candidates
           (lambda ()
             (with-current-buffer helm-dnd-buffer
               (--when-let helm-dnd-last-actor-selected
                 (gethash it dnd-actor-modifiers-active)))))))

(defun helm-dnd-add-actor-modifier ()
  (interactive)
  (let* ((actor (helm :sources helm-dnd-management-actor-source))
         (modifier (when actor (helm :sources helm-dnd-management-actor-modifier-source))))

    (when (and actor modifier)
      (dnd-add-effect actor modifier))))

(defun helm-dnd-remove-actor-modifier ()
  (interactive)
  (let* ((actor (helm :sources helm-dnd-management-actor-source))
         (modifier (when actor (helm :sources helm-dnd-management-actor-active-modifier-source))))

    (when (and actor modifier)
      (dnd-remove-effect actor modifier))))

(defun helm-dnd-remove-all-actor-modifiers ()
  (interactive)
  (let ((actor (helm :sources helm-dnd-management-actor-source)))
    (when actor
      (dnd-remove-all-actor-modifiers actor))))

(defun helm-dnd-don-armor ()
  (interactive)
  (let ((actor (helm :sources helm-dnd-management-actor-source)))
    (when actor
      (dnd-actor-don-armor actor))))

(defun helm-dnd-doff-armor ()
  (interactive)
  (let ((actor (helm :sources helm-dnd-management-actor-source)))
    (when actor
      (dnd-actor-doff-armor actor))))

(defun helm-dnd-sideline ()
  (interactive)
  (let ((action (lambda (el)
                  (dnd-sideline-actor
                   (helm-marked-candidates :all-sources t) t))))

    (helm :sources
          `(,(helm-build-sync-source "Active"
               :candidates (lambda ()
                             (with-current-buffer helm-dnd-buffer
                               (cl-loop for (name . _) in (dnd-table-to-assoc "Actors") collect
                                        (cons name (cons name nil)))))
               :action action)

            ,(helm-build-sync-source "Absent"
               :candidates (lambda ()
                             (with-current-buffer helm-dnd-buffer
                               (cl-loop for (name . _) in (dnd-table-to-assoc "Absent") collect
                                        (cons name (cons name t)))))
               :action action)))))

(defun dnd-award-party-xp (xp-data)
  ""
  (let* ((data (dnd-table-to-assoc "Actors"))
         (participants
          (helm :sources
                `(,(helm-build-sync-source "Participants"
                     :candidates
                     (cl-loop for state in data if (dnd-actor-state-party-memberp state) collect (car state))
                     :action (lambda (el) (helm-marked-candidates))))))
         (participant-count (length participants)))

    (cl-loop for (xp . reason) in xp-data do
             (setq per-participant-xp (number-to-string (/ xp participant-count)))
             (cl-loop for actor in participants do
                      (dnd-table-add-row "SessionXP" `("" ,actor ,per-participant-xp
                                                       ,(if reason reason "")))))))

(defun helm-dnd-award-xp ()
  (interactive)
  (let* ((xp (helm :sources helm-dnd-custom-integer-entry-source))
         (xp-set (and xp (> xp 0)))
         (reason (when xp-set
                   (helm :sources helm-dnd-custom-entry-source))))
    (when xp-set
      (dnd-award-party-xp (list (cons xp reason))))))

(defun dnd-cull-monsters (&optional actors)
  (interactive)
  (let* ((dead-rows '())
         (data (dnd-table-to-assoc "Actors"))
         xp
         table-head
         (monsters (or actors
                       (helm :sources
                             `(,(helm-build-sync-source "Monsters to Remove"
                                  :candidates
                                  (cl-loop
                                   for state in data
                                   if (not
                                       (or (dnd-actor-state-party-memberp state)
                                           (dnd-actor-name-special-p (dnd-query-data state "char"))))
                                   collect
                                   (cons (concat
                                          (when (string= "0" (dnd-query-data state "hp"))
                                            "[dead] ") (car state))
                                         (car state)))
                                  :action (lambda (el) (helm-marked-candidates))))))))

    (setq xp
          (cl-loop for state in data
                   for i from 0
                   as char = (dnd-query-data state "char")
                   if (and (member char monsters)
                           (not (dnd-actor-state-party-memberp state)))
                   collect

                   (progn
                     ;; Remove any conditions
                     (cl-loop for effect in (dnd-get-all-effects char) do
                              (dnd-remove-effect char effect))

                     (dnd-remove-all-actor-modifiers char)
                     (dnd-remove-actor-from-attack-histories char)
                     
                     (setq dead-rows (cons i dead-rows))
                     (cons (string-to-number (dnd-query-data (dnd-get-actor-data state) "xp"))
                           (dnd-query-data state "ref")))))

    (save-excursion
      (dnd-table-goto "Actors" t)
      (setq table-head (point))
      
      (cl-loop for row in dead-rows do
               (goto-char table-head)
               (forward-line row)
               (org-table-kill-row)))
    (dnd-award-party-xp xp)))

(defmacro helm-dnd--table-source (buffer regex label)
  `(helm-build-sync-source ,label
    :candidates
    (lambda ()
      (append
       (with-current-buffer ,buffer
         (cl-loop for name in (dnd-get-all-table-names)
                  when (s-matches? ,regex name) collect
                  name))))
    :action
    (lambda (candidate)
      (with-current-buffer ,buffer
        (dnd-table-to-list candidate t)))))

(defun helm-dnd-table-roll (&optional table)
  (interactive)
  (let ((table
         (or table
             (helm
              :sources
              `(,(helm-dnd--table-source helm-dnd-current-buffer "^random-.*" "Local Tables")
                ,(helm-dnd--table-source helm-dnd-buffer "^random-.*" "Master Tables"))))))

    (dnd-add-queue-form
     (list (dnd-weighted-table-with-header table)))))

(cl-defun helm-dnd-org-insert-inherited-statblock (name)
  (interactive "sName: ")
  (--when-let (helm :sources helm-dnd-statblock-source)
    (when (= helm-exit-status 0)
      (dnd-org-insert-empty-statblock
       name
       `(("inherit" . ,it))))))

(defun helm-dnd-overland-load-map ()
  (interactive)
  (helm
   :sources
   (helm-build-sync-source "Maps"
     :candidates
     (lambda ()
       (with-current-buffer helm-dnd-buffer
         (cl-loop for entry across-ref dnd-overland-party-maps collect 
                  `(,(overland-query-data entry "name") . ,(overland-query-data entry "id")))))
     
     :action
     `(("Load" . (lambda (candidate)
                   (with-current-buffer dnd-master-buffer
                     (dnd-overland-load-map candidate))))
       ("Move Party Here" . (lambda (candidate)
                              (with-current-buffer dnd-master-buffer
                                (dnd-overland-party-switchmap candidate))))))))

(defun helm-dnd-overland-send-sheets ()
  (interactive)
  (helm
   :sources
   (helm-build-sync-source "Sheets"
     :candidates
     (with-current-buffer helm-dnd-buffer
       (org-element-map (org-element-parse-buffer) 'headline
         (lambda (el)
           (when (member "character" (org-element-property :tags el))
             (cons (org-element-property :raw-value el)
                   el)))))
     :action
     (lambda (candidate)
       (overland-upload-party-sheets
        dnd-overland-ws
        (cl-loop
         with sheets = (make-hash-table :test 'equal)
         for entry in (helm-marked-candidates :all-sources t) do
         (--when-let (org-element-property :NAME entry)
           (let ((this-sheet (copy-hash-table (dnd-query-static-data "monsters" it))))
             (puthash
              "info"
              (s-join "\n"        
                      (org-element-map entry 'headline
                        (lambda (sub)
                          (message "TAGS %s" (org-element-property :tags sub))
                          (unless (or (member "character" (org-element-property :tags sub))
                                      (member "DM" (org-element-property :tags sub)))
                            (buffer-substring-no-properties
                             (org-element-property :begin sub)
                             (org-element-property :end sub))))))
              this-sheet)
             (puthash it this-sheet sheets)))
         finally return sheets))))))

;;;;;;;;
(provide 'dnd-helm)
