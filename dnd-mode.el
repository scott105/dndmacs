(require 'json)
(require 'subr-x)
(require 'org-element)
(require 'org-table)
(require 'dash)

(require 'dnd-def)
(require 'dnd-roll)
(require 'dnd-queue)
(require 'dnd-tables)
(require 'dnd-helm)
(require 'dnd-commands)

(require 'overland)
(require 'dnd-overland)

(defvar dnd-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-q") #'dnd-goto-queue)
    (define-key map (kbd "M-c") #'dnd-send-quote-to-chat-or-capitalize)
    (define-key map (kbd "<C-M-return>") #'helm-dnd-management)
    (define-key map (kbd "<C-return>") #'helm-dnd-command)
    (define-key map (kbd "<escape>0") #'helm-dnd-list-tables)
    map)
  "Keymap for dnd mode.")

;;;###autoload
(define-minor-mode dnd-mode
  "Minor mode for playing D&D 5e in an org-mode document.

Configuration of a document is largely done through org-mode data lines in the form of:
#+<type>: <data>

#+config: <path>
\t Loads a relative or absolute path pointing to another org file.  Only the '#+' options are loaded from the config (i.e. not tables nor character sheets).

#+static-data: <path>
\t Loads a relative or absolute path pointing to another org file. The data loaded will be all org tables and all entries marked as character sheets (with the :character: tag)

#+custom-commands: <path>
\t Loads a relative or absolute path pointing to an elisp file. The file is evaluated as lisp code.

#+chat: <url>
\t Specifies the Discord webhook (if https://) or irc (if irc://server/#channel)
\t If no chat URL is specified, chat messages are sent to the log.

When first run in an org document, any missing required sections or tables will be created.

Character sheets are of the form:

* Some Entry :character:
:PROPERTIES:
:name: <character name>
:stats: <str> <dex> <con> <int> <wis> <char>
:class: <class>
:level: <level>
:xp: <xp>
:hp: <hp>
:ac: <ac>
:speed: <speed>
:resistant: <resistances>
:immune: <immunities>
:vulnerable: <vulnerabilities>
:attack: (<to hit bonus> (<dmg type>: <dmg>) <range> [:magic t :silvered t])
:attack+: ...
:END:

Further, characters can have 'proficiencies', 'saves', and 'casting'.
Monsters can have 'skills' and 'cr'.

Skills can have the special forms:
  <skill>:exp    which results in the skill being treated as having 'Expertise'
and
  <skill>:<#>    which specifies the skill score (which may not match the stats for monsters)

Free-form attributes can also be stored in the property block.  These will not be processed in any special way aside from being converted to strings.

Mode Keys:

\\{dnd-mode-map}

In the command queue, several other commands are bound:

\\{dnd-queue-map}
"
  :lighter " D&D"
  :keymap dnd-mode-map
  (dnd-mode-load))

(defun dnd-mode-load ()
  (if (not dnd-mode)
      (prog1 nil
        (when dnd-queue-area
          (delete-overlay dnd-queue-area))

        (when dnd-queue-history
          (delete-overlay dnd-queue-history))

        (cl-loop for v in '(dnd-current-state dnd-attack-history dnd-queue-area dnd-queue-history dnd-static-data dnd-chat-type dnd-chat-url dnd-chat-channel dnd-chat-server-buffer dnd-custom-command-registry) do
                 (eval `(kill-local-variable ,'v))))

    (dnd-org-format-all-blocks)

    (let ((master (car (dnd-get-org-metadata "master-file")))
          (is-modified (buffer-modified-p)))
      (if master
          ;; Load the master file if it hasn't been already and load any adventure data in this file.
          (let ((current (current-buffer))
                (master-buffer (find-file (dnd-get-file-path master))))
            (with-current-buffer master-buffer
              (unless (member "D&D" (split-string (format-mode-line minor-mode-alist)))
                (dnd-mode)))

            ;; Load any local data in this file
            (with-current-buffer current
              (setq dnd-master-buffer master-buffer)
              (dnd-load-character-sheets-and-add)
              (dnd-reload-custom-sources)
              (dnd-load-character-modifiers-and-add)))

        ;;
        (unless dnd-current-state
          (dnd-clear-state))

        (setq dnd-master-buffer (current-buffer)
              dnd-last-master-loaded (current-buffer))
        (setq helm-dnd-buffer (current-buffer))
        (setq dnd-actor-modifiers-active (make-hash-table :test 'equal))
        (setq dnd-attack-history (make-hash-table :test 'equal))

        (dnd-setup-document)

        ;; We don't want any automated changes to be affect the status of an unmodified buffer where the mode was loaded
        (set-buffer-modified-p is-modified)

        ;; Overland callbacks for this buffer
        (setq overland-loadmap-callback 'dnd-overland-load-map-callback
              overland-loadparty-callback 'dnd-overland-load-party-callback)


        ;; Handle any config
        (dnd-load-external-config dnd-master-buffer)
        (dnd-load-config dnd-master-buffer)
        (dnd-reload-adventure-data)

        ;; Restore any saved state
        (dnd-combat-state-restore)))))

(defun dnd-dedicate-master ()
  (interactive)
  (with-current-buffer dnd-master-buffer
    (set-window-dedicated-p nil t)))

(defun dnd-send-quote-to-chat-or-capitalize ()
  "Helper for using the default keybinding fallback"
  (interactive)
  (if (org-in-block-p '("quote"))
      (dnd-send-quote-to-chat)
    (capitalize-word nil)))

(defun dnd-to-symbol (val)
  (if (and (stringp val) (not (string-match "[() ]+" val)))
      (make-symbol val)
    val))

(defun dnd-get-file-path (path)
  (let ((dir (file-name-directory (buffer-file-name))))
    (expand-file-name (substitute-in-file-name (concat dir path)))))    

(defun dnd-org-find-or-create-heading (heading &optional create-before-heading)
  (goto-char (point-min))
  
  (if (search-forward heading nil t)
      (forward-line)

    (if (not create-before-heading)
        (goto-char (point-max))
      
      (let ((case-fold-search t))
        (if (not (search-forward create-before-heading nil t))
            (error "Entry '%s' not found" create-before-heading)
          (forward-line -1))))
    
    (insert "\n" heading "\n")))

(defun dnd-org-insert-block (&optional begin end type)
  (interactive (if (use-region-p)
                   (list (region-beginning) (region-end))
                 (list nil nil)))
  
  (let ((type (or type "quote")))

    (when begin
      (goto-char begin))
    (insert "#+begin_" type "\n")

    (when end
      (goto-char end)
      (forward-line 1))
    (insert "#+end_" type "\n")

    (when (not end)
      (forward-line -1)
      (insert "\n")
      (forward-line -1))))

(defun dnd-org-element-to-chat ()
  (interactive)
  (save-mark-and-excursion
    (org-mark-element)
    (let ((text (buffer-substring-no-properties (region-beginning) (region-end))))
      (with-current-buffer
          (or dnd-master-buffer dnd-last-master-loaded)
        (dnd-send-to-chat
         (replace-regexp-in-string "^[*] \\(.*\\)\n\\(.*\\)" "***\\1***\n\\2"
                                   (dnd-convert-org-to-md text)) "DESCRIPTION")))))

(defun dnd-org-format-all-blocks ()
  (interactive)
  (save-mark-and-excursion
    (let ((is-modified (buffer-modified-p)))
      (org-element-map (org-element-parse-buffer) 'quote-block
        (lambda (el)
          (put-text-property
          (org-element-property :begin el)
          (org-element-property :end el)
          'font-lock-face 'font-lock-string-face)))
      (set-buffer-modified-p is-modified))))      

(cl-defun dnd-org-element-at-point (tag)
  (save-excursion
    (--when-let
        (cl-loop with found
                 with start = (or (member tag (org-get-tags nil t))
                                  (org-at-heading-p)
                                  (org-up-heading-safe))

                 while (and start (not found)) do
                 (if (member tag (org-get-tags nil t))
                     (setq found (org-element-at-point))
                   (setq start (org-up-heading-safe)))
                 finally return found)

      (save-restriction
        (narrow-to-region
         (org-element-property :begin it)
         (org-element-property :end it))
        (caddr (org-element-parse-buffer))))))

(cl-defun dnd-org-element-data-at-point (tag)
  (--when-let (dnd-org-element-at-point tag)
    (org-entry-properties
     (org-element-property :begin it)
     'standard)))

(defun dnd-setup-document ()
  (interactive)
  (save-excursion
    (dnd-setup-party-tables)
    (dnd-setup-command-queue)
    (dnd-setup-adventure-log)))

(defun dnd-setup-adventure-log ()
  (interactive)
  (dnd-org-find-or-create-heading "* Adventure Log" "* Characters"))

(defun dnd-setup-party-tables ()
  (interactive)
  (let ((data
         '(("SessionXP" . ("name" "xp" "reason"))
           ("Chases" . ("quarry" "*pursuers"))
           ("Effects" . ("char" "effect" "count"))
           ("Timers" . ("char" "note" "rounds"))
           ("Absent" . ("char" "init" "hp" "max" "ref" "alias" "hidden" "p"))           
           ("Actors" . ("char" "init" "hp" "max" "ref" "alias" "hidden" "p"))))
        (tables (dnd-get-all-table-names))
        name header)
    
    (save-excursion
      (dnd-org-find-or-create-heading "* Characters")

      (cl-loop for entry in data do
               (setq name (car entry)
                     header (cdr entry))

               (when (not (member name tables))
                 (insert
                  "\n"
                  "#+name: " name "\n"
                  "|!|"
                  (s-join "|" header)
                  "|")
                 (move-beginning-of-line nil)
                 (org-table-align)
                 (org-table-insert-hline)
                 (forward-line)
                 (move-end-of-line nil)
                 (insert "\n"))))))

(defun dnd-setup-chat ()
  (interactive)
  (let ((url (car (dnd-get-org-metadata "chat")))
        parts)
    
    (if (not url)
        (message "No chat metadata present")

      (with-current-buffer dnd-master-buffer
        (setq dnd-chat-url url)
        (setq parts (url-generic-parse-url url))
        (setq dnd-chat-type (url-type parts))

        (when (string= "irc" dnd-chat-type)
          (setq dnd-chat-channel (concat "#" (url-target parts)))
          (setq dnd-chat-server-buffer (concat "*" (url-host parts) "*")))))))

(defun dnd-disable-chat ()
  (interactive)
  (with-current-buffer dnd-master-buffer
    (setq dnd-chat-type nil)))

(defun dnd-setup-captures ()
  (interactive)
  (cl-loop with new-captures = (dnd-get-org-metadata "capture")
           initially (unless (boundp 'org-capture-templates)
                       (setq org-capture-templates nil))
           for capture in new-captures collect
           (let* ((form (car (read-from-string (format "(%s)" capture))))
                  (key (format "%s" (car form)))
                  (options (cdr form)))
             (add-to-list 'org-capture-templates
                          `(,key
                            ,(format "D&D: %s"
                                     (or (plist-get options :label)
                                         (buffer-name helm-dnd-buffer)))
                            item
                            (file+olp+datetree
                             ,(format "%s"
                                      (or (plist-get options :file)
                                          (buffer-file-name helm-dnd-buffer)))
                             ,(format "%s"
                                      (or (plist-get options :heading) "Play Notes")))
                            "%?")
                          t))))

(defun dnd-load-config (master)
  (setq dnd-master-buffer master)
  (dnd-reload-static-references)
  (dnd-reload-custom-sources)
  (dnd-setup-chat)
  (dnd-setup-overland)
  (dnd-setup-captures))

(defun dnd-load-external-config (master)
  (interactive)
  (let ((config-data (car (dnd-get-org-metadata "config"))))
    (when config-data
      (with-current-buffer (find-file-existing (dnd-get-file-path config-data))
        (dnd-load-config master)
        (kill-current-buffer)))))

(defun dnd-load-external-data (path master)
  (let* ((path-hash (sha1 path))
         (src (find-file-existing path))
         (src-hash (with-current-buffer src (sha1 (buffer-string))))
         (cache-dir (concat user-emacs-directory "dnd-cache/"))
         (cache (find-file (concat cache-dir src-hash)))
         cache-hash
         data)

    (unless (= (buffer-size cache) 0)
      (setq cache-hash (read cache)))

    ;; TESTING:
    (setq dnd-master-buffer master)
    
    (setq data
       (cond
        ((string= src-hash cache-hash)
         (read cache))

        (t
         (with-current-buffer src
           (org-mode)

           ;; TODO:  Evaluate the need for table data at all
           ;; .Perhaps it's all just better as org files...
           
           (let ((file-data (append
                             (apply 'dnd-load-data-table (dnd-get-all-table-names))
                             (dnd-load-character-sheets)
                        (dnd-load-character-modifiers))))

             ;; Write cache
             (with-current-buffer cache
               (erase-buffer)
               (prin1 src-hash cache)
               (prin1 file-data cache)
               (set-buffer-file-coding-system 'utf-8 'utf-8-unix)
               (save-buffer 0))
             
             file-data)))))

    (with-current-buffer master
      (dnd-add-static-data data))

    ;;
    (kill-buffer cache)
    (kill-buffer src)))

(defun dnd-add-static-data (data &optional merge)
  (unless dnd-static-data
    (setq dnd-static-data (make-hash-table :test 'equal)))

  (cl-loop for entry in data do
           (let* ((table (car entry))
                  (table-data (cdr entry))
                  (existing (gethash table dnd-static-data)))

             ;; Iterate table-data and puthash into hash
             (if (not existing)
                 (puthash table table-data dnd-static-data)
               
               (cl-loop for k being the hash-keys of table-data do
                        (puthash k (gethash k table-data) existing))))))
    
(defun dnd-reload-static-references ()
  (interactive)
  (message "External data loaded in %s"
           (car (benchmark-run
                    (cl-loop for path in (dnd-get-all-static-refs) do
                             (dnd-load-external-data (dnd-get-file-path path) dnd-master-buffer))))))

(defun dnd-reload-custom-sources ()
  (interactive)
  (cl-loop for path in (dnd-get-org-metadata "custom-commands") do
           (load (dnd-get-file-path path))))

(defun dnd-reload-adventure-data ()
  (interactive)
  (dnd-load-character-sheets-and-add)
  (dnd-character-sheet-expand-ancestors)
  (dnd-load-character-modifiers-and-add)
  (dnd-add-modifiers-from-effect-list)
  
  ;; Warn about missing data
  (cl-loop for missing in (dnd-check-all-missing-character-data (dnd-query-static-data "monsters")) do
           (message "Warning: %s missing %s" (car missing) (prin1-to-string (cdr missing)))))
    
(defun dnd-get-org-metadata (typestr &optional with-positions)
  (save-excursion
    (goto-char (point-min))
    (let ((case-fold-search t)
          names start line)
      (while (setq start (search-forward (format "#+%s:" typestr) nil t))
        (setq line (s-trim (buffer-substring-no-properties start (line-end-position))))
        (if with-positions
            (push (cons line (point-at-bol)) names)
        (push line names)))
      (reverse names))))

(defun dnd-get-all-static-refs ()
  (dnd-get-org-metadata "static-data"))

(defun dnd-get-all-table-names ()
  (dnd-get-org-metadata "name"))

(defun dnd-get-query-path (&rest path)
  (let ((result '())
        (parts))
    (mapc (lambda (part)
            (setq result (append result (s-split "/" part))))
          path)
    result))


(defun dnd-list-values-to-strings (data)
  (let ((conv (lambda (sub)
                (if (stringp sub)
                    sub
                  (prin1-to-string sub)))))

    `(,@(cl-loop for el in data
                 if (listp el) append (cl-loop for sub in el collect (funcall conv sub))
                 if (nlistp el) collect (funcall conv el)))))

(defun dnd-query-data (data &rest path)
  (cl-loop for key in (apply 'dnd-get-query-path path) do
           (cond
            ((listp data)
             (setq data (cdr (assoc key data))))
            ((hash-table-p data)
             (setq data (gethash key data)))

            ((dnd-actor-data-p data)
             (let ((mods (dnd-actor-data-mods data)))
               (setq data (dnd-query-data (dnd-actor-data-data data) key))
               
               (cl-loop
                with val = data
                for modifier in mods do
                (--when-let (dnd-query-data modifier key)
                  ;; TODO:  Figure out how to sandbox this
                  ;; NOTE:  'val' must be bound here to handle the variable substitution mechanism of modifiers
                  (when (stringp val)
                    (setq val (string-to-number val)))
                  
                  (setq val
                        (if (stringp it)
                            (eval (car (read-from-string it)))
                          it))

                  ;; TODO:  Change this to be cleaner and to handle type mismatches better
                  (cond
                   ((numberp val)
                    (setq data (number-to-string val)))
                   ((listp data)
                    (setq data (append data val)))
                   ((hash-table-p data)
                    (cl-loop for k being the hash-keys of val do
                             (puthash k (gethash k val) data)))
                   (t
                    (setq data val)))))))))
  
  data)

(defun dnd-query-static-data (&rest path)
  (apply 'dnd-query-data dnd-static-data path))

(defun dnd-clear-history ()
  (interactive)
  (save-excursion 
    (dnd-goto-queue-history)
    (delete-region (point) (point-max))))

(defun dnd-org-raw-tag (element)
  "Helper for processing org element tag data.
Specifically for dealing with underscores, which org treats as a special type"
  (let ((tag (org-element-property :tag element)))
    (when tag
      (apply 'concat
             (cl-loop for part in tag collect
                      (if (listp part)
                          (buffer-substring-no-properties
                           (org-element-property :begin part)
                           (org-element-property :end part))
                        part))))))

(defun dnd-org-list-to-hash (data)
  (let (res
        child-list
        k v
        (type (org-element-type data)))

    (cond
     ((equal type 'plain-list)
      (setq res (make-hash-table :test 'equal))
      
      (cl-loop for child in (org-element-contents data) do
               (prog1
                   nil
                 (setq child-list (org-element-contents child))
                 
                 (if (cadr child-list)
                     (progn
                       ;; With 2 elements, we assume there's a sublist
                       (setq k (dnd-org-list-to-hash (car child-list)))
                       (setq v (dnd-org-list-to-hash (cadr child-list))))

                   ;; With only 1 we assume a normal entry
                   (setq k (dnd-org-raw-tag child))
                   (setq v (dnd-org-list-to-hash (car child-list))))

                 (when k
                   (puthash (substring-no-properties k) v res)))))

     ((equal type 'paragraph)
      (setq res (s-replace "\n" ""
                           (buffer-substring-no-properties
                            (org-element-property :begin data)
                            (org-element-property :end data))))))

    res))

(defun dnd-process-character-data (data)
  (cl-loop for k in '("str" "dex" "con" "int" "wis" "char")
           for v in (mapcar 's-trim (s-split "[ ]*,[ ]*\\|[ ]+" (dnd-query-data data "stats"))) do
                            (puthash k v data))

  (cl-loop for k in '("proficiencies" "skills" "saves" "skill-disadvantage" "damage-reductions") do
           (let ((field (gethash k data)))
             (when field
               (puthash k (dnd-parse-compact-field field) data))))

  (cl-loop for k in '("immune" "resistant" "vulnerable") do
           (let ((field (gethash k data)))
             (when field
               (puthash k (mapcar 's-trim (s-split "[ ]*,[ ]*\\|[ ]+" field)) data))))

  (setq data (dnd-process-attack-data data))
  (setq data (dnd-process-spell-data data))
  
  ;; Character notes
  (--when-let (gethash "note" data)
    (puthash "note"
             (cl-loop for note in (s-split "\\. *" it)
                      if (> (length note) 0) collect
                      note)
             data))
  data)

(cl-defun dnd-process-attack-data (data)
  (--if-let (gethash "attacks" data)
      (when (stringp it)
        (cl-loop
         for k being hash-keys of it do
         (let ((attack (s-split ", " (gethash k it))))
           (puthash k
                    `(("hit_bonus" . ,(nth 0 attack))
                      ("dmg" . ,(concat
                                 (--when-let (nth 2 attack)
                                   (format "%s: " it))
                                 (nth 1 attack)))
                      ("range" . ,(nth 3 attack)))
                    it))))
    ;; Create empty list if needed
    (puthash "attacks" (make-hash-table :test 'equal) data))

  ;; Properties based attack specification
  (--when-let (gethash "attacks" data)
      (cl-loop for attack in 
               (car (read-from-string (concat "(" (gethash "attack" data) ")")))
               collect
               (progn
                 (let (name)
                   (setq name (pop attack))
                   (when (not (stringp name))
                     (setq name (prin1-to-string name)))

                   (puthash
                    name
                    (if (or (listp (nth 1 attack))
                            (-any?
                             (lambda (sym) (string= (substring (prin1-to-string sym) 0 1) ":"))
                             attack))
                        (cl-loop
                         with default-key = '(:hit_bonus :dmg :range)
                         while attack collect

                         (let ((arg (pop attack)))
                           (cons
                            (--when-let
                                (if (string= (substring (prin1-to-string arg) 0 1) ":")
                                    (prog1 arg
                                      (setq arg (pop attack)))
                                  (pop default-key))
                              (substring (prin1-to-string it) 1))
                            
                            (cond
                             ((listp arg)
                              (s-join " " (mapcar 'prin1-to-string arg)))
                             ((stringp arg)
                              arg)
                             (t
                              (prin1-to-string arg))))))
                      
                      ;; Old format
                      (let ((attack (mapcar (lambda (el)
                                              (if (stringp el)
                                                  el
                                                (prin1-to-string el)))
                                            attack)))

                        `(("hit_bonus" . ,(nth 0 attack))
                          ("dmg" . ,(concat
                                     (--when-let (nth 2 attack)
                                       (format "%s: " it))
                                     (nth 1 attack)))
                          ("range" . ,(nth 3 attack)))))
                    it))))
      ;; To avoid confusion, we remove the original data
      (remhash "attack" data))
  data)

(defun dnd-process-spell-data (data)
  (cl-loop
   with spells = (make-hash-table :test 'equal)
   for level from 0 to 9
   as heading = (format "spells-%s" level)
   do

   (cl-loop
    with level-spells = (make-hash-table :test 'equal)
    with raw-listing = (or (dnd-query-data data heading) "")
    for entry in (eval (read (format "'(%s)" raw-listing)))
    do
    (let (spell-name 
          spell-data)

      (pcase entry
        ((pred listp)
         (setq spell-name (format "%s" (car entry))
               spell-data (cdr entry)))
        (_
         (setq spell-name (format "%s" entry))))

      (when spell-name
        (puthash spell-name spell-data level-spells)))

    finally do
    (when (not (hash-table-empty-p level-spells))
      (remhash heading data)
      (puthash (format "%s" level) level-spells spells)))

   finally do
   (puthash "spells" spells data))

  data)

(defun dnd-missing-character-sheet-data (data)
  "Checks a single character sheet hash table for missing required fields."
  (let ((req-fields '("name" "stats" "xp" "hp" "ac" "speed"))
        missing)
    (cl-remove nil
               (cl-loop for field in req-fields collect
                        (when (not (gethash field data))
                          field)))))

(defun dnd-check-all-missing-character-data (data)
  "Checks a collection of character sheet hashes, embedded in the provided hash, for missing required fields."
  (cl-remove nil 
             (cl-loop for k being hash-keys in data collect
                      (let ((missing (dnd-missing-character-sheet-data
                                      (gethash k data))))
                        (when missing
                          (cons k missing))))))

(defun dnd-org-character-to-sheet (data)
  (let ((sheet (make-hash-table :test 'equal)))

    ;; Load any data listed as properties
    ;; Override any values in the table
    (cl-loop for (k . v) in data
             if (not (string= k "CATEGORY")) do
             (puthash (downcase k) v sheet))

  sheet))

(defun dnd-load-character-sheets ()
  (let ((characters (make-hash-table :test 'equal))
        sheet)

    (dnd-foreach-tag-in-buffer
     "character"
     (--when-let (dnd-org-character-to-sheet
                  (org-entry-properties
                   (org-element-property :begin heading)
                   'standard))
       (puthash (gethash "name" it)
                (if (dnd-query-data it "inherit")
                    it
                  (dnd-process-character-data it))
                characters)))

    (list (cons "monsters" characters))))

(defun dnd-load-character-sheets-and-add ()
  (dnd-add-static-data (dnd-load-character-sheets)))

(defun dnd-character-sheet-expand-ancestors (&optional data)
  (let* ((deps
          (if data
              (cl-loop with sheet = data
                       with bail
                       while (and (not bail) sheet)
                       as inherit = (dnd-query-data sheet "inherit")
                       if inherit
                       collect (cons (dnd-query-data sheet "name") inherit)
                       and do
                       (setq sheet (dnd-query-static-data "monsters" inherit))
                       else do
                       (setq bail t))

            ;; Pull all sheets
            (cl-loop for sheet being the hash-values of (dnd-query-static-data "monsters")
                     as inherit = (dnd-query-data sheet "inherit")
                     if inherit collect
                     (cons (dnd-query-data sheet "name") inherit))))
         (depths
          (cl-loop for (child . parent) in deps collect
                   (let ((depth 0) 
                         (ancestor parent))
                     (while (--when-let (cdr (assoc ancestor deps))
                              (cl-incf depth)
                              (setq ancestor (if (string= it child) nil it))))
                     (cons child depth)))))

    ;; Sort by ancestry depth and expand each sheet
    (cl-loop for (name . depth) in (cl-sort depths (lambda (a b) (< (cdr a) (cdr b))))
             for sheet = (dnd-query-static-data "monsters" name)
             for parent = (dnd-query-static-data "monsters" (dnd-query-data sheet "inherit")) do

             ;; For every element in parent and missing from child, copy
             (cl-loop for k being hash-keys of parent
                      as parent-val = (gethash k parent)
                      as sheet-val = (gethash k sheet)
                      unless sheet-val do (puthash k parent-val sheet)
                      else if (and (hash-table-p parent-val) (hash-table-p sheet-val)) do
                      ;; Merge hash tables
                      (cl-loop for copyk being hash-keys of parent-val
                               as subv = (gethash copyk parent-val)
                               as existing = (gethash copyk sheet-val)
                               unless existing do
                               (puthash copyk subv sheet-val))))))

(cl-defun dnd-load-character-sheet-at-point ()
  (interactive)
  (--when-let (dnd-org-element-data-at-point "character")
    (let* ((sheet (dnd-org-character-to-sheet it))
           (name (dnd-query-data sheet "name"))
           (inherit (dnd-query-data sheet "inherit"))
           (data (dnd-query-static-data "monsters")))
      (dnd-process-attack-data sheet)
      (puthash name
               (if inherit
                   sheet
                 (dnd-process-character-data sheet))
               data)
      (dnd-character-sheet-expand-ancestors sheet))))

(defun dnd-load-character-modifiers ()
  (let ((modifiers (make-hash-table :test 'equal)))
    (dnd-foreach-tag-in-buffer
     "modifiers"
     (org-element-map heading 'property-drawer
       (lambda (drawer)
         (cl-loop
          with data = (make-hash-table :test 'equal)
          with drawer-begin = (org-element-property :begin drawer)
          for (k . v) in (org-entry-properties drawer-begin 'standard)
          if (not (string= k "CATEGORY")) do
          (puthash (downcase k) v data)
          finally
          (puthash (gethash "name" data)
                   (dnd-process-attack-data data)
                   modifiers)))))
    (list (cons "modifiers" modifiers))))

(defun dnd-load-character-modifiers-and-add ()
  (dnd-add-static-data (dnd-load-character-modifiers)))

(defun dnd-org-insert-empty-statblock (name &optional data)
  (interactive "sName: ")
  (save-mark-and-excursion
    (insert
     (org-element-interpret-data
      (org-element-create
       'headline `(:level ,(or (org-current-level) 1) :title ,name :tags ("character") :post-blank 1)
       `(property-drawer
         (:post-blank 1)
         (node-property (:key "name" :value ,(downcase name)))
         ,@(if data
               (cl-loop
                for (k . v) in data collect
                `(node-property (:key ,k :value ,v)))
             (cl-loop
              for field in '("stats" "xp" "hp" "ac" "speed" "attack") collect
              `(node-property (:key ,field :value ""))))))))))

(defun dnd-cull-dead-monsters ()
  (interactive)
  (let ((data (dnd-table-to-assoc "Actors")))
    (dnd-cull-monsters
     (cl-loop for state in data
              if (and (not (dnd-actor-state-party-memberp state))
                      (string= "0" (dnd-query-data state "hp"))) collect
              (dnd-query-data state "char")))))

(defun dnd-award-xp (xp &optional reason)
  (interactive "nXP: \nsReason: ")
  (dnd-award-party-xp (list (cons xp reason))))

(defun dnd-sideline-actor (actors &optional align-tables)
    (cl-loop for (name . reverse) in actors do
             (let* ((to (if reverse "Actors" "Absent"))
                    (from (if reverse "Absent" "Actors"))
                    (row (dnd-table-find-row from `("*" ,name)))
                    line)
               (save-excursion
                 (dnd-table-goto from)
                 (forward-line row)
                 (setq line (buffer-substring-no-properties (point) (point-at-eol)))
                 (kill-whole-line 1)
                 
                 (dnd-table-goto to)
                 (goto-char (org-table-end))
                 (insert line "\n"))))
    (save-excursion
      (dnd-table-goto "Actors")
      (org-table-align)
      (dnd-table-goto "Absent")
      (org-table-align)))

(defun dnd--compare-modifier-by-priority (a b)
  (string>
   (or (gethash "mod-priority" a) "0")
   (or (gethash "mod-priority" b) "0")))

(defun dnd-add-actor-modifier (name modifier &optional no-error)
  (let ((active (gethash name dnd-actor-modifiers-active))
        (exists (dnd-query-static-data "modifiers" modifier)))
    
    (when (and (not no-error) (not exists))
      (error "Character modifier '%s' not found" modifier))

    (when (and exists (not (member modifier active)))
      (prog1
          (puthash name (cl-remove-duplicates (append active `(,modifier)) :test 'equal) dnd-actor-modifiers-active)
        (push `(apply dnd-remove-actor-modifier . ,(list name modifier)) buffer-undo-list)
        (dnd-update-queue-status)))))

(defun dnd-remove-actor-modifier (name modifier)
  (let ((active (gethash name dnd-actor-modifiers-active)))
    (when (member modifier active) 
      (prog1
          (puthash name (cl-remove modifier active :test 'equal)
                   dnd-actor-modifiers-active)
        (push `(apply dnd-add-actor-modifier . ,(list name modifier)) buffer-undo-list)
        (dnd-update-queue-status)))))

(defun dnd-remove-all-actor-modifiers (actor)
  (prog1
      ;; Note:  This could be cleaner as in the case of multiple status effects it does a little bit of extra work removing the individually instead of as a group.
      (cl-loop for mod-name in (gethash actor dnd-actor-modifiers-active) do
               (let ((base-name (replace-regexp-in-string "-[[:digit:]]+$" "" mod-name)))
                 (when (not (string= mod-name base-name))
                   (dnd-remove-effect actor base-name))
                 (dnd-remove-effect actor mod-name)))
    (dnd-update-queue-status)))

(defun dnd-clear-attack-history ()
  "Clears all target/attack history.
Normally this would only be necessary if actors have been manually removed from the Actors table."
  (setq dnd-attack-history (make-hash-table :test 'equal)))

;; Effects
(defun dnd-add-effect (name effect &optional count replace)
  (save-excursion
    (dnd-table-goto "Effects")
    (let ((row (dnd-has-effect name effect))
          (count (or count 1))
          previous
          val)

      (if (not row)
          (progn
            (dnd-table-add-row "Effects" `("" ,name ,effect ,(number-to-string count)))
            (setq previous 0
                  val count))

        (setq previous (if replace
                           0
                         (string-to-number (org-table-get row 4))))
        (setq val (+ previous count))
        (org-table-put row 4 (number-to-string val) t))
      (dnd-add-actor-modifier-for-condition name effect previous val)))
  nil)

(defun dnd-remove-effect (name effect &optional count)
  (save-excursion
    (dnd-table-goto "Effects")
    (let ((row (dnd-has-effect name effect))
          (count (or count 1))
          previous
          val)

      (when row
        (setq previous (string-to-number (org-table-get row 4)))
        (setq val (- previous count))
        (if (<= val 0)
            (progn
              (setq val 0)
              (dnd-table-kill-rows "Effects" `("*" ,name ,effect)))
          (org-table-put row 4 (number-to-string val) t)))

      ;;
      (when previous
        (dnd-remove-actor-modifier-for-condition name effect previous val)))))

(defun dnd-add-actor-modifier-for-condition (name effect previous-level new-level)
  ;; Attempt to add modifiers matching this condition
  (dnd-add-actor-modifier name effect t)

  (cl-loop for level from (1+ previous-level) to new-level do
           (dnd-add-actor-modifier
            name
            (format "%s-%d" effect level)
            t)))

(defun dnd-remove-actor-modifier-for-condition (name effect previous-level new-level)
  (when (<= new-level 0)
    (dnd-remove-actor-modifier name effect))
  
  (cl-loop for level downfrom previous-level below new-level do
           (dnd-remove-actor-modifier
            name
            (format "%s-%d" effect level))))

(defun dnd-has-effect (name effect)
  (dnd-table-find-row "Effects" `("*" ,name ,effect)))

(defun dnd-get-all-effects (name)
  (cl-loop for (char . (_ (_ . effect))) in (dnd-table-to-assoc "Effects") 
           if (string= char name) collect
           effect))

(defun dnd-get-effect-count (name effect)
  (save-excursion
    (dnd-table-goto "Effects")
    (let ((row (dnd-has-effect name effect)))
      (if (not row)
          0
        (max 1 (string-to-number (org-table-get row 4)))))))

(defun dnd-get-effect-list-str (name &optional mods-only public-only)
  (--when-let
      (cl-loop with publicly-visible-modifiers = '("")
               for (char . data) in (dnd-table-to-assoc "Effects")
               as effect = (dnd-query-data data "effect")
               as count = (string-to-number (dnd-query-data data "count"))
               as mod-data = (dnd-query-static-data "modifiers" effect)
               when (and (string= char name)
                         (or (not public-only)
                             (and
                              mod-data
                              (dnd-query-data mod-data "public")))
                         (or (not mods-only)
                             mod-data))

               collect
               (concat (dnd-beautify-actor-name effect)
                       (when (> count 1)
                         (concat " "
                                 (number-to-string count) "X"))))
    (s-join ", " it)))

(defun dnd-add-modifiers-from-effect-list ()
  (let ((data (dnd-table-to-assoc "Effects")))
    (cl-loop for (actor . condition) in data do
             (dnd-add-actor-modifier-for-condition
              actor
              (dnd-query-data condition "effect")
              0 (string-to-number (dnd-query-data condition "count"))))))

(defun dnd-actor-don-armor (name)
  (interactive)
  (let ((data (dnd-get-actor-data name)))
    (--when-let (dnd-query-data data "armor")
      (dnd-add-effect name it 1 t))))
        
(defun dnd-actor-doff-armor (name)
  (interactive)
  (let ((data (dnd-get-actor-data name)))
    (--when-let (dnd-query-data data "armor")
      (dnd-remove-effect name it
                         (dnd-get-effect-count name it)))))

;;;;;;;;;;;;  Timers
(defun dnd-add-timer (char note)
  (dnd-table-set-from-assoc
   "Timers"
   (append
    (dnd-table-to-assoc "Timers")
    `((,char ("char" . ,char) ("note" . ,note) ("rounds" . "1"))))))

(defun dnd-tick-timers ()
  (dnd-table-set-from-assoc
   "Timers"
   (cl-loop for entry in (dnd-table-to-assoc "Timers") collect
            (let ((rounds (car (last entry))))
              (append
               (butlast entry)
               (list
                (cons
                 "rounds"
                 (number-to-string
                  (1+ (string-to-number (cdr rounds)))))))))))

;;;;;;;;;;;;  Helpers
(defun dnd-ability-modifier (score)
  ;; Note:  Rounding was getting a bit weird, so this hardcoded logic did the trick
  (floor (/ (- score
               (if (< score 10)
                   11
                 10)) 2)))

(defun dnd-proficiency-from-cr (cr)
  (+ 2 (floor (/ (max 0 (- cr 1)) 4))))

(defun dnd-proficiency-from-level (level)
  (+ 2 (floor (/ (1- level) 4))))

(defun dnd-get-actor-proficiency-bonus (name-or-data &optional scale)
  (let* ((data (if (stringp name-or-data) (dnd-get-actor-data name-or-data) name-or-data))
         (level (dnd-query-data data "level"))
         (scale (or scale 1)))

    (floor
     (* scale
        (if level (dnd-proficiency-from-level (string-to-number level))
          (dnd-proficiency-from-cr (string-to-number (dnd-query-data data "cr"))))))))

(defun dnd-actor-has-proficiency (actor skill)
  (dnd-query-static-data "monsters" actor "proficiencies" skill))

(defun dnd-parse-hp-field (str)
  (mapcar 'string-to-number (s-split "," str)))

(defun dnd-process-hp (real tmp delta &optional type)
  (cond
   ((equal type :heal)
    (setq real (+ real delta)))

   ((equal type :dmg)
    (setq tmp (- tmp delta))
    (when (< tmp 0)
      (cl-incf real tmp)
      (setq tmp 0)))

   ((equal type :temp)
    (setq tmp (max tmp delta))))
   
  (list real tmp))

(defun dnd-get-actor-state (name)
  (let ((row (dnd-table-find-row "Actors" `("*" ,name))))
    (if row
        (dnd-table-get-row "Actors" row)
      (--when-let (dnd-query-static-data "monsters" name)
        (let ((hp (number-to-string (dnd-roll-result (dnd-query-data it "hp") nil t))))
          `(("!") ("char" . ,name) ("ref" . ,name)
            ("hp" . ,hp)
            ("max" . ,hp)
            ("hidden" . "*")
            ("dummy" . "*")))))))

(defun dnd-get-actor-data (name-or-state)
  (let* ((actor-state (if (stringp name-or-state)
                          (dnd-get-actor-state name-or-state)
                        name-or-state))
         name ref)

    (if (not actor-state)
        (setq ref name-or-state)
      (setq name (dnd-query-data actor-state "char"))
      (setq ref (dnd-query-data actor-state "ref")))
    (--when-let (dnd-query-static-data "monsters" (or ref name))
      (make-dnd-actor-data
       :data it
       :mods (--when-let (gethash name dnd-actor-modifiers-active)
               (sort
                (cl-loop for mod in it collect
                         (dnd-query-static-data "modifiers" mod))
                #'dnd--compare-modifier-by-priority))))))

(defun dnd-actor-name-special-p (name)
  (when name
    (s-matches? ":\\.*" name)))

(defun dnd-get-actor-public-name (state)
  (unless (dnd-query-data state "hidden")
    (or
     (dnd-query-data state "alias")
     (dnd-query-data state "char"))))

(defun dnd-get-actor-ac (data)
  (when data
    (let* ((dex (dnd-ability-modifier
                 (string-to-number (dnd-query-data data "dex")))))
      (string-to-number (dnd-query-data data "ac")))))

;;;;;;;;;;;;  Design functions
;; Inserting data from 5e tables

(defun dnd-arg-as-string (arg)
  (--when-let arg
    (if (not (stringp it))
        (format "%s" arg)
      arg)))

(cl-defun dnd-add-actor-monster (monster count &key name hp current alias hidden noref player)
  (with-current-buffer dnd-master-buffer
    (save-excursion
      (let* ((monster (dnd-arg-as-string monster))
             (name (dnd-arg-as-string name))
             (alias (dnd-arg-as-string alias))

             ;;
             (data (dnd-query-static-data "monsters" monster))
             (name (or name monster))
             (count (if count (dnd-roll-result count) 1))
             (current (if current (int-to-string current) nil))
             hp)

        (cl-loop repeat count do
                 (let ((hp (or hp (int-to-string (dnd-roll-hitdice (dnd-query-data data "hp"))))))
                 (dnd-table-add-row
                  "Actors"
                  `("" ,(dnd-unique-actor-name name (= count 1)) ""
                    ,(or current hp) ,hp
                    ,(if noref "" monster)
                    ,(or alias "")
                    ,(if hidden "*" "")
                    ,(if player "*" "")))))))))

(cl-defun dnd-add-actor (actor &key current alias hidden)
  (dnd-add-actor-monster actor 1 :noref t :current current :alias alias :hidden hidden :player t))

(cl-defun dnd-add-special-actor-entry (name &optional init)
  (--when-let (format ":%s:" name)
    (dnd-table-kill-rows "Actors" `("*" ,it))
    
    (dnd-table-add-row
     "Actors"
     `(""
       ,it
       ,(format "%s" (or init 20))
       "" ""
       ,it))
    (dnd-sort-actors-initiative)))

(defun dnd-remove-special-actor-entry (name)
  (when name
    (let ((name (format ":%s:" name)))
      (dnd-table-kill-rows "Actors" `("*" ,name)))))

(cl-defun dnd-encounter-get-properties ()
  (--when-let
      (with-current-buffer (or helm-dnd-current-buffer (current-buffer))
        (save-excursion
          (let ((el (org-element-at-point)))
            (--if-let (equal (org-element-type el) 'headline)
                el
              (org-previous-visible-heading 1)
              (org-element-at-point)))))

    (when (member "encounter" (org-element-property :tags it))
      (let ((data (org-entry-properties
                   (org-element-property :begin it)
                   'standard)))
        `((entity . ,it)
          (pos . ,(org-element-property :begin it))
          (levels . ,(--when-let (cdr (assoc "LEVELS" data))
                       (mapcar 'string-to-number (s-split " " it))))
          (actors . ,(read
                      (concat
                       "("
                       (cdr (assoc "ACTOR" data))
                       ")"))))))))

(cl-defun dnd-encounter-add-actor-entry (ref count &rest params)
  (--when-let (dnd-encounter-get-properties)
    (save-excursion
      (goto-char 
       (cdr (org-get-property-block 
             (cdr (assoc 'pos it))
             'force)))
      (insert ":actor+: "
              (prin1-to-string
               (append
                `(,ref ,(dnd-roll-result count))
                params))
              "\n")
      (forward-line -1)
      (org-indent-line))))

(cl-defun dnd-encounter-update-lethality ()
  (interactive)
  (org-set-property
   "lethality"
   (dnd-encounter-lethality)))

(cl-defun dnd-encounter-get-actors ()
  (cdr (assoc 'actors (dnd-encounter-get-properties))))

(cl-defun dnd-add-encounter-actors ()
  (interactive)
  (cl-loop
   for entry in (dnd-encounter-get-actors)
   do (apply #'dnd-add-actor-monster entry)))

(cl-defun dnd-get-party-levels ()
  (with-current-buffer dnd-master-buffer
    (cl-loop 
     with party = (cl-loop for (char . entry) in (dnd-table-to-assoc "Actors")
                           if (string= "*" (cdr (assoc "p" entry))) collect char)
     for char in party
     as sheet = (dnd-get-actor-data char)
     as level = (dnd-query-data sheet "level")
     if level collect (string-to-number level))))

(cl-defun dnd-get-party-lethality-levels (&optional party-levels)
  (interactive)
  (with-current-buffer dnd-master-buffer
    (cl-loop
     with lethality = '((1 . (25 50 75 100))
                        (2 . (50 100 150 200))
                        (3 . (75 150 225 400))
                        (4 . (125 250 375 500))
                        (5 . (250 500 750 1100))
                        (6 . (300 600 900 1400))
                        (7 . (350 750 1100 1700))
                        (8 . (450 900 1400 2100))
                        (9 . (550 1100 1600 2400))
                        (10 . (600 1200 1900 2800))
                        (11 . (800 1600 2400 3600))
                        (12 . (1000 2000 3000 4500))
                        (13 . (1100 2200 3400 5100))
                        (14 . (1250 2500 3800 5700))
                        (15 . (1400 2800 4300 6400))
                        (16 . (1600 3200 4800 7200))
                        (17 . (2000 3900 5900 8800))
                        (18 . (2100 4200 6300 9500))
                        (19 . (2400 4900 7300 10900))
                        (20 . (2800 5700 8500 12700)))

     with totals = '(0 0 0 0)
     for level in (or party-levels (dnd-get-party-levels))
     do
     (setq totals
           (cl-loop for threshold in (cdr (assoc level lethality))
                    for current in totals
                    collect (+ current threshold)))
     finally return totals)))

(cl-defun dnd-encounter-lethality (&optional party-levels)
  (interactive)
  (let* ((encounter (dnd-encounter-get-properties))
         (actors (cdr (assoc 'actors encounter)))
         (party-levels (or party-levels (cdr (assoc 'levels encounter))))
         (lethality-levels (dnd-get-party-lethality-levels party-levels))
         (count 0)
         multiplier
         adjusted-xp)

    (with-current-buffer dnd-master-buffer
      (cl-loop with actor-count
               with xp
               for (ref number) in actors
               as sheet = (dnd-query-static-data "monsters" (format "%s" ref))
               sum number into actor-count
               sum (* (string-to-number (dnd-query-data sheet "xp")) number) into xp
               finally (setq count actor-count
                             adjusted-xp xp)))

    ;; TODO:  Apply extra modifications for extreme party sizes (ie. anything outside of 3-5 players; DMG.83)
    ;; .Don't count actors with CR some threshold below the rest of the encounter for the multiplier.
    
    (setq multiplier
          (cdr
           (--last (>= count (car it))
                   '((1 . 1)
                     (2 . 1.5)
                     (6 . 2)
                     (10 . 2.5)
                     (14 . 3)
                     (15 . 4)))))

    (setq adjusted-xp (* adjusted-xp multiplier))
    
    (nth (or (--find-last-index (> adjusted-xp it) lethality-levels) 0)
         '("easy" "medium" "hard" "deadly"))))

(cl-defun dnd-encounter-lethality-current-party ()
  (interactive)
  (let ((lethality (dnd-encounter-lethality (dnd-get-party-levels))))
    (when (called-interactively-p)
      (message "Lethality: %s" lethality))
    lethality))

;;;###autoload
(cl-defmacro dnd-challenge-probability (successes bonus &rest forms)
  `(progn
     (let (data
           last-dc
           (step (lambda (state probability depth)
                   (let* ((success-count (logcount state))
                          (failure-count (- depth success-count))
                          (just-succeeded (= 1 (logand 1 (lsh state (* -1 (1- depth))))))
                          (dc (float (progn ,@forms)))
                          (probable-success
                           (max 0
                                (min 1
                                     (/ (- 21 (- dc ,bonus)) 20)))))

                     (setq last-dc dc)
                     
                     (if (or (= success-count ,successes)
                             (= failure-count 3))
                         (push (list failure-count success-count state probability) data)

                       ;;
                       (funcall step (logior (lsh 1 depth) state) (* probability probable-success) (1+ depth))
                       (funcall step state (* probability (- 1 probable-success)) (1+ depth)))))))
       (funcall step 0 1 0)
       data)))

;;;###autoload
(cl-defmacro dnd-challenge-probability-aggregate (successes &rest forms)
  (let ((no-formatting (eval (plist-get forms :no-formatting))))
    `(dnd-pivot-table-columns
      (append
       '(("Bonus" "Fail*" "Fail" "Victory" "Victory*"))
       (cl-loop
        for bonus in '(2 3 4 5 6 7 8) collect
        (cons
         bonus
         (let ((data (dnd-challenge-probability ,successes bonus ,@forms)))
           (cl-loop
            with out = (make-hash-table)
            for (failure-count success-count pattern probability) in data
            do
            (--when-let (pcase failure-count
                          (0 "Total Victory")
                          (3 (if (= success-count 0) "Total Failure" "Failure"))
                          (_ "Partial Victory"))
              (puthash
               it (+ probability (or (gethash it out) 0)) out))

            finally return
            (cl-loop
             for k being the hash-keys of out collect
             ,(--when-let '(* 100 (gethash k out))
                (if no-formatting
                    it
                  `(format "%d%%" ,it)))))))))
      t)))

;;;;;;;;;;;;  Playtime functions
(defun dnd-point-in-overlay (overlay)
  (and (> (point) (overlay-start overlay)) (< (point) (overlay-end overlay))))

(defun dnd-set-state (key value)
  (push `(apply dnd-set-state . ,(list key (cdr (assoc key dnd-current-state)))) buffer-undo-list)
  (setq dnd-current-state (cl-acons key value dnd-current-state))
  (dnd-update-queue-status)
  value)

(defun dnd-get-state (key &optional default)
  (let ((val (cdr (assoc key dnd-current-state))))
    (if val val default)))

(defun dnd-inc-state (key &optional amount)
  (let ((current (dnd-get-state key)))
    (dnd-set-state key (+ (if current current 0) (if amount amount 1)))))

(defun dnd-append-state (key form)
  (let ((current (dnd-get-state key)))
    (dnd-set-state key (append current form))))

(defun dnd-get-state-number (key)
  (let ((val (dnd-get-state key)))
    (if val val 0)))

(defun dnd-restore-state (state)
  (setq dnd-current-state state)
  (dnd-update-queue-status))

(defun dnd-clear-state (&optional actor)
  (interactive)
  (let ((last-roll (dnd-get-state :last-roll))
        (speaker-history (dnd-get-state :speaker-history))
        (combat-round (dnd-get-state :round))
        (challenge-success (dnd-get-state :challenge-success))
        (challenge-failure (dnd-get-state :challenge-failure))
        (cleave-attack (dnd-get-state :cleave-attack))
        (cleave-dmg (dnd-get-state :cleave-dmg)))
    (push `(apply dnd-restore-state . ,(list (cl-copy-list dnd-current-state))) buffer-undo-list)
    (setq dnd-current-state '())
    (when actor
      (dnd-set-state :actor actor))
    (dnd-set-state :last-roll last-roll)
    (dnd-set-state :speaker-history speaker-history)
    (dnd-set-state :round combat-round)
    (dnd-set-state :challenge-success challenge-success)
    (dnd-set-state :challenge-failure challenge-failure)
    (dnd-set-state :cleave-attack cleave-attack)
    (dnd-set-state :cleave-dmg cleave-dmg)
    (dnd-update-queue-status)))

(cl-defun dnd--request-data-path (&rest keys)
  (s-join
   "/"
   (mapcar (lambda (el) (format "%s" el))
           (-flatten keys))))
      
(cl-defun dnd-request-data (&rest keys)
  (unless dnd-requested-data
    (setq dnd-requested-data (make-hash-table :test 'equal)))
  
  (let* ((path (apply 'dnd--request-data-path keys))
         (value (gethash path dnd-requested-data)))
    (unless value
      (puthash path nil dnd-requested-data))
    value))

(cl-defun dnd-request-data-set (path value)
  (unless dnd-requested-data
    (setq dnd-requested-data (make-hash-table :test 'equal)))
  (puthash path value dnd-requested-data))
  
(defun dnd-string-to-chat ()
  (interactive)
  (when (dnd-point-in-queue)
    (beginning-of-line)
    
    (if (equal "\"" (buffer-substring-no-properties (point-at-bol) (1+ (point-at-bol))))
        ;;
        (progn
          (save-excursion
            (forward-sexp)
            (let ((end (point))
                  start)
              (er/mark-outside-quotes)
              (setq start (point))

              (goto-char end)
              (backward-char)
              (insert "\n")
              (forward-char)
              (insert ")\n")
              (goto-char start)
              (insert "(chat ")
              (forward-char)
              (insert "\n")))
          (forward-line))

      ;;
      (insert "(chat \"")
      (dnd-queue-indent-line)
      (insert "\n\n\")\n")
      (forward-line -2))))

(defun dnd-beautify-actor-name (name)
  (if name
      (capitalize
       (s-replace
        "_" " "
        (replace-regexp-in-string
         "_\\([[:digit:]]+\\)$" " #\\1"
         name)))
    ""))

(defun dnd-actor-speak ()
  (interactive)
  (let* ((history-count 10)
         (data (dnd-table-to-assoc "Actors"))
         (actor
          (helm :sources
                `(,(helm-build-sync-source "Recent"
                     :candidates
                     (lambda ()
                       (with-current-buffer helm-dnd-buffer
                         (cl-remove nil
                                    (dnd-get-state :speaker-history)))))

                  ,(helm-build-sync-source "Actors"
                     :candidates
                     (lambda ()
                       (with-current-buffer helm-dnd-buffer
                         (cl-remove nil
                                    (cl-loop for entry in data collect
                                             (when (and
                                                    (not (dnd-query-data entry "hidden"))
                                                    (> (string-to-number (dnd-query-data entry "hp")) 0))
                                               (dnd-query-data entry "char")))))))
                  ,helm-dnd-custom-entry-source))))

    (when actor
      (dnd-set-state :last-speaker actor)
      (dnd-set-state :speaker-history
                     (let ((history (remove-duplicates
                                     (cons actor (dnd-get-state :speaker-history))
                                     :test 'equal :from-end t)))
                       (butlast history (- (length history) history-count))))
      
      (insert (format "(speak \"%s\" \"\n\n\")\n"
                      (dnd-beautify-actor-name
                       (or (dnd-query-data data actor "alias")
                            actor))))
      (forward-line -2))))

(defun dnd-actor-state-playerp (state)
  (not (dnd-query-data state "ref")))

(defun dnd-actor-state-party-memberp (state)
  (when (dnd-query-data state "p")
    t))

(defun dnd-remove-actor-from-attack-histories (actor)
  (cl-loop for k being the hash-keys of dnd-attack-history do
           (let ((entry (gethash k dnd-attack-history)))
             (when (string= actor (cadr entry))
               (remhash k dnd-attack-history)))))

(defun dnd-update-party-xp-from-session ()
  (interactive)
  (let* ((data (dnd-table-to-assoc "SessionXP"))
         (totals (make-hash-table :test 'equal))
         changes)

    ;; Calculate XP from session
    (cl-loop for entry in data do
             (let ((name (dnd-query-data entry "name"))
                   (xp (string-to-number (dnd-query-data entry "xp"))))
               (puthash name (+ xp (or (gethash name totals) 0)) totals)))

    (save-excursion
      ;; Clear out SessionXP
      (dnd-table-goto "SessionXP" t)
      (delete-region (point) (org-table-end))

      (org-map-entries
       (lambda ()
         (setq name (or
                     (org-entry-get nil "name")
                     (downcase (org-get-heading t t t t))))

         (--when-let (gethash name totals)
           (let* ((old-xp (string-to-number (or (org-entry-get nil "xp")
                                                (dnd-query-static-data "monsters" name "xp"))))
                  (new-xp (+ old-xp it)))
             (let ((val (number-to-string new-xp)))
               (org-set-property "xp" val)
               (puthash "xp" val (dnd-query-static-data "monsters" name)))
             (push (list name old-xp new-xp) changes))))
       "character"))
    (reverse changes)))

(defun dnd-update-party-xp-from-session-and-chat ()
  (interactive)
  (dnd-send-to-chat 
   (s-join "\n"
           (cl-loop for (actor before after) in (dnd-update-party-xp-from-session) collect
                    (format "**%s:** _%d xp  ->  %d xp      (+%s)_"
                            (capitalize actor) before after (- after before))))
   "XP"))

(defun dnd-next-turn ()
  (interactive)
  (let* ((indicator ">")
         (cells (cdr (dnd-get-table-raw "Actors" "@<$1..@>$1")))
         (current (cl-position indicator cells :test 'string=))
         (count (length cells))
         (i 0)
         nextline found next-actor
         actor-state
         hp conditions
         new-round
         chat)
    
    (save-excursion
      (dnd-table-goto "Actors" t)

      (when current
        (org-table-put (+ 2 current) 1 ""))

      ;; Start at the current position and loop like a ring buffer searching for positive HP

      (while (and (not found) (< i count))
        (if (not current)
            (progn
              (setq current 0)
              (setq nextline 2)
              (dnd-set-state :round 1)
              (dnd-combat-state-set "round" 1)
              (dnd-tick-timers)
              (setq new-round t))
          
          (cl-incf current)
          (setq nextline (+ 2 (mod current count)))
          (when (= nextline 2)
            (dnd-inc-state :round 1)
            (dnd-combat-state-set "round" (dnd-get-state :round))
            (dnd-tick-timers)
            (setq new-round t)))

        (unless (or (and (string= "0" (s-trim (org-table-get nextline 4)))
                         (not (string= "" (s-trim (org-table-get nextline 6)))))
                    (not (string= "" (s-trim (org-table-get nextline 8)))))
          (setq found nextline))

        (cl-incf i))

      (cond
       (found
        (org-table-put found 1 indicator t)
        (setq next-actor (s-trim (org-table-get found 2)))

        (setq actor-state (dnd-get-actor-state next-actor)
              conditions (dnd-get-effect-list-str next-actor t)
              hp (s-trim (org-table-get nextline 4)))

        (setq chat
              (format "***%s's turn:***     %s"
                      (dnd-beautify-actor-name (dnd-get-actor-public-name (dnd-get-actor-state next-actor)))
                      (if (dnd-actor-state-party-memberp actor-state)
                          (format "*%s hp%s*" hp
                                  (if conditions
                                      (concat " [" conditions "]")
                                    ""))
                        ""))))
       
       (t
        (setq next-actor nil)))

      (dnd-set-state :actor next-actor)
      (dnd-update-queue-status))

    (when new-round
      (dnd-send-actor-report-to-chat))
    (when chat
      (dnd-send-to-chat chat "TURN"))
    
    next-actor))

(defun dnd-last-log-to-md (&optional count)
  (replace-regexp-in-string
      "  - \\(.*?\\) :: \\(.*?\\)$" "\n__  ***\\1 :: \\2***  __"
      (replace-regexp-in-string
       "  - \\*SESSION\\*" "\n__     *Session Log*     __"
       (dnd-log-get-session-diary (or count 1))
       t)))
  
(defun dnd-send-last-log-to-chat ()
  (interactive)
  (dnd-send-to-chat
   (dnd-last-log-to-md 2)))

(defun dnd-send-last-log-to-clipboard-md ()
  (let ((log (dnd-last-log-to-md)))
    (with-temp-buffer
      (insert
       (s-replace
        "    " "  "
        log))
       (clipboard-kill-region (point-min) (point-max)))))

(defun dnd-send-actor-report-to-chat ()
  (interactive)
  (let ((round (dnd-get-state :round)))
    (dnd-send-to-chat
     (concat
      (if round
          (format "__**Round:\t*%d***%s__\n" round (make-string 60 ? ))
        (format "__**Status:**%s__\n" (make-string 60 ? )))
      (dnd-actor-state-report) "\n"
      (dnd-chase-report)))))

(defun dnd-send-hold-music-to-chat ()
  (interactive)
  (let ((lines
         '("***Please hold...  Your Dungeon Associate is doing something with rules or some such...***"
           "***One moment please...  Game Master is __not__ panicking...***"
           "***Thank you for your patience...  Your business means everything to the DM...***"
           "***Please hold... \n\tNOTE:  It's not AFK if you're just doing something else...***")))
                 
    (dnd-send-to-chat (nth (random (length lines)) lines) "Hold Music")))

(defun dnd-convert-org-to-md (str)
  (let ((replacements '(("*" . "**") ("/" . "_") ("+" . "~~"))))
    (replace-regexp-in-string
     "\\([*/+_]\\)\\(.*?\\)\\1"

     (lambda (result)
       (let ((out result))
         (mapc (lambda (r)
                 (setq out (s-replace (car r) (cdr r) out)))
               replacements)
         out))
     str)))

(defun dnd-log-goto ()
  (interactive)
  (dnd-org-find-or-create-heading "* Adventure Log")
  (forward-line -1)
  (--if-let (org-element-property :contents-end (org-element-at-point))
      (goto-char it)
    (forward-line)))

(defun dnd-log-add (&optional str)
  (interactive "sEntry: ")
  (when (and (not (called-interactively-p)) (not str))
    (setq str (read-string "Entry: ")))

  (save-excursion
    (dnd-log-goto)
    (insert "    - " str "\n")
    ;; TODO:Maybe reformat if multiline?
    ;; .Will likely require entering lines manually
    )
  str)

(defun dnd-log-mark-session ()
  (interactive)
  (dnd-log-add "*SESSION*"))

(defun dnd-log-get-date (&optional with-diary)
  (save-excursion
    (dnd-log-goto)
    
    (let* ((start (save-excursion (search-backward "* Adventure Log")))
           (match 2)
           (entry
            ;; search backwards in region for "  - xxxx-xx-xx"  or "  - Day x" format
            (cond
             ((re-search-backward "  - \\(\\([0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}\\) :: .*\\)" start t)
              (match-string match))

             ((re-search-backward "  - \\(\\(Day [0-9]+\\) :: .*\\)" start t)
              (match-string match)))))

      
      (if (not entry)
          "Day 0"
        ;;
        (if (not with-diary)
            (progn
              (set-text-properties 0 (length entry) nil entry)
              entry)
          ;;
          (save-mark-and-excursion
            (org-mark-element)
            (s-trim (buffer-substring-no-properties (region-beginning) (1- (region-end))))))))))

(defun dnd-log-get-session-diary (&optional count)
  (save-mark-and-excursion
    (let ((count (or count 1)))
      (dnd-log-goto)
      (org-narrow-to-subtree)

      (goto-char (point-max))
      
      (unless (re-search-backward "  - \\*SESSION\\*$" nil t count)
        (goto-char (point-min))
        (forward-line))
                 
      (prog1
          (buffer-substring-no-properties (point) (point-max))
        (widen)))))

(defun dnd-log-increase-date (date &optional days-passed)
  (let ((days-passed (or days-passed 1))
        day-format-match)
    (cond
     ((setq day-format-match (s-match "^Day \\([0-9]+\\)$" date))
      (concat "Day " (number-to-string (+ (string-to-number (cadr day-format-match)) days-passed))))

     (t
      ;; Note:  We deal with days/months as zero indexed internally.
      (let* ((parts
              (mapcar (lambda (el)
                        (string-to-number el)) (s-split "-" date)))
             (year (pop parts))
             (month (1- (pop parts)))
             (day (+ (1- (pop parts)) days-passed)))

        ;; Handle roll-over of days into months; month to years.
        (setq month (+ month (/ day 30))
              day (mod day 30)
              year (+ year (/ month 12))
              month (mod month 12))
        
        (format "%d-%02d-%02d" year (1+ month) (1+ day)))))))

(defun dnd-log-new-day (&optional note days-passed)
  (interactive "sNote: \nnDays: ")
  (when (not (called-interactively-p))
    (setq note (or note (read-string "Note: "))
          days-passed (or days-passed (max 1 (string-to-number (read-string "Days: "))))))

  (let* ((days-passed (or days-passed 1))
         (date (dnd-log-get-date))
         (new-date (dnd-log-increase-date date days-passed)))
    
    (dnd-log-goto)
    (insert "  - " new-date " :: " note "\n")
    new-date))

(defun dnd-send-to-chat (str &optional sender)
  (with-current-buffer dnd-master-buffer
    (cond
     ((string= dnd-chat-type "https")
      (let ((sender (or sender "DM"))
            (url-request-method "POST")
            (url-request-extra-headers '(("Content-Type" . "application/json")))
            url-request-data)

        (setq url-request-data
              (json-encode `((username . ,sender)
                             (content . ,str))))
        
        (url-retrieve-synchronously dnd-chat-url (lambda (status) (kill-buffer))))
      t)

     ((and (string= dnd-chat-type "irc") (fboundp 'rcirc-buffer-process) (when dnd-chat-server-buffer (get-buffer dnd-chat-server-buffer)))
      (let ((process  (rcirc-buffer-process dnd-chat-server-buffer)))
        (when process
          (rcirc-send-message process dnd-chat-channel str))))

     (t
      (message "CHAT: %s" str)))))

(defun dnd-send-quote-to-chat ()
  (interactive)
  (when (org-in-block-p '("quote"))
    (save-excursion
      (--when-let (org-element-at-point)
        (let* ((parent (org-element-property :parent it))
               (type (org-element-type parent)))
          (when (equal type 'quote-block)

            (dnd-send-to-chat
             (dnd-convert-org-to-md
              (buffer-substring
               (org-element-property :contents-begin parent)
               (org-element-property :contents-end parent)))
             (org-element-property :name parent))))))))

(defun dnd-combat-state-restore ()
  (interactive)
      (save-excursion
        (dnd-org-find-or-create-heading "* Characters")
        (forward-line -1)
        (--when-let
            (org-element-property :ROUND (org-element-at-point))
          (dnd-set-state :round (string-to-number it)))
        (--when-let
            (org-element-property :CHALLENGE-SUCCESS (org-element-at-point))
          (dnd-set-state :challenge-success (string-to-number it)))
        (--when-let
            (org-element-property :CHALLENGE-FAILURE (org-element-at-point))
          (dnd-set-state :challenge-failure (string-to-number it)))
        ))

(defun dnd-combat-state-set (key value)
  (save-excursion
    (dnd-org-find-or-create-heading "* Characters")
    (forward-line -1)
    (if value
        (org-set-property key (format "%s" value))
      (org-delete-property key))))

(defun dnd-combat-end ()
  (interactive)
  (dnd-set-state :round nil)
  (dnd-combat-state-set "round" nil)

  ;; Kill common special entries
  (dnd-remove-special-actor-entry "lair")
  
  ;; Kill turn indicator
  (--when-let (dnd-table-find-row "Actors" '(">"))
    (dnd-table-set-field "Actors" it "!" "")))

(defun dnd-health-str (hp-percentage)
  (if (= hp-percentage 0)
      "dead"
    (cond ((<= hp-percentage 0.25)
           "wounded")
          (t
           "alive"))))

(defun dnd-actor-state-report ()
  (interactive)
  ;; NOTE:  Alignment doesn't actually work because of non-monospaced fonts, but it looks better than with nothing even attempting it.
  (cl-loop
   with data = (dnd-table-to-assoc "Actors")
   with align-offset = 16

   for entry in data

   as hp = (dnd-query-data entry "hp")
   as hp-max = (dnd-query-data entry "max")
   as dead = (string= hp "0")
   as is-player = (dnd-actor-state-party-memberp entry)
   as name = (dnd-query-data entry "char")
   as public-name = (dnd-beautify-actor-name (dnd-get-actor-public-name entry))

   unless (or (dnd-query-data entry "hidden") (dnd-actor-name-special-p name)) concat
   (concat
    "  - "
    (when dead "~~")
    "**" public-name ":**"
    (make-string (max 1 (- align-offset (length public-name))) ? )
    "_"
    (if (not is-player)
        (concat (dnd-health-str (/ (string-to-number hp) (float (string-to-number hp-max)))) "")
      (concat hp " hp"))
    (--when-let (dnd-get-effect-list-str name t (not is-player))
      (concat "    " it))
    "_"
    (when dead "~~")
    "\n")))

(defun dnd-chase-report ()
  (interactive)
  (let ((data (dnd-table-to-assoc "Chases")))

    (s-join "\n"
            (cl-loop for chase in data collect
                     (let* ((quarry (dnd-query-data chase "quarry"))
                            (quarry-state (dnd-get-actor-state quarry))
                            (quarry-name (dnd-beautify-actor-name (dnd-get-actor-public-name quarry-state)))
                            (pursuers (dnd-query-data chase "pursuers"))
                            (quarry-distance (string-to-number (dnd-query-data pursuers quarry))))
                            
                       (format
                        "***%s** is pursued by %s*" quarry-name
                        (s-join ", "
                                (cl-remove
                                 nil
                                 (cl-loop for (actor . distance) in pursuers
                                          unless (string= actor quarry) collect
                                          (let ((pursuer-state (dnd-get-actor-state actor)))
                                            (unless (dnd-query-data pursuer-state "hidden")
                                              (format "**%s** (%sft)"
                                                      (dnd-beautify-actor-name
                                                       (dnd-get-actor-public-name pursuer-state))
                                                      (- (string-to-number distance)
                                                         quarry-distance)))))))))))))

(defun dnd-chase-complication (&optional table)
  (interactive)
  (let ((table (or table
                   (helm :sources
                         `(,(helm-build-sync-source "Complications Table"
                              :candidates (lambda ()
                                            (with-current-buffer helm-dnd-buffer
                                              (cl-loop for name in (dnd-get-all-table-names)
                                                       when (s-matches? "^chase-.*" name) collect
                                                       name)))))))))
    (dnd-add-queue-form (list (dnd-weighted-table (dnd-table-to-list table))))))

 (defun dnd-replace-element-in-form (search replace form)
   (cond
    ((listp form)
     (cl-loop for el in form collect
              (dnd-replace-element-in-form search replace el)))
    
    ((equal form search)
     replace)

    (t
     form)))

(cl-defmacro dnd-foreach-tag-in-buffer (tag &rest forms)
  `(progn
     (let (heading-tags results)
       (org-element-map (org-element-parse-buffer) 'headline
         (lambda (heading)
           (prog1 nil
             (setq heading-tags
                   (mapcar #'substring-no-properties (org-element-property :tags heading)))

             (when (member ,tag heading-tags)
               (let ((data (org-entry-properties (org-element-property :begin heading) 'standard))
                     (heading-title
                      (substring-no-properties (car (org-element-property :title heading)))))
                 (push (progn ,@forms) results))))))
       results)))

;;;;;;;;;;;;;
(provide 'dnd-mode)

