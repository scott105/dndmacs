;;;; Overland integration
(defun dnd-setup-overland ()
  (let ((server (car (dnd-get-org-metadata "overland-server")))
        (key (car (dnd-get-org-metadata "overland-apikey"))))
    (with-current-buffer dnd-master-buffer
      (setq dnd-overland-server (or server dnd-overland-server)
            dnd-overland-apikey (or key dnd-overland-apikey)))))

(defun dnd-overland-connect (&optional no-warn)
  (interactive)

  ;; Actually connect to the overland service using the configured information
  (if (and dnd-overland-server dnd-overland-apikey)
      (progn
        (setq dnd-overland-ws
              (overland-connect (format "%s/service" dnd-overland-server)))
        (overland-api-key-auth dnd-overland-ws dnd-overland-apikey))
    (unless no-warn
      (warn "Missing either `overland-server' or `overland-apikey' metadata."))))

(defun dnd-overland-disconnect ()
  (interactive)
  (when dnd-overland-ws
    (overland-disconnect dnd-overland-ws)
    (setq dnd-overland-ws nil)))

(defun dnd-overland-load-map (map)
  (overland-loadmap dnd-overland-ws map))

(defun dnd-overland-party-switchmap (map)
  (when dnd-overland-ws
    (overland-party-switchmap dnd-overland-ws map)))

;;;; Overland callbacks
(defun dnd-overland-load-party-callback (data)
  (with-current-buffer dnd-master-buffer
    (setq dnd-overland-party-maps (overland-query-data data "maps"))))

(defun dnd-overland-load-map-callback (data map-buffer)
  ;; TODO:  Make this a proper variable
  (let ((server dnd-overland-server))
    (with-current-buffer map-buffer
      (setq overland-map-source
            (concat "http" (substring server 2)))))
  
  (switch-to-buffer-other-window map-buffer))

;;;;
(provide 'dnd-overland)
