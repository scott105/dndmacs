(defvar dnd-queue-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "<tab>") #'dnd-queue-indent-line)
    (define-key map (kbd "C-c C-c") #'dnd-resolve-queue)
    (define-key map (kbd "M-c") #'dnd-string-to-chat)
    (define-key map (kbd "C-M-n") #'dnd-next-turn)
    (define-key map (kbd "C-M-c") #'dnd-actor-speak)
    map)
  "Keymap for dnd mode.")

(defun dnd-setup-command-queue ()
  (interactive)
  (save-excursion
    (if dnd-queue-marker
        (progn
          (goto-char (overlay-end dnd-queue-marker))
          (forward-line))

      ;;
      (dnd-org-find-or-create-heading "* Command Queue")
      (let ((current (point)))
        (save-excursion
          (forward-line -1)
          (setq dnd-queue-marker (make-overlay (point) current))))
      (overlay-put dnd-queue-marker 'invisible t)
      (overlay-put dnd-queue-marker 'intangible t))

    ;;
    (when dnd-queue-area
      (delete-overlay dnd-queue-area))
    
    (insert "\n")
    (setq dnd-queue-area (make-overlay (point) (1+ (point))))
    (insert "\n")
    (overlay-put dnd-queue-area 'before-string "Queue:  ")
    (overlay-put dnd-queue-area 'line-prefix "> ")
    (overlay-put dnd-queue-area 'keymap dnd-queue-map)
    (dnd-update-queue-status)

    ;;
    (when dnd-queue-history
      (delete-overlay dnd-queue-history))
    
    (insert "\n\n\n")

    ;; Move the queue overlay ending above the newlines we created for the history
    (move-overlay dnd-queue-area (overlay-start dnd-queue-area) (1- (point)))
    
    (forward-line 1)
    (setq dnd-queue-history (make-overlay (point) (1+ (point))))
    (insert "\n")
    (overlay-put dnd-queue-history 'before-string "History:")
    (overlay-put dnd-queue-history 'line-prefix "; ")

    ;;
    (dnd-clear-history)))

(defun dnd-propertize-queue-key-value (k v &optional sep)
  (concat
   (propertize (format "%s%s" k sep) 'font-lock-face 'dnd-queue-status-key)
   " "
   (propertize (format "%s" v) 'font-lock-face 'dnd-queue-status-value)))

(defun dnd-queue-status-indicators-from-table (indicator color table)
  (--when-let (cond
               ((stringp table)
                (length (dnd-table-to-list table)))
               ((listp table)
                (length table))
               ((numberp table)
                table))
    (cl-loop repeat it
             concat (propertize indicator 'face `(:inherit dnd-queue-status-title
                                                           :foreground ,color
                                                           :weight normal))
             concat (propertize " " 'font-lock-face 'dnd-queue-status-title))))

(defun dnd-update-queue-status ()
  (let ((keys (sort (seq-uniq (mapcar 'car dnd-current-state)) 'string<))
        (chase-data (dnd-table-to-list "Chases"))
        (timer-data (dnd-table-to-list "Timers")))
    (when dnd-queue-area
      (run-hook-with-args 'dnd-queue-status-hook :chases chase-data :timers timer-data)
      (overlay-put dnd-queue-area 'before-string
                   (concat
                    (propertize " Queue:          " 'font-lock-face 'dnd-queue-status-title)
                    (dnd-queue-status-indicators-from-table "≡" "cyan" chase-data)
                    (dnd-queue-status-indicators-from-table "○" "black" timer-data)
                    (dnd-queue-status-indicators-from-table "∎" "#F92672" (dnd-get-state :round))
                    (propertize " \n" 'font-lock-face 'dnd-queue-status-title)
                    (when dnd-actor-modifiers-active
                      (concat
                       (propertize ":mods " 'font-lock-face 'dnd-queue-status-key)
                       (cl-loop for name being the hash-keys of dnd-actor-modifiers-active concat
                                (--when-let (gethash name dnd-actor-modifiers-active)
                                  (format "%s[ %s ] " name
                                          (propertize (s-join " " it)
                                                      'font-lock-face 'dnd-queue-status-modifier-name))))
                       "\n"))
                    (cl-loop for k in keys concat
                             (let ((v (dnd-get-state k)))
                               (when v
                                 (cond
                                  ((member k '(:dmg :roll :last-roll :dc))
                                   (concat
                                    (dnd-propertize-queue-key-value k (dnd-roll-to-string v nil t) dnd-queue-status-sep) "  "))
                                  
                                  ((nlistp v)
                                   (concat
                                    (if (equal v t)
                                        (propertize (prin1-to-string k) 'font-lock-face 'dnd-queue-status-flag-key)
                                      (dnd-propertize-queue-key-value k v dnd-queue-status-sep)) "  "))))))
                    "\n")))))

(defun dnd-point-in-queue ()
  (interactive)
  (dnd-point-in-overlay dnd-queue-area))
       
(defun dnd-goto-queue (&optional end)
  (interactive)
  (when dnd-queue-area
    (push-mark nil t)
    (goto-char (if end (overlay-end dnd-queue-area) (overlay-start dnd-queue-area)))
    (forward-line (when end -1))))

(defun dnd-goto-queue-history ()
  (interactive)
  (when dnd-queue-history
    (goto-char (overlay-start dnd-queue-history))
    (forward-line)))

(defun dnd-add-queue-form (form)
  (save-excursion
    (with-current-buffer dnd-master-buffer
      (dnd-goto-queue t)
      (prog1 form
        (cl-loop for el in form do
                 (insert (prin1-to-string el) "\n"))))))

(defun dnd-map-command (form)
  (cons
   (intern (format "dnd-5e-%s" (car form)))
   (cdr form)))

(defun dnd-execute-cmd (form)
  (let (dnd-queue-pass
        execution-started
        (execution-priority 0)
        (priority 0)
        logging)

    ;; Scrub the top-level forms and find the highest priority level
    (cl-loop for exp in form do
             (when (listp exp)
               (--when-let (cdr (assoc (car exp) dnd-command-priorities))
                 (when (> it execution-priority)
                   (setq execution-priority it)))))
    
    `(,(cl-loop for line-number upfrom 1 
                for exp in form
                do
                (setq priority
                      (or
                       (when (listp exp)
                         (cdr (assoc (car exp) dnd-command-priorities)))
                       0))

                if (or dnd-queue-pass (< priority execution-priority)) collect
                `(,exp)

                else collect
                (pcase exp
                  (:wait-completed
                   (if (not execution-started)
                       nil
                     (setq dnd-queue-pass t)
                     '(:wait-completed)))

                  (:break
                   (prog1 nil
                     (setq dnd-queue-pass t)))

                  (:clear
                   (prog1 nil
                     (dnd-clear-state (dnd-get-state :actor))))

                  ((pred stringp)
                   (prog1 nil (push (prin1-to-string exp) logging)))

                  (_
                   (pcase (macroexpand (dnd-map-command exp))
                       (`((:break ,new-forms))
                        (setq execution-started t
                              dnd-queue-pass t)
                        `(,@new-forms
                          ,exp))

                       (new-form
                        (setq execution-started t)
                        ;; Logging certain commands
                        (--when-let
                            (pcase exp
                              (`(,(or 'chat 'speak) . ,_)
                               (format "%s %s"
                                       (propertize (prin1-to-string (car exp))
                                                   'font-lock-face 'dnd-history-command-name)
                                       (s-join ": " (mapcar 's-trim (cdr exp))))))
                          (push it logging))
                   
                        new-form)))))
      . ,logging)))

;; TODO:  This should really format the form but not do the insertion itself
;; .That would allow it to be called recursively cleanly
(defun dnd-insert-formatted-queue-sexp (exp &optional inside)
  "Formats a single queue expression."
  (when inside
    (insert " "))

  (pcase exp
    ;; Overlays
    (`((:overlay . ,overlay-data) . ,rest)
     (let ((current (point))
           overlay)
       (cl-loop for sub-exp in rest do
                (dnd-insert-formatted-queue-sexp sub-exp t))

       (setq overlay (make-overlay current (point)))
       (overlay-put overlay 'evaporate t)
       (cl-loop for (key val) on overlay-data by 'cddr do
                (overlay-put overlay key val))))
     
    ;; Properties
    (`((:propertize . ,properties) . ,rest)
     (--when-let (point)
       (cl-loop for sub-exp in rest do
                (dnd-insert-formatted-queue-sexp sub-exp t))
       (add-text-properties it (point) properties)))
    
    ;; Raw strings
    ((pred stringp)
     (insert
      (if (string= exp "")
          " "
        (propertize (prin1-to-string exp)
                    'font-lock-face 'font-lock-string-face))
      (if inside "" "\n")))

    ;; flow-control blocks
    (`(,(and (or 'on-success 'on-failure 'else 'foreach) func) . ,rest)
     (insert "("
             (propertize (prin1-to-string func)
                         'font-lock-face 'font-lock-function-name-face)
             "\n")

     (cl-loop for arg in rest do
              (dnd-insert-formatted-queue-sexp arg))

     (insert ")\n"))

    ;; Normal command calls
    (`(,func . ,rest)
     (insert "("
             (propertize (prin1-to-string func)
                         'font-lock-face 'font-lock-function-name-face))
     (when rest
       (cl-loop for arg in rest do
                (dnd-insert-formatted-queue-sexp arg t)))
     
     (insert ")\n"))

    ;; Raw elements (w/ keyword detection)
    (_
     (let* ((text (prin1-to-string exp))
            (keyword (equal (substring text 0 1) ":")))
       (insert
        (cond
         (keyword
          (propertize text 'font-lock-face 'font-lock-keyword-face))
         ((stringp exp)
          (propertize
           text
           'font-lock-face
           'font-lock-negation-char-face))
         (t
          text))
        (if inside "" "\n"))))))

(defun dnd-add-to-history (str)
  (save-excursion
    (dnd-goto-queue-history)
    (cond
     ((stringp str)
      (insert str "\n"))

      (t
       (cl-loop for entry in (reverse str) do
                (save-excursion
                  (insert entry "\n")))))
                  
    (move-overlay dnd-queue-history
                  (overlay-start dnd-queue-history)
                  (point-max))))

(defun dnd-resolve-queue ()
  (interactive)

  (dnd-goto-queue)
  
  (save-excursion
    (condition-case err
        (let* ((raw (s-trim (delete-and-extract-region (overlay-start dnd-queue-area) (overlay-end dnd-queue-area))))
               (form (dnd-execute-cmd (car (read-from-string (concat "(" raw ")"))))))

          (insert "\n")
          
          (cl-loop
           with new-form = (car form)
           with log-output = (cdr form)

           for chunk in new-form
           as queue-pos = (point)

           if (and chunk (listp chunk)) do
           (cl-loop for f in chunk do
                    (dnd-insert-formatted-queue-sexp f))

           finally do
           ;; Logging
           (dnd-add-to-history log-output)))
      
      ;;
      ((error debug)
       (dnd-insert-formatted-queue-error err)))

    ;;
    (insert "\n\n")
    (move-overlay dnd-queue-area (overlay-start dnd-queue-area) (point)))

  ;; Formatting queue as lisp
  (dnd-queue-indent)

  (forward-line)

  (dnd-update-queue-status)

  ;;
  (--when-let (text-property-any
               (overlay-start dnd-queue-area)
               (overlay-end dnd-queue-area)
               'input-capture t)
    (goto-char it)
    (delete-char 1)))

(cl-defmacro dnd-with-queue (&rest body)
  `(progn
     (save-restriction
       (narrow-to-region (overlay-start dnd-queue-area)
                         (overlay-end dnd-queue-area))
       ,@body)))
  
(defun dnd-queue-indent ()
  (interactive)
  (dnd-with-queue
   (lisp-indent-region (point-min) (point-max))))

(defun dnd-queue-indent-line ()
  (interactive)
  (dnd-with-queue
   (lisp-indent-line)))

(defun dnd-insert-formatted-queue-error (err)
  (insert
   (propertize (prin1-to-string (format "[ERROR] %s"
                                        (prin1-to-string err)))
               'font-lock-face 'dnd-queue-error)
   "\n"))

;;;;
(provide 'dnd-queue)
