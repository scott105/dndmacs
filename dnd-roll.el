(defun dnd-roll-parse (equation &rest parameters)
  ;; HACK:  Adding a '+ so we can easily split into parts
  (let* ((equation-parts (s-split
                          "[+]"
                          (s-replace
                           "-" "+-"
                           (if (not (stringp equation))
                               (prin1-to-string equation)
                             (replace-regexp-in-string
                              "[+]+" "+"
                              (s-replace " " "+" equation)))))))

    (cl-loop for part in equation-parts
             when (not (string-empty-p part)) collect
             (cond
              ;; Dice
              ((string-match "\\([0-9]+\\)d\\([0-9]+\\)" part)
               (cons
                (string-to-number (match-string 1 part))
                (string-to-number (match-string 2 part))))

              ;; Literal
              ((s-matches? "[0-9]+" part)
               (string-to-number part))

              ;; Label
              ((s-matches? ".+:" part)
               (make-dnd-roll-label :label part))

              ;; Symbol
              (t
               (let ((sym (intern (format ":%s" part))))
                 (--if-let (plist-get parameters sym)
                     it
                   sym)))))))

(defun dnd-roll-perform (roll-spec &rest parameters)
  (when (nlistp roll-spec)
    (setq roll-spec (dnd-roll-parse roll-spec)))

  (let ((scale 1)
        count die)

    (cl-loop for part in roll-spec collect
             (cond
              ;; Labels
              ((dnd-roll-label-p part)
               part)

              ;; Literals
              ((numberp part)
               part)

              ;; Symbols
              ((symbolp part)
               (or (plist-get parameters part) 0))
              
              ;; Already performed
              ((listp (car part))
               part)

              ;; Actual rolling
              ((numberp (car part))
               (setq count (car part)
                     die (cdr part))
               
               (when (< count 0)
                 (setq count (abs count)
                       scale -1))
               
               (cons
                (cl-loop repeat count collect
                         (* (dnd-roll-die die) scale))
                (* die scale)))))))

(defun dnd-roll-collect-label-groups (roll)
  (cl-loop
   with groups = (make-hash-table :test 'equal)
   with label = nil
   for part in roll do
   (if (dnd-roll-label-p part)
       (setq label (dnd-roll-label-label part))
     (puthash label (append (gethash label groups) (list part)) groups))
   finally return groups))

(defun dnd-roll-group-labels (roll)
  (cl-loop
   with rolls = (dnd-roll-collect-label-groups roll)
   for roll-group being the hash-keys of rolls
   append (list (make-dnd-roll-label :label roll-group))
   append (gethash roll-group rolls)))

(defun dnd-roll-get-d20 (completed-roll)
  (let (found)
    (cl-loop for part in completed-roll until found do
             (--when-let (listp part)
               (when (= (cdr part) 20)
                 (setq found (caar part)))))
    found))

(defun dnd-roll-crit-spec (roll-spec)
  (cl-loop for part in roll-spec collect
           (cond
            ((or (numberp part) (dnd-roll-label-p part))
             part)
            (t
             (cons (* 2 (car part)) (cdr part))))))

(defun dnd-roll-result (completed-roll-or-equation &optional flag average)
  (let ((roll (if (nlistp completed-roll-or-equation)
                  (dnd-roll-perform completed-roll-or-equation)
                completed-roll-or-equation)))
    (cl-loop for part in roll sum
             (cond
              ((dnd-roll-label-p part)
               ;; TODO:  Modify a scaling factor based on the label as approriate.
               0)
              
              ((numberp part)
               part)

              ((= (cdr part) 20)
               (apply (if (equal flag :disadvantage) 'min 'max) (car part)))
              
              (t
               (if average
                   (* (/ (cdr part) 2) (length (car part)))
                 (apply '+ (car part))))))))

(defun dnd-roll-to-spec-string (completed-roll)
  (cl-loop
   with first-part = t
   for part in completed-roll
   unless first-part concat " "
   concat
   (cond
    ((dnd-roll-label-p part)
     (dnd-roll-label-label part))
    ((numberp part)
     (format "+%s" part))
    ((listp part)
     (format "+%s" (cl-loop for die in (car part) sum die))))
   do (setq first-part nil)))

(defun dnd-roll-replace-equation-parameters (equation parameters)
  (let (k v)
    (while parameters
      (setq k (substring (format "%s" (pop parameters)) 1)
            v (format "%s" (pop parameters)))
      (setq equation (s-replace k v equation))))
  equation)

(defun dnd-roll-replace-rolls (roll &rest results)
  (cl-loop
   for part in roll collect
   (pcase part
     (`(,(and (pred numberp) count) . ,(and (pred numberp) die))
      (--if-let (pop results)
          (cons `(,it) die)
        (cons count die)))
     (_ part))))

(defun dnd-roll-reduce (roll amount)
  "Reduces the performed portions of a roll, in order, by `amount' such that the total reduction equals `amount' (or less if amount was greater than the complete total, in which case the result of the roll will be 0"
  (cl-loop
   for part in roll collect
   (if (<= amount 0)
       part
     (cond
      ((numberp part)
       (--when-let (max 0 (- part amount))
         (setq amount (- amount (- part it)))
         it))
      ((listp part)
       (cons
        (cl-loop for die in (car part) collect
                 (--when-let (max 0 (- die amount))
                   (setq amount (- amount (- die it)))
                   it))
        (cdr part)))
      (t part)))))

(defun dnd-roll-remaining-rolls (roll &optional unperformed-only)
  (cl-loop
   for part in roll sum
   (pcase part
     (`(,(pred numberp) . ,(pred numberp))
      1)
     (`(,(pred listp) . ,(pred listp))
      (if unperformed-only
          0
        1))
     (_ 0))))

(defun dnd-roll-to-string (roll &optional preamble no-markdown)
  (let (rolls element roll-string op inside spec-only label groups first-in-group)
    (cl-loop for part in roll

             ;;
             if (dnd-roll-label-p part) do
             (when (dnd-roll-label-label part)
               (setq first-in-group t)
               (push (concat
                      (when inside " ")
                      (unless no-markdown "**")
                      (capitalize (substring (dnd-roll-label-label part) 0 (unless no-markdown -1)))
                      (unless no-markdown "**")
                      " ")
                     rolls))

             ;;
             else if (numberp part) do
             (when (not (= part 0))
               (if (and (or (not inside) first-in-group) (< part 0))
                   (push "-" rolls)
                 (when (and inside (not first-in-group))
                   (push (if (< part 0) " - " " + ") rolls)))
               
               (push (number-to-string (abs part)) rolls))
             (setq first-in-group nil)
             (setq inside t)

             ;;
             else if (symbolp part) do
             (when (and inside (not first-in-group))
               (push " + " rolls))
             (push (substring (format "%s" part) 1) rolls)
             (setq first-in-group nil)             

             ;;
             else do
             ;; Die rolls
             ;; Handle parsed vs. performed rolls
             (if (numberp (car part))
                 (setq spec-only t
                       op (if (< (car part) 0) " - " " + "))
               (setq op
                     (if (< (cdr part) 0) " - " " + ")))
             
             (when (and inside (not first-in-group))
               (push op rolls))

             (if spec-only
                 (setq element
                       (format "%dd%d"
                               (abs (car part))
                               (cdr part)))

               ;; Performed roll
               (setq element
                     (format "%sd%s( %s )" 
                             (length (car part))
                             (abs (cdr part))
                             (s-join ", "
                                     (mapcar
                                      (lambda (el)
                                        (prin1-to-string (abs el)))
                                      (car part))))))
             
             (push element rolls)
             (setq first-in-group nil)
             
             (setq inside t))
    (concat
     preamble
     (s-join "" (reverse rolls)))))

(defun dnd-roll-has-disadvantage ()
  (when (and (dnd-get-state :disadvantage)
             (not (dnd-get-state :advantage)))
    :disadvantage))

(defun dnd-roll-die (die)
  (1+ (random die)))

(defun dnd-roll-hitdice (dice)
  (max 1 (dnd-roll-result dice)))

;;;;;
(provide 'dnd-roll)
