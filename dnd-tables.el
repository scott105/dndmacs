(defun dnd-load-data-table (&rest name-or-id)
  (let (data)
    (cl-loop for table in name-or-id do
             (unless (ignore-errors
                       (setq data (cl-acons table (dnd-table-to-hash table) data)))
               (message "Warning:  No table '%s'" table)))
    data))

(defun dnd-weighted-table-with-header (data &optional weight-column)
  (let* ((type (nth (or weight-column 0) (car data)))
         (data (cddr data))
         (roll (unless (string= type "wt")
                 (dnd-roll-result type))))
    (dnd-weighted-table data weight-column roll)))

(defun dnd-weighted-table (data &optional weight-column forced-roll)
  (let* ((weight-column (or weight-column 0))
         (data (cl-loop for entry in data collect
                        (list
                         (--if-let (nth weight-column entry)
                             (if (stringp it)
                                 (string-to-number it)
                               it))
                         (car (last entry)))))
         (total (apply '+ (mapcar 'car data)))
         (roll (or forced-roll (random total)))
         entry)
    (while (and data (>= roll 0))
      (setq entry (pop data))
      (cl-incf roll (- (car entry))))
    (car (last entry))))

(defun dnd-get-table-raw (name range)
  (let ((data (org-table-get-remote-range name range)))
    (if (listp data)
        (mapcar (lambda (field)
                  (set-text-properties 0 (length field) nil field)
                  field)
                data)
      
      (set-text-properties 0 (length data) nil data)
      `(,data))))

(defun dnd-find-table (name-or-id)
  (save-match-data
    (let ((case-fold-search t) (id-loc nil)
	  ;; Protect a bunch of variables from being overwritten
	  ;; by the context of the remote table
	  org-table-column-names org-table-column-name-regexp
	  org-table-local-parameters org-table-named-field-locations
	  org-table-current-line-types org-table-current-begin-line
	  org-table-current-begin-pos org-table-dlines
	  org-table-current-ncol
	  org-table-hlines org-table-last-alignment
	  org-table-last-column-widths org-table-last-alignment
	  org-table-last-column-widths tbeg
	  buffer loc)

      (save-excursion
	(save-restriction
	  (widen)
	  (save-excursion
	    (goto-char (point-min))
	    (if (re-search-forward
		 (concat "^[ \t]*#\\+\\(tbl\\)?name:[ \t]*"
			 (regexp-quote name-or-id) "[ \t]*$")
		 nil t)
		(setq buffer (current-buffer) loc (match-beginning 0))
	      (setq id-loc (org-id-find name-or-id 'marker))
	      (unless (and id-loc (markerp id-loc))
		(user-error "Can't find remote table \"%s\"" name-or-id))
	      (setq buffer (marker-buffer id-loc)
		    loc (marker-position id-loc))
	      (move-marker id-loc nil)))
	  (with-current-buffer buffer
	    (save-excursion
	      (save-restriction
		(widen)
		(goto-char loc)
		(forward-char 1)
		(unless (and (re-search-forward "^\\(\\*+ \\)\\|[ \t]*|" nil t)
			     (not (match-beginning 1)))
		  (user-error "Cannot find a table at NAME or ID %s" name-or-id))
                (point-at-bol)))))))))
                

(defun dnd-table-goto (name-or-id &optional skip-to-data)
  (goto-char (dnd-find-table name-or-id))
  (if (not skip-to-data)
      1
    (forward-line 2)
    2))

(defun dnd-table-row-matches (values)
  (org-table-goto-column 1)
  (let ((matched t) elem (i 1))
    (while (and values matched)
      (unless (equal "*" (setq elem (pop values)) )
        (unless (equal elem (s-trim (org-table-get-field i)))
          (setq matched nil)))
      (cl-incf i))
    matched))

(defun dnd-table-get-row (name-or-id row)
  (cl-pairlis
   (dnd-get-table-raw name-or-id "@1$<..@1$>")
   (mapcar (lambda (el)
             (if (string= "" el)
                 nil
               el))
           (dnd-get-table-raw name-or-id (format "@%s$<..@%s$>" row row)))))

(defun dnd-table-kill-rows (name-or-id values)
  (save-excursion
    (dnd-table-goto name-or-id t)
    (while (org-at-table-p)
      (if (dnd-table-row-matches values)
          (org-table-kill-row)
        (forward-line)))))

(defun dnd-table-find-row (name-or-id values &optional last)
  (save-excursion
    (let ((i (dnd-table-goto name-or-id t)))

      (when last
        (let ((start (1+ last)))
          (forward-line (- start i))
          (setq i start)))
      
      (while (and (org-at-table-p) (not (dnd-table-row-matches values)))
        (forward-line)
        (cl-incf i))
      (when (org-at-table-p) i))))

(defun dnd-table-get-player-row (name-or-id player)
  (dnd-table-get-row name-or-id (dnd-table-find-row name-or-id `("*" ,player))))

(defun dnd-table-set-field (name-or-id row &rest pairs)
  (save-excursion
    (dnd-table-goto name-or-id)
    (let ((header (dnd-get-table-raw name-or-id "@1$<..@1$>"))
          key val)

      (cl-loop while pairs do
               (setq key (pop pairs)
                     val (pop pairs))
               (--if-let (cl-position key header :test 'equal)
                   (org-table-put row (1+ it) val))))
    (org-table-align)))

(defun dnd-table-add-row (name-or-id data)
  (save-excursion
    (dnd-table-goto name-or-id)
    (if (not (org-at-table-p))
        (error "table-add-row:  Can't find table %s" name-or-id)

      (goto-char (org-table-end))

      (cl-loop for val in data do
               (insert "|" val))
      (insert "\n")
      (org-table-align))))

(defun dnd-parse-compact-field (s)
  (let ((data (s-split "[ ]*,[ ]*\\|[ ]+" s)))
    (mapcar (lambda (el)
              (let* ((kvp (s-split ":" el))
                     (k (s-trim (car kvp)))
                     (v (cadr kvp)))
                `(,k . ,(if v (s-trim v) t))))
            data)))

(defun dnd-sort-compact-by-numeric-value (data)
  (sort data
        (lambda (a b) (>
                       (string-to-number (cdr a)) 
                       (string-to-number (cdr b))))))

(defun dnd-convert-compact-field (data)
  (let ((entries (cl-remove-duplicates data
                                       :key #'car
                                       :test 'equal
                                       :from-end t)))
    (s-join ", "
            (cl-loop for entry in entries collect
                     (format "%s:%s"(car entry) (cdr entry))))))

(defun dnd-table-to-list (name-or-id &optional include-header)
  (save-mark-and-excursion
    (dnd-table-goto name-or-id)
    (let* ((lines (s-lines (buffer-substring-no-properties (point) (1- (org-table-end))))))

      (unless include-header
        (setq lines (cddr lines)))
        
      (cl-loop for r in lines collect
               (mapcar 's-trim
                       (butlast (cdr (s-split "|" (s-trim r)))))))))

(defun dnd-table-to-assoc (name-or-id)
  (save-mark-and-excursion
    (dnd-table-goto name-or-id)
    
    (let* ((lines (s-lines (buffer-substring-no-properties (point) (1- (org-table-end)))))
           (rows
            (mapcar (lambda (row) (mapcar 's-trim (cddr row)))
                    (mapcar (lambda (l) (s-split "|" l)) lines)))
           (header (butlast (pop rows))))

      ;; Note:  The, the cddr above is to get rid of the "!" column as well as an empty from the string split
      
      ;; Kill the hline
      (pop rows)

      ;; Create the alist
      (cl-loop for r in rows collect
               (let* ((entry
                       (cl-loop for field in header
                                for val in r collect

                                (if (s-match "^\\*" field)
                                    `(,(s-replace "*" "" field) . ,(dnd-parse-compact-field val))
                                  `(,field . ,(if (string= val "") nil val))))))
                 (cons (cdar entry) entry))))))
                

(defun dnd-table-to-hash (name-or-id)
  (save-mark-and-excursion
    (dnd-table-goto name-or-id)
    
    (let* ((data (make-hash-table :test 'equal))
           (lines (s-lines (buffer-substring-no-properties (point) (1- (org-table-end)))))
           (rows
            (mapcar (lambda (row) (mapcar 's-trim (cddr row)))
                    (mapcar (lambda (l) (s-split "|" l)) lines)))
           (header (butlast (pop rows))))

      ;; Note:  The, the cddr above is to get rid of the "!" column as well as an empty from the string split
      
      ;; Kill the hline
      (pop rows)

      (cl-loop for r in rows do
               (let ((entry (make-hash-table :test 'equal))
                     (keys header)
                     field)

                 (cl-loop for el in r do
                          (setq field (pop keys))
                          
                          (when (not (string= "" el))
                            (if (s-match "^\\*" field)
                                (puthash (s-replace "*" "" field)
                                         (dnd-parse-compact-field el)
                                         entry)
                              (puthash field el entry))))
                 
                 (puthash (car r) entry data)))
      data)))

(defun dnd-sort-actors-initiative ()
  (interactive)
  (save-excursion
    (dnd-table-goto "Actors" t)
    (org-table-goto-column 3)
    (org-table-sort-lines nil ?N)))

(defun dnd-table-set-from-assoc (name-or-id data)
    (save-mark-and-excursion
      (dnd-table-goto name-or-id)

      (let* ((el (org-element-at-point))
             (begin (org-element-property :begin el))
             (end (org-element-property :end el)))

        (delete-region begin end)
        (insert "\n")
        (forward-line -1)
        
        (insert
         "#+Name: " name-or-id "\n| ! |"
         (cl-loop for field in (mapcar 'car (cdadr data)) concat
                  (concat " " field " |")))

        (org-table-insert-hline)
        (beginning-of-line)
        (forward-line 2)

        (insert
         (cl-loop for (_ . entry) in data concat
                  (concat "| | " (s-join " | " (mapcar 'cdr entry)) " |\n"))
         "\n")

        (forward-line -2)
        (org-table-align)
        (forward-line 2))))

(defun dnd-unique-actor-name (basename &optional only-one)
  (let ((actors (cdr (dnd-get-table-raw "Actors" "@<$char..@>$char")))
        (i 1)
        name)

    (while (member (setq name
                         (if (and (= i 1) only-one)
                             basename
                           (format "%s_%s" basename i))) actors)
      (cl-incf i))

    name))

(defun dnd-pivot-table-columns (data &optional add-hlines)
  (let ((result
         (cl-loop
          for i from 0 below (length (car data)) collect
          (-select-column i data))))
    (if add-hlines
        (dnd-add-table-hlines result)
      result)))

(defun dnd-add-table-hlines (tbl)
  `(hline
    ,(car tbl)
    hline
    ,@(cdr tbl)
    hline))

;;;;;
(provide 'dnd-tables)
