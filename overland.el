(require 'websocket)

(setq websocket-debug t)

(defvar-local overland-loadparty-callback nil)
(defvar-local overland-party-files-callback nil)
(defvar-local overland-loadmap-callback nil)

(setq overland--handlers
      '((broadcast . overland-broadcast-handler)
        (auth-apikey-response . overland-auth-apikey-response-handler)
        (loadparty-response . overland-loadparty-response-handler)
        (party-files-response . overland-party-files-response-handler)
        (loadmap-response . overland-loadmap-response-handler)
        (chat . overland-chat-handler)
        (delta . overland-delta-response-handler)
        (upload-party-sheets-response . overland-upload-party-sheets-response-handler)))

(defun overland-get-query-path (&rest path)
  (let ((result '())
        (parts))
    (mapc (lambda (part)
            (setq result (append result (s-split "/" part))))
          path)
    result))

(defun overland-query-data (data &rest path)
  (cl-loop for key in (apply 'overland-get-query-path path) do
           (cond
            ((listp data)
             (setq data (cdr (assoc-string key data))))
            ((hash-table-p data)
             (setq data (gethash key data)))))
  data)

(defun overland--dispatch (ws data)
  (let* ((type (cdr (assoc 'type data)))
         (status (cdr (assoc 'status data)))
         (payload (cdr (assoc 'payload data)))
         handler)

    (if (string= type "broadcast")
        (overland--dispatch ws payload)

      ;; Non-broadcast
      ;; TODO:  Handle error codes uniformly
      
      (--if-let (cdr (assoc (read type) overland--handlers))
          (apply it `(,ws ,payload))
        (message "overland:  Unhandled: [%s] %s: %s" status type payload)))))

;;;;;;;;;;
(defun overland-connect (url)
  (websocket-open
   url
   :on-message (lambda (ws frame)
                 (overland--dispatch ws (json-read-from-string
                                         (websocket-frame-text frame))))
   :on-close (lambda (ws)
               (message "ws: closed"))))

(defun overland-disconnect (ws)
  (websocket-close ws))

(defun overland-send (ws data)
  (websocket-send-text ws (json-encode data)))

;;;;;;;;;;
(defun overland-api-key-auth (ws key)
  (overland-send
   ws
   `((type . auth-apikey)
     (payload . ((key . ,key))))))

(defun overland-loadparty (ws party)
  (overland-send
   ws
   `((type . loadparty)
     (payload . ((party . ,party))))))

(defun overland-party-files (ws party)
  (overland-send
   ws
   `((type . party-files)
     (payload . ((party . ,party))))))

(defun overland-party-files-synchronous (ws party)
  (let* (done
         results
         (overland-party-files-callback
          (lambda (data)
            (setq
             done t
             results data))))

    (overland-party-files ws party)

    (while (not done)
      (sleep-for 0.01))
    (cdr (assoc 'files results))))

(defun overland-loadmap (ws map)
  (overland-send
   ws
   `((type . loadmap)
     (payload . ((map . ,map))))))

(defun overland-party-switchmap (ws map)
  (overland-send
   ws
   `((type . party-switchmap)
     (payload . ((map . ,map))))))

(defun overland-delta (ws delta)
  (overland-send
   ws
   `((type . delta)
     (payload . ,delta))))

(defun overland-upload-party-sheets (ws sheets)
  (overland-send
   ws
   `((type . upload-party-sheets)
     (payload . ,(overland-data-to-alist `((sheets . ,sheets)))))))

(defun overland-data-to-alist (data)
  (cond
   ((hash-table-p data)
      (cl-loop for k being the hash-keys of data collect
               (--when-let (gethash k data)
                 (cons k (overland-data-to-alist it)))))

   ((and (listp data) (-cons-pair? (car data)))
    (cl-loop for (k . v) in data collect
             (cons k (overland-data-to-alist v))))

   (t
    data)))

;;;;;
(defun overland-poi-to-org (poi tag &optional annotations)
  (cl-loop for (name . info) in poi collect
           (org-element-create
            'headline `(:level 2 :title ,(format "%s" name) :tags (,tag))
            `(property-drawer
              nil
              ,@(cl-loop for (key . val) in info collect
                         `(node-property (:key ,key :value ,val))))
            (or (overland-query-data annotations (format "poi:%s" name)) ""))))

(defun overland-frag-to-org (frags tag)
  (cl-loop for (name . info) in frags collect
           (org-element-create
            'headline `(:level 2 :title ,(format "%s" name) :tags (,tag))
            `(property-drawer
              nil
              ,(cl-loop for (key . val) in info collect
                        `(node-property (:key ,key :value ,val)))))))

(defun overland-map-data-to-org (data)
  (let ((frags (overland-query-data data "fragments"))
        (icons (overland-query-data data "icons"))
        (poi (overland-query-data data "poi"))
        (poi-pending (overland-query-data data "poi_pending"))
        (annotations (overland-query-data data "annotations")))

    (insert
     (org-element-interpret-data
      (org-element-create
       (format "#+STARTUP: indent inlineimages\n#+title: %s\n#+id: %s\n\n"
               (overland-query-data data "name")
               (overland-query-data data "id"))
       nil

       `(headline
         (:level 1 :title "Data")
         (property-drawer
          nil
          ,(cl-loop
            with blacklist = '(fragments _id key icons poi poi_pending annotations)
            for (key . val) in data
            unless (-contains? blacklist key) collect
            `(node-property (:key ,key :value ,val)))))

       `(headline
         (:level 1 :title "Fragments")
         ,@(overland-frag-to-org frags "frag"))

       `(headline
         (:level 1 :title "Places")
         ,@(overland-poi-to-org poi "poi" annotations)
         ,@(overland-poi-to-org poi-pending "pending" annotations)))))))

(defun overland-org-metadata-to-data (ast)
  (car
   (org-element-map ast 'headline
     (lambda (el)
       (when (string= (org-element-property :raw-value el) "Data")
         (cl-loop for (k . v) in
                  (car
                   (org-element-map el 'property-drawer
                     (lambda (drawer)
                       (org-entry-properties (org-element-property :begin drawer) 'standard))))
                  unless (string= k "CATEGORY")
                  collect (cons (downcase k) v)))))))

(defun overland-org-frags-to-data (ast type)
  (org-element-map ast 'headline
    (lambda (el)
      (when (member type (org-element-property :tags el))
        (cons
         (org-element-property :raw-value el)
         (cl-loop for (k . v) in 
                  (car
                   (org-element-map el 'property-drawer
                     (lambda (drawer)
                       (org-entry-properties (org-element-property :begin drawer) 'standard))))
                  unless (string= k "CATEGORY")
                  collect (cons (downcase k) v)))))))

(defun overland-org-poi-to-data (ast type)
  (org-element-map ast 'headline
    (lambda (el)
      (when (member type (org-element-property :tags el))
        (cons
         (org-element-property :raw-value el)
         (cl-loop for (k . v) in 
                  (car
                   (org-element-map el 'property-drawer
                     (lambda (drawer)
                       (org-entry-properties (org-element-property :begin drawer) 'standard))))
                  unless (string= k "CATEGORY")
                  collect (cons (downcase k) v)))))))

(defun overland-org-annotation-to-data (ast)
  (org-element-map ast 'headline
    (lambda (el)
      (--when-let (org-element-property :tags el)
        (when (or (member "poi" it)
                  (member "pending" it))
          (let ((drawer-end
                 (car
                  (org-element-map el 'property-drawer
                    (lambda (drawer) (org-element-property :end drawer))))))

            (cons
             (format "%s" (org-element-property :raw-value el))
             (buffer-substring-no-properties
              drawer-end
              (org-element-property :contents-end el)))))))))

(defun overland-org-to-data ()
  (let ((ast (org-element-parse-buffer)))
    `((id . ,overland-map-id)
      ,@(overland-org-metadata-to-data ast)
      (fragments . ,(overland-org-frags-to-data ast "frag"))
      (poi . ,(overland-org-poi-to-data ast "poi"))
      (poi_pending . ,(overland-org-poi-to-data ast "pending"))
      (annotations . ,(overland-org-annotation-to-data ast)))))

(defun overland-org-insert-fragment (&optional name)
  (interactive)

  (--when-let
      (helm
       :sources
       (helm-build-sync-source "Files"
         :candidates
         (lambda ()
           (cl-loop for (file . name) in
                    (overland-party-files-synchronous
                     overland-ws
                     (overland-query-data overland-data "owner"))
                    as file = (format "%s" file)
                    as name = (format "%s" name)
                    collect
                    `(,name . (,name . ,file))))))

    (when (= helm-exit-status 0)
      (insert
       (org-element-interpret-data
        (org-element-create
         'headline `(:level 2 :title ,(car it) :tags ("frag") :post-blank 1)
         `(property-drawer
           ()
           (node-property (:key "img" :value ,(cdr it)))
           (node-property (:key
                           "pixelsPerMile"
                           :value
                           ,(overland-query-data
                             overland-data
                             "pixelsPerMile")))
           (node-property (:key "x" :value "0"))
           (node-property (:key "y" :value "0")))))))))

(defun overland-org-insert-poi (&optional name)
  (interactive)
  ;; TODO:  Go to 'Places'
  (insert "** ")
  (--when-let (point)
    (insert
     "  :poi:
:PROPERTIES:
:icon: 0
:x: 0
:y: 0
:hidden: t
:END:\n")
    (goto-char it)
    (when name
        (insert name))))

(defun overland-data-equal-test (a b)
  (cond
   ((and (listp a) (listp b))
    (cl-loop with fail
             for (k . v) in a
             do (setq
                 fail
                 (not
                  (overland-data-equal-test
                   v
                   (overland-query-data b (format "%s" k)))))
             if fail return nil
             finally return t))
   (t
    (string= (format "%s" a)
             (format "%s" b)))))

(defun overland-data-key-delta (a b)
  `(,(cl-loop for entry in a
              unless (cl-assoc (car entry) b :test 'string=) collect entry)
    ,(cl-loop for entry in b
              unless (cl-assoc (car entry) a :test 'string=) collect entry
              else unless (overland-data-equal-test
                           (cdr (cl-assoc (car entry) a :test 'string=))
                           (cdr entry)) collect entry)))

(defun overland-calculate-map-delta (data buffer-data)
  ;; Compare the previously loaded data with the current buffer state and generate the overland delta format

  ;; Create datasets from the buffer data
  ;; Additions = (local but not in data || local and in data but differ)
  ;; Deletions = (in data but not local)

  ;; Need to be done for poi, poi_pending, and annotations

  (cl-loop 
   with additions = (make-hash-table :test 'equal)
   with deletions = (make-hash-table :test 'equal)
   with delta = (cl-loop for section in '("fragments" "poi" "poi_pending" "annotations") collect
                         (cons
                          section
                          (overland-data-key-delta
                           (overland-query-data data section)
                           (overland-query-data buffer-data section))))
   
   for (s d a) in delta

   when a do
   (puthash s (append (gethash s additions) a) additions)

   when d do
   (puthash s (append (gethash s deletions) d) deletions)
   
   finally return
   `((map . ,(overland-query-data data "id"))
     (a . ,additions)
     (d . ,(cl-loop for k being the hash-keys of deletions collect
                    `(,k . ,(mapcar 'car (gethash k deletions))))))))

(defun overland-save-buffer ()
  (interactive)
  (let ((buffer-data (overland-org-to-data)))
    ;; TEMP:  Saving off this new data
    (overland-delta
     overland-ws
     (overland-calculate-map-delta overland-data buffer-data))
    (setq overland-data buffer-data)))

(defun overland-data-to-tiled (data)
  "Converts map data into a Tiled file"

  (let ((map-ppm (string-to-number (overland-query-data data "pixelspermile")))
        poi-layer image-layers)

    (setq poi-layer
          (cl-loop
           with poi
           for (name . info) in (overland-query-data data "poi")
           collect
           `((name . ,name)
             (gid . ,(1+ (string-to-number (overland-query-data info "icon"))))
             (x . ,(* (string-to-number (overland-query-data info "x")) map-ppm))
             (y . ,(* (string-to-number (overland-query-data info "y")) map-ppm))
             (id . 2)
             (rotation . 0)
             (type . ,(if (overland-query-data info "hidden") "hidden" ""))
             (width . 64)
             (height . 64))
           into poi
           finally return
           `(((id . 2)
             (name . "poi")
             (type . "objectgroup")
             (opacity . 1)
             (visible . t)
             (x . 0)
             (y . 0)
             (objects . ,(apply 'vector poi))))))
  
    (cl-loop
     for (name . info) in (overland-query-data data "fragments") collect
     `((id . 2)
       (image . ,(overland-query-data info "img"))
       (name . ,name)
       (opacity . 1)
       (type . "imagelayer")
       (visible . t)
       (offsetx . ,(* (string-to-number (overland-query-data info "x")) map-ppm))
       (offsety . ,(* (string-to-number (overland-query-data info "y")) map-ppm)))
     into image-layers

     finally return
     `(("height" . 100)
       ("hexsidelength" . 18)
       ("infinite" . true)
       ("layers" . ,(apply 'vector (append image-layers poi-layer)))
       ("nextlayerid" . 100)
       ("nextobjectid" . 100)
       ("orientation" . "hexagonal")
       ("renderorder" . "right-down")
       ("staggeraxis" . "x")
       ("staggerindex" . "odd")
       ("tiledversion" . "1.2.2")
       ("tileheight" . 32)
       ("tilesets" . [(("columns" . 16)
                       ("firstgid" . 1)
                       ;; TODO:
                       ("image" . "overland-icons.png")
                       ("imageheight" . 1024)
                       ("imagewidth" . 1024)
                       ("margin" . 0)
                       ("name" . "icons")
                       ("spacing" . 0)
                       ("tilecount" . 256)
                       ("tileheight" . 64)
                       ("tilewidth" . 64)
                       ("tileoffset" .
                        (("x" . -32)
                         ("y" . 32))))])
       ("tilewidth" . 32)
       ("type" . "map")
       ("version" . 1.2)
       ("width" . 100)))))

(defun overland-download-map-images (data destination base-url)
  (make-directory (format "%s/uploads" destination) t)

  (cl-loop
   with downloads
   for (name . info) in (overland-query-data data "fragments")
   as img = (overland-query-data info "img")
   if img collect
   (cons (format "%s/%s" base-url img)
         (format "%s/%s" destination img))
   into downloads

   finally
   (cl-loop for (src . dest) in downloads do
            (url-copy-file src dest t))
   
   ;; TODO:  Read any icon sheet information
   (url-copy-file
    (format "%s/static/%s" base-url "overland-icons.png")
    (format "%s/%s" destination "overland-icons.png")    
    t)))

(defun overland-sanitize-map-id (id)
  (replace-regexp-in-string "[:]" "-" id))  

(defun overland-data-to-tiled-file (data destination)
  (let ((id (overland-query-data data "id")))
    (with-temp-buffer
      (insert
       (json-encode
        (overland-data-to-tiled data)))
      (write-file
       (overland-temp-filename id destination)))))

(defun overland-load-tiled-file (path)
  (cl-loop
   with frags
   with poi
   for layer across
   (overland-query-data
    (json-read-file path)     
    "layers")
   as type = (overland-query-data layer "type")

   if (string= type "imagelayer") collect
   (cons (overland-query-data layer "name")
         `((image . ,(overland-query-data layer "image"))
           (x . ,(overland-query-data layer "offsetx"))
           (y . ,(overland-query-data layer "offsety"))))
   into frags

   else if (string= type "objectgroup") append
   (cl-loop for obj across (overland-query-data layer "objects") collect
            (cons (overland-query-data obj "name")
                  `((icon . ,(overland-query-data obj "gid"))
                    (x . ,(overland-query-data obj "x"))
                    (y . ,(overland-query-data obj "y"))
                    (hidden . ,(string= (overland-query-data obj "type") "hidden")))))
   into poi

   finally return
   `((frags . ,frags)
     (poi . ,poi))))  

(defun overland-data-update-from-tiled (data tiled)
  ;; Run through all fragments and poi in tiled
  ;; Anything existing in data, updat
  ;; Anything new, add

  ;; Run through all fragments and poi in data
  ;; if missing in tiled, delete

  ;; This can be done against the current data and then inserted back into the buffer
  (cl-loop
   with map-ppm = (string-to-number (overland-query-data data "pixelspermile"))
   with tiled-frags = (overland-query-data tiled "fragments")
   with tiled-poi = (overland-query-data tiled "poi")

   for (k . v) in data collect
   (pcase k
     ;; ('frags "FRAGS")
     ('poi
      (cons k
            ;; We just use all of the data from Tiled in this case
            ;; .That does mean that any new entries in Emacs will get lost
            ;; .Actually merging could be done as a feature in the future; for now it just requires some discipline in the workflow.
            (cl-loop
             for (name . info) in tiled-poi collect
             (cons
              name
              (cl-loop
               for (type . val) in info collect
               (pcase type
                 ((or 'x 'y) (cons type (/ val map-ppm)))
                 ('icon (cons type (1- val)))
                 (_ (cons type val))))))))
     (_ (cons k v)))))

(defun overland-org-update-from-tiled-file ()
  (interactive)
  (let* ((data (overland-org-to-data))
         (id (overland-query-data data "id"))
         (path (overland-temp-filename id))
         (tiled (overland-load-tiled-file path)))
    (--when-let (overland-data-update-from-tiled data tiled)
      (erase-buffer)
      (overland-map-data-to-org it))))

(defun overland-temp-directory ()
  (concat (temporary-file-directory) "overland/"))

(defun overland-temp-filename (id &optional dir)
  (format "%s/%s.json"
          (or dir (overland-temp-directory))
          (overland-sanitize-map-id id)))

(defun overland-current-buffer-to-tiled ()
  (interactive)
  (let* ((data (overland-org-to-data))
         (destination (overland-temp-directory))
         (id (overland-sanitize-map-id (overland-query-data data "id"))))
    (overland-download-map-images data destination overland-map-source)
    (overland-data-to-tiled-file data destination)
    (overland-open-tiled destination id)))

(defun overland-open-tiled (destination id)
  (let (exe param)
    (--if-let (or
               (executable-find "Tiled")
               (executable-find "c:/Program Files/Tiled/Tiled.exe")
               (executable-find "/Applications/Tiled.app/Contents/MacOS/Tiled"))
        (setq exe it
              param (format "%s%s.json" destination id))
      ;;
      (--when-let (or
                   (executable-find "explorer"))

        (setq exe it
              param (replace-regexp-in-string
                     "[/]" "\\\\" (format "%s" destination)))))

    (when exe
      (call-process exe nil 0 nil param))))

(defun overland-party-switchmap-to-loaded ()
  (interactive)
  (--when-let (overland-query-data overland-data "id")
    (overland-party-switchmap overland-ws it)))

;;;;;
(defun overland-auth-apikey-response-handler (ws data)
  (message "Auth handler: %s" data)
  (overland-loadparty ws (overland-query-data data "party")))

(defun overland-loadparty-response-handler (ws data)
  (message "Loadparty handler: %s" data)

  ;; TODO:  Figure out how to store the maps
  (--when-let overland-loadparty-callback
    (apply it `(,data))))

(defun overland-party-files-response-handler (ws data)
  (message "Party-files handler: %s" data)
    (--when-let overland-party-files-callback
      (apply it `(,data))))

(defun overland-loadmap-response-handler (ws data)
  (message "Loadmap handler: %s" data)

  (let* ((id (overland-query-data data "id"))
         (name (overland-query-data data "name"))
         (map-buffer (get-buffer-create (format "overland:[%s] %s" id name))))

    (with-current-buffer map-buffer
      ;; TODO:  Had some issues with this with values not updating correctly.
      ;; (defvar-local overland-ws ws)
      ;; (defvar-local overland-data data)
      (setq overland-ws ws
            overland-data data
            overland-map-id id)
      (erase-buffer)
      (org-mode)
      (overland-map-data-to-org data))

    (--when-let overland-loadmap-callback
      (apply it `(,data ,map-buffer)))))

(defun overland-delta-response-handler (ws data)
  (message "Map delta handler: %s" data))

(defun overland-chat-handler (ws data)
  (message "Chat: %s" data))

(defun overland-upload-party-sheets-response-handler (ws data)
  (message "Sheet upload: %s" data))

;;;;;;;;;;;;
(provide 'overland)
