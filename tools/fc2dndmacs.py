import sys
import xmltodict
import re
import io
from collections import OrderedDict

_spellCopyFields = {
    'name',
    'level',
    'school',
    'ritual',
    'time',
    'range',
    'components',
    'duration',
    'classes',
    }

_spellSchoolLabels = {
    'A': 'abjuration',
    'C': 'conjuration',
    'D': 'divination',
    'EN': 'enchantment',
    'EV': 'evocation',
    'I': 'illusion',
    'N': 'necromancy',
    'T': 'transmutation',
    'U': 'universal'
}

_spellDataLabels = [
    'time',
    'range',
    'components',
    'duration',
    'classes',
]


def SpellSchoolAndLevelLabel(school, level):
    label = None

    schoolLabel = _spellSchoolLabels[school]
    
    if level == '0':
        return '%s cantrip' % schoolLabel
    
    elif level == '1':
        label = '1st'
    elif level == '2':
        label = '2nd'
    elif level == '3':
        label = '3rd'
    else:
        label = level + 'th'

    return '%s-level %s' % (label, schoolLabel)


def SpellToOrg(fptr, data):
    fptr.write(u'* %s\n' % data['name'].upper())
    fptr.write(u'/%s' % SpellSchoolAndLevelLabel(data['school'], data['level']))

    if 'ritual' in data:
        fptr.write(u' (ritual)')
        
    fptr.write(u'/\n')

    for k in _spellDataLabels:
        if k in data:
            fptr.write(u'  - %s :: %s\n' % (k, data[k]))
    
    #
    for text in DataList(data['text']):
        fptr.write(u'%s\n' % (text if text is not None else ''))

    fptr.write(u'\n')


_monsterCopyFields = [
    'size',
    'languages',
    'cr',
    'spells',
    'slots',
]


_monsterCRtoXP = {
    '0': 10,
    '1/8': 25,
    '1/4': 50,
    '1/2': 100,
    '1': 200,
    '2': 450,
    '3': 700,
    '4': 1100,
    '5': 1800,
    '6': 2300,
    '7': 2900,
    '8': 3900,
    '9': 5000,
    '10': 5900,
    '11': 7200,
    '12': 8400,
    '13': 10000,
    '14': 11500,
    '15': 13000,
    '16': 15000,
    '17': 18000,
    '18': 20000,
    '19': 22000,
    '20': 25000,
    '21': 33000,
    '22': 41000,
    '23': 50000,
    '24': 62000,
    '25': 75000,
    '26': 90000,
    '27': 105000,
    '28': 120000,
    '29': 135000,
    '30': 155000,
}

_ocrNumberMap = {
    'l': '1',
}

_damageTypeRegex = r'(acid|bludgeoning|cold|fire|force|lightning|necrotic|piercing|poison|psychic|radiant|slashing|thunder)'

_silveredRegex = r'(bludgeoning, piercing,( and|) slashing( damage|) from nonmagical (attacks not made with silvered weapons|weapons that aren\'t silvered))'

def _reOCRCharToNumber(m):
    return _ocrNumberMap[m.groups()[0]]


def CleanRollSpec(spec):
    # Scrub through a roll spec and look for typos/bad OCR data
    return re.sub(r'([A-Za-ce-z])', _reOCRCharToNumber, spec)


def CleanAttackDescription(desc):
    # Currently this is just to address certain common typos in damage names
    return desc.replace('bludegoning', 'bludgeoning')


def DataList(data):
    # HACK:  xmltodict only uses list for multiple element
    if type(data) is not list:
        return [data]
    return [e for e in data if e is not None]


def HasData(data, key):
    return key in data and data[key] is not None


def Property(key, val):
    return unicode((':%s:' % key).ljust(12) + (' %s\n' % val))


def WriteProperty(fptr, key, val):
    fptr.write(Property(key, val))
    

def MonsterHP(hp):
    return re.sub(r'\d+\s*\((.*)\)', r'\1', hp)


def MonsterAC(ac):
    return re.sub(r'(\d+).*', r'\1', ac)


def WriteMonsterList(fptr, label, data):
    entries = []

    for node in DataList(data):
        if not node:
            continue
        
        for entry in re.split('\s*,\s*', node):
            entries.append(entry.lower().replace(' ', ':').replace('+', ''))

    WriteProperty(fptr, label, ' '.join(entries))


def WriteMonsterSpeed(fptr, data):
    #TEMP:  Same matching as MonsterAC
    WriteProperty(fptr, 'speed', MonsterAC(data))


def WriteMonsterDescription(fptr, data):
    abilities = OrderedDict()
    
    if 'trait' in data:
        for trait in DataList(data['trait']):
            abilities[trait['name']] = DataList(trait['text'])

    if 'action' in data:
        for action in DataList(data['action']):
            if not 'attack' in action:
                abilities[action['name']] = DataList(action['text'])
                
    if len(abilities) > 0:
        fptr.write(u'\n*Abilities*\n')

        for k, v in abilities.items():
            fptr.write(u'  - %s :: %s\n' % (k, v[0]))

            for text in v[1:]:
                if text:
                    fptr.write(u'    - %s\n' % text)

    
def WriteMonsterAttacks(fptr, data):
    first = True
    cleanResult = True
    
    for action in data:
        if HasData(action, 'attack'):
            name = action['name']
            desc = CleanAttackDescription('\n'.join(DataList(action['text'])).lower())

            #TODO:  May need to extract the damage types and then detect the different groupings in the separate weapon entries
            #.The problem is that if there are different variants of the same attack with different types
            #.Could just enter them manually... Most of these things don't matter much since the players rarely have any immunities
            #.Another option is to only do this detection when it's completely obvious and not otherwise

            dmgTypes = list(set(re.findall(_damageTypeRegex + '\s+damage', desc)))

            attackRangeMatch = re.findall(r'(?:reach|range)\s+([0-9/]+)\s+ft', desc)
            attackRanges = []
            
            if attackRangeMatch:
                for m in attackRangeMatch:
                    for r in m.split('/'):
                        attackRanges.append(int(r))
                
                attackRanges.sort()
                attackRanges = ' ' + '/'.join(map(str, attackRanges))

            for attack in DataList(action['attack']):
                if not attack:
                    continue
                
                attackName, tohit, dmg = attack.split('|')

                dmg = CleanRollSpec(dmg)

                if not dmg:
                    dmg = "0"
                    
                elif not dmgTypes:
                    print 'NO DAMAGE: %s: %s' % (name, desc)
                    cleanResult = False

                #Handle matching damage output and types
                dmgGroups = re.findall(r'((?:\d+d\d+(?:(?:\s*[+-]\s*\d+)|))|\d+)', dmg)

                if dmg and dmgTypes:
                    dmg = []
                    for i in range(len(dmgGroups)):
                        groupType = None
                        if i >= len(dmgTypes):
                            groupType = dmgTypes[0]
                        else:
                            groupType = dmgTypes[i]

                        dmg.append('{}: {}'.format(groupType, dmgGroups[i]))
                    #
                    dmg = '({})'.format(' '.join(dmg))

                if attackName != name:
                    if attackName == '':
                        attackName = name
                    else:
                        attackName = '%s:%s' % (name, attackName)

                WriteProperty(fptr, 'attack%s' % ('' if first else '+'), '("%s" %s %s%s)' % (attackName, tohit if tohit else 0, dmg, attackRanges or '')),
                first = False

    return cleanResult


def WriteMonsterCasting(fptr, data):
    for trait in DataList(data):
        if trait['name'] == 'Spellcasting':
            stat = re.search(r'spellcasting ability is (str|dex|con|int|wis|char)', DataList(trait['text'])[0].lower())
            if stat:
                WriteProperty(fptr, 'casting', stat.groups()[0])

                
def WriteImmunities(fptr, data, field, target):
    if field in data:
        rawData = data[field]
        silvered = False

        silveredMatch = re.search(_silveredRegex, rawData)
        if silveredMatch:
            silvered = True
            rawData = rawData.replace(silveredMatch.group(0), "")
        
        typeList = re.findall(_damageTypeRegex, rawData)

        if silvered:
            if target == 'vulnerable':
                typeList.append('magical-or-silver')
            else:
                typeList.append('non-magical-or-silver')
        
        WriteProperty(fptr, target, ' '.join(typeList))

        
def WriteMonsterType(fptr, data):
    typeMatch = re.match(r'([^ ,]+)\s*(?:\((.*?)\)|)\s*,\s*(.*)', data)

    if typeMatch:
        thisType, race, source = typeMatch.groups()

        WriteProperty(fptr, 'type', thisType)

        if race:
            WriteProperty(fptr, 'race', race)
            
        WriteProperty(fptr, 'source', source)
    

def MonsterToOrg(fptr, data):
    fptr.write(u'* %s  :character:\n:PROPERTIES:\n' % (data['name']))

    # NOTE:  There may be typos.  Found at least one instance of 'l' instead of '1'.  Maybe check for letters that aren't 'd' in all roll specs.
        
    # Data
    WriteProperty(fptr, 'name', data['name'].lower())
    WriteMonsterType(fptr, data['type'])
    WriteProperty(fptr, 'stats', '%s %s %s %s %s %s' % (data['str'], data['dex'], data['con'], data['int'], data['wis'], data['cha']))

    for field in _monsterCopyFields:
        if HasData(data, field):
            WriteProperty(fptr, field, data[field])

    WriteProperty(fptr, 'hp', MonsterHP(data['hp']))
    WriteProperty(fptr, 'ac', MonsterAC(data['ac']))
    WriteProperty(fptr, 'xp', _monsterCRtoXP.get(data['cr'], 0))

    if 'trait' in data:
        WriteMonsterCasting(fptr, data['trait'])
    
    # TODO:
    # .Maybe store some of the truncated data in informational fields (conditionImmune, senses, AC mods, other speeds)

    WriteMonsterSpeed(fptr, data['speed'])

    if HasData(data, 'save'):
        #HACK:  dndmacs uses 'char' instead of 'cha' (should probably change that)
        WriteMonsterList(fptr, 'saves', data['save'].lower().replace('cha', 'char'))
    
    if HasData(data, 'skill'):
        WriteMonsterList(fptr, 'skills', data['skill'])
    
    if HasData(data, 'action'):
        if not WriteMonsterAttacks(fptr, DataList(data['action'])):
            print u'Dirty Entry: %s' % data['name']

    if HasData(data, 'resist'):
        WriteImmunities(fptr, data, 'resist', 'resistant')

    if HasData(data, 'immune'):
        WriteImmunities(fptr, data, 'immune', 'immune')
    
    if HasData(data, 'vulnerable'):
        WriteImmunities(fptr, data, 'vulnerable', 'vulnerable')
    
    fptr.write(u':END:\n')

    # Descriptive text
    WriteMonsterDescription(fptr, data)

    fptr.write(u'\n')
    

def Convert(inpath, outpath):
    data = None

    with open(inpath) as fptr:
        data = xmltodict.parse(fptr.read())

    with io.open(outpath, 'w', encoding='utf8') as fptr:
        
        compendium = data['compendium']
        for nodeType, node in compendium.items():

            if nodeType == 'spell':
                for spell in node:
                    SpellToOrg(fptr, spell)

            elif nodeType == 'monster':
                for monster in node:
                    MonsterToOrg(fptr, monster)
            
        
##########################
Convert(sys.argv[1], sys.argv[2])

